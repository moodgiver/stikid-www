  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <h2 class="title">Simulatore</h2>
</div>
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="margin:0;padding:0">
  <?php
    //print_r ( $prodotto[0] );
    $foto = base_url().'public/img/simulatore-bg.jpg';
    if ( isset($this->session->simulatore) ){
        $foto = $this->session->simulatore;
    }
  ?>
  <div>
  <img id="parete" src="<?=$foto?>" style="width:100%;height:auto;border:2px solid #999;">
  <?php
    if ( count($sim_stickers) > 0 ){
      foreach ( $sim_stickers AS $sim ){
        $top = rand(15,85);
        $left = rand(15,85);
        echo '
        <img class="draggable hide" data-toggle="tooltip" title="'.$sim['ac_width_cm'].'x'.$sim['ac_height_cm'].' cm" src="'.$sim['ac_immagine'].'" data-width="'.$sim['ac_width_px'].'" data-height="'.$sim['ac_height_px'].'" data-larg="'.$sim['ac_width_cm'].'" data-alt="'.$sim['ac_height_cm'].'" style="position:absolute;top:'.$top.'%;left:'.$left.'%;cursor:move;float:left;height:auto;"/>
        ';
      }
    } else {
      echo '
      <img id="draggable"  src="'.$this->config->item('static_url').'images/stickers/'.$prodotto[0]['ac_image'].'"  style="cursor:move;position:absolute;top:10%;left:10%;z-index:9;height:auto"/>
      ';
    }
  ?>
  </div>
  <p class="text-left" style="margin-top:20px;color:#999;margin-left:20px"><small>Il simulatore fornisce esclusivamente un'indicazione del posizionamento dello sticker sulla vostra parete. Le dimensioni sono indicative e servono solo come supporto alla scelta del prodotto selezionato. Per eventuali consigli contattateci pure al nostro Servizio Clienti</small></p>
</div>

<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="margin:0;padding:10px;border:1px solid #eaeaea;border-top-right-radius:10px;border-bottom-right-radius:10px; background:#fafafa;">
  <ul class="simulatore text-center">
    <li data-toggle="tooltip" title="Clicca sull'icona per caricare la foto della tua parete">
      <button class="btn get" data-toggle="modal" data-target="#myModal" style="margin:0;"><span class="fa fa-camera"></span></button> <span style="padding-top:10px">Carica la foto della parete</span>
    </li>
    <li style="margin-top:20px;" data-toggle="tooltip" title="Imposta la larghezza reale in cm della foto che hai caricato muovendo il cursore">
      <span style="padding-top:10px">Seleziona la larghezza reale in cm della foto che hai caricato</span>
      <input class="larg_reale pointer" type="range" value="350" min="100" max="1000" step="1"  style="position:relative;left:0;width:80%;font-size:2em;height:1em;margin-left:20px;margin-bottom:20px;"><br>
      <span class="larg_reale_attuale" style="font-weight:bold;">350cm</span>
    </li>

    <li style="margin-top:20px;" data-toggle="tooltip" title="Trascina l'adesivo per posizionarlo sulla tua parete">
      <span style="padding-top:10px">Posiziona lo sticker tenendo premuto il tasto sx sullo sticker e muovendo il mouse</span>
    </li>
    <li style="margin-top:20px;" data-toggle="tooltip" title="Usa il cursore per dimensionare l'adesivo">
      <p>Scegli la misura del tuo sticker</p>
      <input class="dim pointer" type="range" value="<?=(int)$prodotto[0]['ac_W_min']?>" min="<?=(int)$prodotto[0]['ac_W_min']?>" max="<?=(int)$prodotto[0]['ac_W_max']?>" step="1"  style="position:relative;left:0;width:80%;font-size:2em;height:1em;margin-left:20px;margin-bottom:20px;">
    </li>
    <li style="margin-top:-20px;">
      <strong><span class="sticker_dim"><?=(int)$prodotto[0]['ac_W_min']?>x<?=(int)$prodotto[0]['ac_H_min']?></span> cm</strong>

    </li>
    <li>
      <h3 class="head-title prezzo">&euro; <?=calcolo_prezzo($prodotto[0]['ac_W_min'],$prodotto[0]['ac_H_min'],$prodotto[0]['ac_coefficiente_sconto'])?></h3>
    </li>

  </ul>

  <button class="btn btn-success btn-sm btn-show-misure">Mostra tutte le misure</button>
  <button class="btn btn-success btn-sm btn-hide-misure hide">Nascondi tutte le misure</button>

  &nbsp;
  <a href="#" onclick="history.back();"><button class="btn btn-success btn-sm">ACQUISTA</button></a>
  <input class="sticker_width" type="hidden" value="<?=(int)$prodotto[0]['ac_W_min']?>">
  <input class="start_width" type="hidden" value="<?=(int)$prodotto[0]['ac_W_min']?>">
  <input class="start_height" type="hidden" value="<?=(int)$prodotto[0]['ac_H_min']?>">
  <input class="ratio" type="hidden" value="<?=(float)(int)$prodotto[0]['ac_W_min']/(int)$prodotto[0]['ac_H_min']?>">
  <input class="bg" type="hidden" value=""><br>
  <input class="bgratio" type="hidden" value="">
  <input class="sconto" type="hidden" value="<?=$prodotto[0]['ac_coefficiente_sconto']?>">
</div>
<div class="col-lg-12 clearfix">

</div>

<?php
  include_once ( 'simulatore_foto_upload.php' );
?>
<style>
ul.simulatore li { background:#e1e1e1; padding:5px; border-radius:5px; }
ul.simulatore li input { background:#e1e1e1; }

.draggable:hover { border:1px dashed #444; border-radius:5px; }
</style>
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
  $("#draggable").draggable();
  $('.draggable').draggable({
    containment: 'parent'
  });
  var bg_w = $('#parete').width();
  var bg_h = $('#parete').height();
  var bg_r = parseInt(bg_w)/parseInt(bg_h);
  $('.bg').val( $('#parete').width() + ' x ' + $('#parete').height() );
  $('.bgratio').val( bg_r );
  var default_w = 350; //cm
  var default_ratio =  350/bg_w;


  $('.draggable').each(function(i, obj) {
    var calcolo = (bg_w*parseInt($(this).data('larg')))/350;
    $(this).attr('data-init_w',calcolo);
    console.log ( 'width px => ' , calcolo );
    $(this).css('width',calcolo).css('height','auto');
    $(this).removeClass('hide');
  });
  //var image_w= parseInt($('.sticker_width').val())/default_ratio;
  //$('#draggable').css('width',image_w);
  //console.log ( 'image width calculated' ,  image_w );

  $('.larg_reale').on('change',function(){
    $('.larg_reale_attuale').html ( $(this).val() + 'cm' );
    $('.dim').val($('.start_width').val());
    $('.sticker_dim').html ( $('.dim').val()  + ' x ' + $('.start_height').val() );
    ricalcola($(this).val());
  });

  function ricalcola(valore){
    $('.draggable').each(function(i, obj) {
      var calcolo = (bg_w*parseInt($(this).data('larg')))/valore;
      $(this).attr('data-init_w',calcolo);
      $(this).css('width',calcolo).css('height','auto');
      var perc = parseInt(valore)/parseInt($('.start_width').val());
      console.log ( perc );
    })
  }
  $('.dim').on('change',function(){
    $('.draggable').tooltip('hide')
    var w = $(this).val();
    var r = parseFloat($('.ratio').val());
    var sw = $('.start_width').val();
    var perc = (w-sw)/sw;
    if ( r < 1 ){
      var h = parseInt(w*parseFloat($('.ratio').val()));
    } else {
      var h = parseInt(w/parseFloat($('.ratio').val()));
    }
    $('.sticker_dim').html ( w + ' x ' + h );
    var perc = parseInt(w)/parseInt($('.start_width').val());
    ricalcola_prezzo(w,h,$('.sconto').val());
    $(this).removeClass('hide');
    console.log ( 'resizing ...')
    $('.draggable').each(function(i, obj) {
      var new_w = parseInt(parseInt($(this).data('init_w'))*perc);
      var new_larg = parseInt(parseInt($(this).data('larg'))*perc);
      var new_alt = parseInt(parseInt($(this).data('alt'))*perc);
      console.log ( $(this).data('width') , new_w , new_larg );
      $(this).css('width',new_w).css('height','auto');
      $(this).attr('title',new_larg+"x"+new_alt+" cm");
      $(this).attr('title', new_larg+"x"+new_alt+" cm").tooltip('fixTitle');
    });

  });

  function ricalcola_prezzo(w,h,s){
    $.post ( '../ajax' ,
      {
        mode: 'calcolo_prezzo',
        w: w,
        h: h,
        cs:s
      }, function ( result ){
        $('.prezzo').html ( '&euro; ' + result );
      }
    )
  }

  $('.btn-show-misure').on('click',function(){
      $('.draggable').tooltip('show');
      $(this).addClass('hide');
      $('.btn-hide-misure').removeClass('hide');
  })

  $('.btn-hide-misure').on('click',function(){
      $('.draggable').tooltip('hide');
      $(this).addClass('hide');
      $('.btn-show-misure').removeClass('hide');
  })

});
</script>
