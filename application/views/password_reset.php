<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <h2 class="title">Reimposta password</h2>
  <div class="col-lg-3 col-md-3">
  </div>
  <?php
  if ( !isset($email) || isset($error) ){
    echo '
  <div class="col-lg-6 col-md-6">
    <p>Stai per richiedere la reimpostazione della tua password. Inserisci la mail con cui ti sei registrato e riceverai via mail le istruzioni per reimpostare la password.
    </p>
    Email<br>
  <form action="'.base_url().'password/request" method="post">
    <div class="form-group text-center">

      <div class="input-group" style="max-width:50vw;">
        <input type="email" name="email" class="form-control" placeholder="indirizzo email della registrazione" value="'.set_value('email').'"/>
        <div class="input-group-addon email-input"><span class="fa fa-at"></span></div>
      </div>
      <br>
      <button type="submit" class="btn get">Reimposta Password</button>
    </div>
  </form>';
    if ( isset($error) ){
      echo '<div class="alert alert-danger">'.$email.'</div>';
    }
  } else {
      echo '<div class="alert alert-success">'.$email.'</div>';
  }
  ?>
</div>
</div>
