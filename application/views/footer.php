<?php
  echo '<script type="text/javascript">
      var routing = "'.base_url().'ajax/";
      var home_url = "'.base_url().'";
  </script>';
?>



<footer id="footer"><!--Footer-->


  <div class="footer-top">
	  <div class="container">
		  <div class="row">
		    <div class="col-lg-12 col-xs-12 text-center" style="margin-bottom:20px;padding-top:10px">
			  <img src="<?=$this->config->item('static_url')?>images/misure-personalizzate.png" style="margin-right:10px"/>
			  <img src="<?=$this->config->item('static_url')?>images/spedizione.png" style="margin-right:10px"/>
			  <img src="<?=$this->config->item('static_url')?>images/soddisfatti.png" style="margin-right:10px"/>
			  <img src="<?=$this->config->item('static_url')?>images/pagamenti.png" style="margin-right:10px"/>
			  <img src="<?=$this->config->item('static_url')?>images/italiano.png"/>
		  </div>

  <div class="footer-widget">
			<div class="container">
				<div class="row">
          <?php
          $heading = ['INFO','FAQ','SERVIZIO','CHI SIAMO'];
          for ( $n=1 ; $n<5 ; $n++){
            $footer = 'footer_'.$n;
            echo '
            <div class="col-lg-2 col-xs-12">
            <div class="single-widget">
              <h2>'.$heading[$n-1].'</h2>
              <ul class="nav nav-pills nav-stacked">';
              foreach ( ${$footer} AS $menu ){
                if ( $menu['int_sito'] == $this->config->item('sito') || $menu['int_sito'] == 0 ){
                  echo '
                  <li><a href="'.base_url().'pagina/'.$menu['ac_url'].'"> '.$menu['ac_titolo'].'</a></li>
                  ';
                }
              }
            echo '</ul></div></div>';
          }
          ?>

					<div class="col-lg-3 col-sm-offset-1 col-xs-12">
						<div class="single-widget">
							<h2>STICASA Newsletter</h2>
							<form action="#" class="searchform">
								<input type="text" placeholder="indirizzo mail" />
								<button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
								<p>Inviami le ultime informazioni sui prodotti STICASA</p>
							</form>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-xs-12">
					<p class="pull-left">Copyright Sticasa. Tutti i diritti riservati - P.IVA e Reg. Imprese 12822420159 - servizioclienti@sticasa.com</p>
					<p class="pull-right">Designed by <span><a target="_blank" href="http://www.moodgiver.com">Moodgiver</a></span></p>
					</div>
				</div>
			</div>
		</div>

</footer>
<div class="notification"></div>
<script>
var w = $(window).width();
if ( w < 640 ){
  $('#cssmenu .button').click(function(){

    if ( !$('#slider-carousel').hasClass('hide') ){

      $('#slider-carousel').addClass('hide');
    } else {
      $('#slider-carousel').removeClass('hide');
    }

  })
}
</script>


<script src="<?=base_url()?>public/js/bootstrap.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="<?=base_url()?>public/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>public/js/jquery.scrollUp.min.js"></script>
<!--<script src="<?=base_url()?>public/js/mlens.js"></script>-->
<script src="<?=base_url()?>public/js/lightbox/js/lightbox.min.js"></script>
<script src="<?=base_url()?>public/js/jquery.prettyPhoto.js"></script>
<script src="<?=base_url()?>public/js/main.js"></script>

<script src="<?=base_url()?>public/js/routing.js"></script>
<script src="<?=base_url()?>public/js/utils.js"></script>
<!--- <script src="<?=base_url()?>public/js/carrello.js"></script> --->
<script src="<?=base_url()?>public/js/delegate.js"></script>
<script src="<?=base_url()?>public/js/menuheader.js"></script>
<br>
<!--<?php
  print_r ( $_SESSION );
 ?>-->
<!--<?php
  echo $_SERVER['SERVER_ADDR'];
  $this->config->set_item('site','STIKID');
  foreach ( $this->config->item('settings') AS $setting ){
    echo $setting.' => ';
    print_r($this->config->item($setting));
    echo '<br>';
  }
 ?>-->
</body>
</html>
