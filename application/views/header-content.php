
        <div class="row mobile-hide" style="background:#f7f7f7;padding:0px;top:0;width:100%;left:0;">
	         <div class="col-lg-6 col-sm-6 col-xs-12">
		        <ul class="nav navbar-nav menutop">
                <li><a href="<?=base_url();?>cart/conferma" class="top-menu">Il tuo carrello</a>
                </li>
                <li><a href="#" class="vertical-bar"> | </a></li>
			          <?php
                  if ( $this->session->user['islogged'] ){
                    echo '
                    <li><a href="'.base_url().'profilo/account" class="top-menu">Il tuo account</a>
                    </li>
                    <li><a href="#" class="vertical-bar"> | </a></li>
                    <li><a href="'.base_url().'logout" class="top-menu">Esci</a>';
                  } else {
                    echo '
                    <li><a href="#" class="top-menu" data-toggle="modal" data-target="#loginModal">Accedi</a>
                    </li>
                    <li><a href="#" class="vertical-bar"> | </a></li>
                    <li><a href="#" data-toggle="modal" data-target="#registerModal" class="top-menu">Registrati</a>
                    </li>
                    ';
                    // data-toggle="modal" data-target="#registerModal"
                  }
                ?>
              </ul>
            </div>


	<div class="col-lg-6 col-sm-6 col-xs-12">

		<ul class="nav navbar-nav pull-right menutop">
      <?php
      foreach ( $header_top_right AS $menu ){
        $link = base_url().'pagina/'.$menu['ac_url'];
        if ( $menu['ac_livello'] == -1 ){
          $link = 'http:'.$menu['ac_url'];
        }
        echo '<li><a href="'.$link.'" class="top-menu">'.$menu['ac_titolo'].'</a></li>';
        echo '<li><a href="#" class="vertical-bar"> | </a></li>';
      }
      ?>
      <!--
			<li><a href="<?=base_url()?>pagina/faq/95" class="top-menu">Domande frequenti</a>
			</li>
			<li><a href="#" class="vertical-bar"> | </a></li>
			<li><a href="<?=base_url()?>pagina/video istruzioni/246" class="top-menu">Istruzioni montaggio</a>
			</li>
			<li><a href="#" class="vertical-bar"> | </a></li>
			<li><a href="http://blog.stikid.com" target="_new" class="top-menu">Blog</a>
			</li>
    -->
			<li id="preloader">Loading ...<i class="fa fa-spin fa-refresh"></i></li>
		</ul>

	</div>

</div>


<!--- LOGO --->
<div class="clearfix"></div>
<div class="col-lg-8 col-sm-8 col-xs-12 header-logo" style="margin-top:20px;margin-bottom:20px;">
  <?php
    $logo = "stikid-logo-normal.png";
    if ( $this->config->item('sito') == 1 ){
      $logo = "sticasa-logo.jpg";
    }
    if ( $this->config->item('isqpb') ){
      $logo = "quadriperbambini.png";
    }
  ?>
	<a href="<?=base_url()?>"><img src="<?=$this->config->item('static_url')?>images/<?=$logo?>" alt="Home" title="Home"/></a>
</div>

<div class="col-lg-3 col-sm-3 mobile-hide form-inline search-bar" style="margin-right:0px;padding-right:0px;">
	<p style="padding-left:0px;"><small><span class="fa fa-envelope"></span> servizioclienti@sticasa.com</small></p>
  <form action="<?=base_url()?>cerca" method="post">
	<div class="form-group">
	<input type="text" class="form-control" name="search" placeholder="Cerca...">
	</div>
	<button class="btn btn-primary get" data-controller="search-site" data-action="search-site" style="margin-top:0"><span class="fa fa-search"></span></button></form>

</div>
<div class="col-lg-1" style="margin-left:-20px;padding-top:10px;padding-left:0px"><br><br><a href="<?=base_url();?>cart/conferma" class="top-menu"><button class="btn btn-primary get" style="background:#eb008b;margin-top:0px"><span class="fa fa-shopping-cart"></span></button></a></div>
<div class="clearfix"></div>

<div class="modal fade modal-sm" id="registerModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document" style="width:30vw;">
    <div class="modal-content">
      <div class="modal-header" style="background:#96bf0d">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Registrazione</h4>
      </div>
      <div class="modal-body">
        <form action="<?=base_url()?>registrazione" method="post">
        <div class="signup-form"><!--sign up form-->
          <div class="form-group">
            Email<br>
            <div class="input-group">
              <input type="email" name="email" class="form-control" placeholder="indirizzo email" value="<?php echo set_value('email'); ?>"/>
              <div class="input-group-addon email-input"><span class="fa fa-at"></span></div>
            </div>
            <br>
            Password<br>
            <div class="input-group">
              <input type="password" name="password" class="form-control"/>
              <div class="input-group-addon"><span class="fa fa-lock"></span></div>
            </div>
            <br>
            <!--Conferma Password<br>
            <div class="input-group">
              <input type="password" name="passwordconf" class="form-control"/>
              <div class="input-group-addon"><span class="fa fa-lock"></span></div>
            </div>
            -->
          </div>
        </div><!--/sign up form-->
        <h5 class="msg-danger msg_register_cart"><?php echo form_error('email'); ?></h5>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn get">Registrami</button>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade modal-sm" id="loginModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document" style="width:30vw;">
    <div class="modal-content">
      <div class="modal-header" style="background:#96bf0d">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Accedi</h4>
      </div>
      <div class="modal-body">
        <form action="<?=base_url()?>signin" method="post">
        <input type="hidden" name="normal" value="1">
        <div class="signup-form"><!--sign up form-->
          <div class="form-group">
            Email<br>
            <div class="input-group">
            <input type="text" name="email_login" class="form-control" placeholder="indirizzo email" value="<?php echo set_value('email_login'); ?>" />
            <div class="input-group-addon email-cart-input"><span class="fa fa-at"></span></div>
            </div>
            <br>

            Password<br>
            <div class="input-group">
              <input type="password" name="password_login" class="form-control"/>
              <div class="input-group-addon"><span class="fa fa-lock"></span></div>
            </div>

          </div>
        </div><!--/sign up form-->


        <h5 class="msg-danger msg_register_cart"><?php echo form_error('email'); ?></h5>
        <p>Password dimenticata? <a href="<?=base_url()?>password/reset">Richiedi una nuova password</a></p>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn get">Accedi</button>


      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
