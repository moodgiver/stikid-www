
<section>
  <div class="container full-container">
    <!-- collezioni -->

    <div class="col-lg-9 col-sm-9 col-xs-12">

      <h2 class="title text-center" style="margin-bottom:20px">Collezioni</h2>
      <?php

        include_once ( 'collezioni.php' );

        if ( !$this->config->item('isqpb') ){
          //piu venduti
          $lista = 'venduti';
          include ( 'prodotti.php' );
        }

        $lista = 'offerte';
        include ( 'prodotti.php');
        include_once('banner_after_offerte.php');
      ?>
    </div>
  </div>
</section>
