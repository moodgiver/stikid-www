<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
  <div class="col-sm-12 misure-container box-info-prodotto">
  <?php
    $misure = $this->Site_model->prezzi_qpb($p['ac_materiale']);
    $telaio = $misure[0]['ac_p'];
    $w = $misure[0]['ac_w'];
    $h = $misure[0]['ac_h'];
    $prezzo = prezzo_base_qpb ( $p['ac_materiale'] );
    $cs = $p['ac_coefficiente_sconto'];

    echo '<h2 class="prezzo" style="font-size:1.5em;">
    <span class="prezzo_val">&euro; '.number_format($prezzo,2).'</span>
    </h2>';
    echo '<h5>Misure disponibili</h5>';
    $checked = 'checked';
    foreach ( $misure AS $m ){
      echo '
      <div class="col-lg-1 col-xs-1">
        <input type="radio" name="misura" class="misura misura_'.$m['id_misura'].'" data-w="'.$m['ac_w'].'" data-h="'.$m['ac_h'].'" data-p="'.$m['ac_p'].'" data-prezzo="'.$m['ac_prezzo'].'" '.$checked.'>
      </div>
      <div class="col-lg-6 col-xs-6">
        '.$m['ac_w'].'x'.$m['ac_h'].'
        <p>Telaio '.$m['ac_p'].' cm
      </div>
      <div class="col-lg-4 col-xs-4 text-right">
        &euro; '.number_format($m['ac_prezzo'],2).'
      </div>
      <div class="clearfix"></div>';
      $checked = '';
    }


    echo '
    <div class="clearfix" style="margin-top:50px;"></div>';
    $spedizione = $this->config->item('spese_spedizione');
    $color = 'Black';
    if ( $p['bl_colore'] == 0 ){
      $color = 'Multicolor';
    }
    echo '<p>Pi&uacute; &euro; '.number_format($spedizione,2).' spese di spedizione</p>';
    echo '<button class="btn cart btn-aggiungi-al-carrello btn-action" data-action="addToCart" data-id="'.$p['id_prodotto'].'" data-sconto="'.$cs.'"  data-uuid="" style="width:100%"><i class="fa fa-shopping-cart"></i> AGGIUNGI AL CARRELLO</button>';


  ?>

  <div class="col-sm-12 text-center">
    <?php
    echo '<input type="hidden" class="id_prodotto" value="'.$p['id_prodotto'].'">
    <input type="hidden" class="ac_prodotto"	value="'.$p['ac_prodotto'].'">
    <input type="hidden" class="thumb"		value="'.$p['ac_immagine'].'">
    <input type="hidden" class="ac_width"		value="'.$w.'">
    <input type="hidden" class="ac_height"	value="'.$h.'">
    <input type="hidden" class="ac_prezzo"	value="'.$prezzo.'">
    <input type="hidden" class="telaio" value="'.$telaio.'">
    <input type="hidden" class="ac_colore"			value="'.$color.'">
    <input type="hidden" class="currentTesto" value="">
    <input type="hidden" class="currentWidth" value="">
    <input type="hidden" class="currentHeight" value="">
    <input type="hidden" class="currentFont" value="">
    <input type="hidden" class="currentColorName" value="">
    <input type="hidden" class="currentTotale" value="">
    <input type="hidden" class="ac_font" value="">
    <input type="hidden" class="ac_testo" value="">
    <input type="hidden" class="ac_collezione" value="'.$p['ac_categoria'].'">';
    ?>

  </div>

  <div class="col-lg-12 col-xs-12 box-info-prodotto">
      <h4>Soddisfatti o rimborsati</h4>
      <p><small>Se il tuo Stickers non aderisce, puoi scegliere se ottenere il rimborso o la sostituzione del tuo ordine.</small></p>
  </div>
  <div class="col-lg-12 col-xs-12 box-info-prodotto">
      <h4>Spedito in 11 giorni</h4>
      <p><small>Per acquisti con bonifico bancario i tempi di lavorazione decorrono dalla data di ricezione del pagamento.</small></p>
  </div>

</div>
</div>

<script>
$(document).ready(function(){
  $('.misura').on('click',function(){
    var prezzo = $(this).data('prezzo');
    var w = $(this).data('w');
    var h = $(this).data('h');
    var telaio = $(this).data('p');
    $('.prezzo_val').html ( "&euro; " + parseFloat(prezzo).toFixed(2) );
    $('.ac_width').val( w );
    $('.ac_height').val ( h );
    $('.ac_prezzo').val(parseFloat(prezzo).toFixed(2));
    $('.telaio').val ( telaio );
  })
});
</script>
