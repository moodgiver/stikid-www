<?php
  $sm = ['S','M','L','XL'];
  $diff = $wMax-$w;
  $dist = (int)($diff/3);
  $sc = $p['ac_coefficiente_sconto'];
  if ( isset($isPromo) ){
    $sc = $prodotto_promo[0]['ac_sconto'];
  }
  $prezzoS = number_format((int)($this->ecommerce->prezzo_prodotto($w,$h)*$sc),2);
  echo '<div class="col-lg-1 col-xs-1">
  <input type="radio" name="misura" class="misura_pre misura_1" data-w="'.$w.'" data-h="'.$h.'"  data-prezzo="'.$prezzoS.'" checked>
  </div>';
  echo '<div class="col-lg-6 col-xs-6">'.$w.' x '.$h.'cm</div>';
  echo '<div class="col-lg-4 col-xs-4 text-right">
        &euro; '.$prezzoS.'
      </div><div class="clearfix"></div>';
  //echo $scelta_misure[0].'-'.$w.'x'.$h.'('.$prezzoS.')<br>';
  $m = $w+$dist;
  for ( $x=0 ; $x<2 ; $x++ ){
    $hn = (int)($m/$ratio);
    $prezzoS = number_format((int)($this->ecommerce->prezzo_prodotto($m,$hn)*$sc),2);
    echo '<div class="col-lg-1 col-xs-1">
    <input type="radio" name="misura" class="misura_pre misura_'.($x+2).'" data-w="'.$m.'" data-h="'.$hn.'"  data-prezzo="'.$prezzoS.'">
    </div>';
    echo '<div class="col-lg-6 col-xs-6">'.$m.' x '.$hn.'cm</div>';
    echo '<div class="col-lg-4 col-xs-4 text-right">
          &euro; '.$prezzoS.'
        </div><div class="clearfix"></div>';
    //echo $scelta_misure[$x+1].'-'.$m.'x'.$hn.'('.$prezzoS.')<br>';
    $m = $m+$dist;
  }
  $hn = (int)($wMax/$ratio);
  $prezzoS = number_format((int)($this->ecommerce->prezzo_prodotto($m,$hn)*$sc),2);
  echo '<div class="col-lg-1 col-xs-1">
  <input type="radio" name="misura" class="misura_pre misura_4" data-w="'.$wMax.'" data-h="'.$hn.'"  data-prezzo="'.$prezzoS.'">
  </div>';
  echo '<div class="col-lg-6 col-xs-6">'.$wMax.' x '.$hn.'cm</div>';
  echo '<div class="col-lg-4 col-xs-4 text-right">
        &euro; '.$prezzoS.'
      </div><div class="clearfix"></div>';
  //echo $scelta_misure[3].'-'.$wMax.'x'.$hn.'('.$prezzoS.')<br>';
?>

<script>
$(document).ready(function(){

  $('.misura_pre').on('click',function(){
    var p = $(this).data('prezzo');
    var w = $(this).data('w');
    var h = $(this).data('h');
    $('.prezzo_val').html('&euro; ' + p );
    $('.misure').html('L ' + w + ' x A ' + h + 'cm');
    $('.ac_width').val(w);
    $('.ac_height').val(h);
    $('.ac_prezzo').val(p);
    $('.selector-misure').val(w);
  })

})
</script>
