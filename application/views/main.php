<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
$title = $header_meta[0]['ac_content'];
//$title = 'Stickers bambini - Stikid: adesivi bambini, decorazione camerette con adesivi per pareti, decorare stencil, disegni murali';

$meta = array(
      array (
        'name' => 'description',
        'content' => $header_meta[1]['ac_content']
      ),
      array(
        'name' => 'keywords',
        'content' => $header_meta[2]['ac_content']
      ),
      array(
        'name' => 'Content-type',
        'content' => 'text/html; charset=utf-8', 'type' => 'equiv'
      )
);
  switch ($mode){

    case 'collezione':
      $title = 'STIKID - '.$prodotti_collezione[0]['collezione_meta_title'];
      $meta[0]['content'] = $prodotti_collezione[0]['ac_meta_description'];
      $meta[1]['content'] = $prodotti_collezione[0]['ac_meta_keys'];
    break;

    case 'prodotto':
      $cat = $prodotto[0]['ac_categoria'];
      if ( $prodotto[0]['ac_categoria_lang'] != ''){
        $cat = $prodotto[0]['ac_categoria_lang'];
      }
      $title = 'STIKID - '.$cat.' - '.$prodotto[0]['ac_prodotto'].' - Adesivo Murale';
      $meta[0]['content'] = $prodotto[0]['ac_meta_description'];
      $meta[1]['content'] = $prodotto[0]['ac_meta_keys'];
    break;
  }
?>

<?php
  include_once('header.php');



  echo '<div class="container">';
  echo '<div class="row" style="margin-right:0px;margin-left:0px">';
  if ( $this->config->item('sito') == 4 ) {
    include_once('menu_header.php');
  } else {
    include_once('menu_header_sticasa.php');
  }
  if ( $mode == 'home' && $this->config->item('sito') == 4 && !$this->config->item('isqpb') ){
    include_once('slider.php');
    echo '<br><br>';
  }


  echo '</div></div>';

?>

<section>
  <div class="container full-container" style="padding:0px;">

  <?php
    if ( $this->config->item('isqpb') ){
      echo '<div class="clearfix" style="margin-top:30px;"></div>';
    }

    echo '<div class="container">';
    if ( $mode != 'cart' && $mode != 'password' && $mode != 'profilo' && $mode != 'simulatore' && $mode != 'registrazione' ){
      include_once('sidebar.php');
    }

    if ( $mode == 'error' ){
    	if ( $view == 'login' ){
    		echo '<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 text-center"><h3>Impossibile accedere. Controllare email o password</h3></div>';
    		echo '<script>alert("Impossibile accedere. Controllare email o password")</script>';
    		//include_once('homepage.php');
    	}
    }

    if ( $mode == 'home' ){
      include_once('homepage.php');
    } else {
      include_once('banner_hooks.php');
    }

    if ( $mode == 'collezione' ){
      $lista = 'collezione';
      echo '<div class="col-lg-9 col-sm-9 col-xs-12">';
      include_once('prodotti.php');
      echo '</div>';
    }

    if ( $mode == 'collezioneqpb' ){
      $lista = 'collezione_qpb';
      echo '<div class="col-lg-9 col-sm-9 col-xs-12">';
      include_once('prodotti_qpb.php');
      echo '</div>';
    }

    if ( $mode == 'offerte' ){
      $lista = 'offerte';
      echo '<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">';
      include_once('prodotti.php');
      echo '</div>';
    }

    if ( $mode == 'promo' ){
      $lista = 'promo';
      echo '<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">';
      include_once('prodotti_promo.php');
      echo '</div>';
    }

    if ( $mode == 'search' ){
      $lista = 'collezione';
      echo '<div class="col-lg-9 col-sm-9 col-xs-12">';
      include_once('prodotti.php');
      echo '</div>';
    }

    if ( $mode == 'sticky-text' ){
      echo '<div class="col-lg-9 col-sm-9 col-xs-12">';
      include_once('sticky_text.php');
      echo '</div>';
    }

    if ( $mode == 'prodotto' ){
      $lista = 'prodotto';
      echo '<div class="col-lg-9 col-sm-9 col-xs-12">';
      include_once('prodotto.php');
      if ( count($upsell) > 0 ){
        $lista = 'upsell';
        include_once('prodotti.php' );
      }
      echo '</div>';
    }

    if ( $mode == 'prodottoqpb' ){
      $lista = 'prodottoqpb';
      echo '<div class="col-lg-9 col-sm-9 col-xs-12">';
      include_once('prodotto_qpb.php');
      echo '</div>';
    }

    if ( $mode == 'simulatore' ){
      include_once('simulatore.php');
    }

    if ( $mode == 'cart' ){
      if ( $view == 'detail' ){
        if ( $_SESSION['cart_items'] ){
          include_once ( 'cart_detail.php' );
        } else {
          echo '<div class="col-lg-12 col-xs-12 text-center" style="height:50vh"><h4>Il tuo carrello &eacute; vuoto</h4></div>';
        }
      }

      if ( $view == 'spedizione' ){
        if ( !$this->session->user['islogged'] ){
          require_once ( 'cart_login.php' );
        } else {
          require_once ( 'cart_spedizione.php');
        }
      }

      if ( $view == 'checkout' ){
        require_once ( 'cart_checkout.php' );
      }

      if ( $view == 'paypal_cancel' ){
        require_once ( 'cart_checkout.php' );
        echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix>"<p>Hai annullato la transazione. Per procedere con l\'ordine scegli un metodo di pagamento</p></div>';
      }
    }

    if ( $mode == 'order' ){
      require_once ( 'order_conferma.php' );
    }

    if ( $mode == 'password' ){
      if ( $view == 'reset' ){
        require_once ( 'password_reset.php' );
      }
      if ( $view == 'modifica' ){
        require_once ( 'password_modifica.php' );
      }
      if ( $view == 'changed' ){
        require_once ( 'password_modifica_success.php' );
      }
    }

    if ( $mode == 'registrazione' ){
      require_once ( 'registrazione.php' );
    }

    if ( $mode == 'profilo' ){
      include_once('profilo_sidebar.php');
      if ( $view == 'account' || $view == 'salva' ){
        require_once ( 'profilo_utente.php' );
      }
      if ( $view == 'ordini' ){
        require_once ( 'profilo_ordini.php' );
      }
      if ( $view == 'ordine' ){
        require_once ( 'profilo_ordine.php' );
      }
    }

    if ( $mode == 'pagina' ){
      if ( $view == 'normale' ){
        include_once ( 'pagina.php' );
      }
      if ( $view == 'designers' ){
        include_once ( 'pagina.php' );
        include_once ( 'designers.php' );
      }
    }

    if ( $mode == 'designer' ){
      include_once ( 'designer.php' );
    }

    if ( $mode == 'debug' ){
      echo '<div class="col-lg-9">';
  		echo '<h3>ORDINE</h3>';
      print_r ( $debug_order );
      //echo '<table class="table bordered">';
  		//foreach ( $debug_order AS $key => $value ){
  			//echo '<tr><td>'.$key.'</td><td>'.$value.'</td></tr>';
  		//}
  		echo '</table>';
      echo '<h3>CARRELLO</h3>';
  		echo '<table class="table bordered">';
      if ( count($debug_cart) > 0 ){
  		foreach ( $debug_cart AS $item ){
        echo '<tr><td colspan="2"><strong>ITEM</strong></td></tr>';
        foreach ( $item AS $key=>$value ){
  			  echo '<tr><td>'.$key.'</td><td>'.$value.'</td></tr>';
        }
  		}
      }
  		echo '</table>';
      echo '<h3>UTENTE</h3>';
      echo '<table class="table bordered table-striped">';
      foreach ( $debug_user AS $key => $value ){
        echo '<tr><td>'.$key.'</td><td>'.$value.'</td></tr>';
      }
      echo '</table></div>';
      print_r ( $_SERVER );

    }

    echo '</div>'
  ?>
  <!--
  <?php
  /*
    if ( !isset ( $view ) ){
      $view = '';
    }
    if ( $view != 'spedizione' && $view != 'checkout' && $mode != 'registrazione' && $mode != 'order' ){
      echo '
      <div class="clearfix col-lg-12 col-sm-12 col-xs-12" style="margin-top:30px">
        <!-- TrustBox widget - Mini Carousel -->

        <div class="trustpilot-widget" data-locale="it-IT" data-template-id="539ad0ffdec7e10e686debd7" data-businessunit-id="5195756d0000640005303eb0" data-style-height="350px" data-style-width="100%"data-theme="light" data-stars="1,2,3,4,5" data-schema-type="Organization"> <a href="https://it.trustpilot.com/review/sticasa.com"target="_blank">Trustpilot</a> </div>

        <!-- End TrustBox widget -->
        </div>
      </div>
      ';
    }
    if ( $mode == 'order' ){
      echo '

      <!-- Trustpilot conversion script -->
      <script type="text/javascript">
(function(c,o,n,v,e,r,t){c["TPConversionObject"]=e;c[e]=c[e]||function(){c[e].buid="5195756d0000640005303eb0",(c[e].q=c[e].q||[]).push(arguments)};r=o.createElement(n),t=o.getElementsByTagName(n)[0];r.async=1;r.src=v;t.parentNode.insertBefore(r,t)})(window,document,"script","https://widget.trustpilot.com/conversion/conversion.js","tpConversion");

tpConversion("amount","'.$this->session->order['ORDER_TOTAL'].'");
tpConversion("currency", "EUR");
tpConversion("basket_size", "'.count($this->session->cart_items).'");
</script>
<!-- End Trustpilot conversion script -->
      ';
    }
    */
  ?>
  -->


</section>
<style>
.banners-mobile {
  display:none;
}
@media (max-width: 1024px) {
  .banners-mobile {
    display:block;
  }
}
</style>
<?php
if ( count($banner_hooks) > 0 ){
  echo '<div class="col-lg-12 text-center banners-mobile" style="margin-top:30px;">';
  foreach ( $banner_hooks AS $banner ){
    if ( $banner['posizione'] == 'sidebar' && ($banner['int_sito'] == $this->config->item('sito') || $banner['int_sito'] == 0) ){
      $uri = '#';
      if ( $banner['url'] != '' ){
        $uri = $banner['url'];
      }
      echo '<a href="'.$uri.'"><img src="'.$this->config->item('static_url').'images/banners/'.$banner['banner'].'" class="clearfix" style="width:70%;height:auto;"></a>';
    }
  }
  echo '</div>';
  }

  include_once('footer.php');
?>
