<h5>Dati Cliente</h5>
<div class="form-three form-inline">

    Nome<br>
    <input type="text" class="form-control" name="o_nome" id="o_nome" placeholder="Nome *" value="<?php echo set_value('o_nome'); ?>">
    <?php echo form_error('o_nome'); ?>
    Cognome<br>
    <input type="text" class="form-control"  name="o_cognome" id="o_cognome"  placeholder="Cognome *" value="<?php echo set_value('o_cognome'); ?>">
    <?php echo form_error('o_cognome'); ?>
    <br>
    Azienda<br>
    <input type="text" name="o_azienda" id="o_azienda" value="<?php echo set_value('o_azienda'); ?>" class="form-control" placeholder="Azienda">
    <?php echo form_error('o_azienda'); ?>
    <br>
    Email<br>
    <input type="text" name="o_email" id="o_email" value="<?php echo set_value('o_email'); ?>" readonly class="form-control" placeholder="Email*">
    <?php echo form_error('o_email'); ?>
    <br>
    Indirizzo<br>
    <input type="text" name="o_indirizzo" id="o_indirizzo" value="<?php echo set_value('o_indirizzo'); ?>" class="form-control" placeholder="Indirizzo *">
    <?php echo form_error('o_indirizzo'); ?>
    <br>
    CAP<br>
    <input type="text" name="o_cap" id="o_cap" value="<?php echo set_value('o_cap'); ?>" class="form-control" placeholder="CAP *"
    ><?php echo form_error('o_cap'); ?>
    <br>
    COMUNE<br>
    <input type="text" name="o_citta" id="o_citta" value="<?php echo set_value('o_citta'); ?>" class="form-control" placeholder="Citt&agrave; *" data-msg="Citta'">
    <?php echo form_error('o_citta'); ?>
    <br>
    Provincia<br>
    <select name="o_pv" id="o_pv" class="form-control">
      <option value="">Seleziona ...</option>
      <?php
        foreach ( $province AS $pv ){
          $selected = '';
          if ( $pv['ac_sigla'] == $this->session->order['O_PV'] ){
            $selected = ' selected';
          }
          echo '<option value="'.$pv['ac_sigla'].'"'.$selected.'>'.$pv['ac_provincia'].'</option>';
        }
      ?>
    </select>
    <?php echo form_error('o_pv'); ?>
    <br>
    Nazione<br>
    <select name="o_nazione" id="o_nazione" class="form-control">
      <?php
        if ( $this->session->order['O_NAZIONE'] == '' ){
          $forcedcty = 'ITALIA';
        } else {
          $forcedcty = $this->session->order['O_NAZIONE'];
        }
        foreach ( $this->config->item('country') AS $cty ){
          $selected = '';

          if ( $cty == $forcedcty ){
            $selected = ' selected';
          }
          echo '<option value="'.$cty.'"'.$selected.'>'.$cty.'</option>';
        }
      ?>
    </select>
    <?php echo form_error('o_nazione'); ?>
    <br>
    Telefono<br>
    <input type="text" name="o_telefono" id="o_telefono" value="<?php echo set_value('o_telefono'); ?>" class="form-control required" placeholder="Telefono *">
    <?php echo form_error('o_telefono'); ?>
    <br>
    Mobile<br>
    <input type="text" name="o_mobile" id="o_mobile" value="<?php echo set_value('o_mobile'); ?>" class="form-control required" placeholder="Mobile *">
    <?php echo form_error('o_mobile'); ?>
    <br>
    Codice Fiscale<br>
    <input type="text" name="o_cf" id="o_cf" value="<?php echo set_value('o_cf'); ?>" class="form-control required" placeholder="Codice Fiscale *">
    <?php echo form_error('o_cf'); ?>
    P.IVA<br>
    <input type="text" name="o_piva" id="o_piva" value="<?php echo set_value('o_piva'); ?>" class="form-control required" placeholder="Partita IVA">
    <?php echo form_error('o_piva'); ?>
