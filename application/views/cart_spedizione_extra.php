<!-- SPEDIZIONE ALTRO INDIRIZZO -->
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 clearfix" style="border-left:1px dotted #eaeaea;">
    <div class="ship-to bill-to hide">
      <h5>Spedisci a</h5>

      <div class="form-three form-inline">
          <span class="text-danger">Nome</span><br>
          <input type="text" class="form-control" name="os_nome" id="os_nome" placeholder="Nome *" value="<?php echo set_value('os_nome'); ?>">
          <?php echo form_error('os_nome'); ?>
          <span class="text-danger">Cognome</span><br>
          <input type="text" class="form-control"  name="os_cognome" id="os_cognome"  placeholder="Cognome *" value="<?php echo set_value('os_cognome'); ?>">
          <?php echo form_error('os_cognome'); ?>
          Azienda<br>
          <input type="text" name="os_azienda" id="os_azienda" value="<?php echo set_value('os_azienda'); ?>" class="form-control" placeholder="Azienda">

          <br>
          <span class="text-danger">Indirizzo</span><br>
          <input type="text" name="os_indirizzo" id="os_indirizzo" value="<?php echo set_value('os_indirizzo'); ?>" class="form-control" placeholder="Indirizzo *">
          <?php echo form_error('os_indirizzo'); ?>
          <span class="text-danger">CAP</span><br>
          <input type="text" name="os_cap" id="os_cap" value="<?php echo set_value('os_cap'); ?>" class="form-control" placeholder="CAP *"
          ><?php echo form_error('os_cap'); ?>
          <span class="text-danger">COMUNE</span><br>
          <input type="text" name="os_citta" id="os_citta" value="<?php echo set_value('os_citta');?>" class="form-control" placeholder="Citt&agrave; *" data-msg="Citta'">
          <?php echo form_error('os_citta'); ?>
          <span class="text-danger">Provincia</span><br>
          <select name="os_pv" id="os_pv" class="form-control">
            <option value="">Seleziona ...</option>
            <?php
              foreach ( $province AS $pv ){
                $selected = '';
                if ( $pv['ac_sigla'] == set_value('os_pv') ){
                  $selected = ' selected';
                }
                echo '<option value="'.$pv['ac_sigla'].'"'.$selected.'>'.$pv['ac_provincia'].'</option>';
              }
            ?>
          </select>
          <?php echo form_error('os_pv'); ?>
          <br>
          <span class="text-danger">Nazione</span><br>
          <select name="os_nazione" id="os_nazione" class="form-control">
            <?php
              if ( $this->session->order['OS_NAZIONE'] == '' ){
                $forcedcty = 'ITALIA';
              } else {
                $forcedcty = $this->session->order['OS_NAZIONE'];
              }
              foreach ( $this->config->item('country') AS $cty ){
                $selected = '';
                if ( $cty == $forcedcty ){
                  $selected = ' selected';
                }
                echo '<option value="'.$cty.'"'.$selected.'>'.$cty.'</option>';
              }
            ?>
          </select>
          <br>
          Telefono<br>
          <input type="text" name="os_telefono" id="os_telefono" value="<?php echo set_value('os_telefono'); ?>" class="form-control" placeholder="Telefono *">
          <?php echo form_error('os_telefono'); ?>
          <span class="text-danger">Mobile</span><br>
          <input type="text" name="os_mobile" id="os_mobile" value="<?php echo set_value('os_mobile'); ?>" class="form-control" placeholder="Mobile *">
          <?php echo form_error('os_mobile'); ?>
          <input type="hidden" class="shipto" name="o_shipto" id="o_shipto" value="<?=$this->session->order['SHIPTO']?>" data-validation="boolean">
      </div>
    </div>
  </div>


</div>
