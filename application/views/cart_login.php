
<section id="form"><!--form-->

		<div class="container container-login-cart">
			<div class="row">
        <!-- REGISTRAZIONE -->
				<div class="col-lg-4 col-sm-4 col-sm-offset-1 col-xs-12">
          <form action="<?=base_url()?>registrazione" method="post">
					<div class="signup-form"><!--sign up form-->
						<h2>Registrazione</h2>
						<div class="form-group">
							Email<br>
							<div class="input-group">
								<input type="email" name="email" class="form-control" placeholder="indirizzo email" value="<?php echo set_value('email'); ?>"/>
								<div class="input-group-addon email-input"><span class="fa fa-at"></span></div>
							</div>
							<br>


							Password<br>
							<div class="input-group">
								<input type="password" name="password" class="form-control"/>
								<div class="input-group-addon"><span class="fa fa-lock"></span></div>
							</div>
							<input type="hidden" name="redirect" value="cart/spedizione">
							<button type="submit" class="btn get">Registrami</button>
						</div>
					</div><!--/sign up form-->
					<?php
						if ( isset($error_register) ) {
							echo '<div class="alert alert-danger alert-dismissible">'.$error.'</div>';
						}
					?>
          </form>
				</div>
        <!-- ./REGISTRAZIONE -->

        <div class="col-sm-1 mobile-hide">
					<h2 class="or"><span class="fa fa-chevron-left"></span><span class="fa fa-chevron-right"></span></h2>
				</div>

        <!-- LOGIN -->
				<div class="col-sm-4 col-xs-12">
					<form action="<?=base_url()?>login" method="post">
					<div class="login-form"><!--login form-->
						<h2>Login</h2>
						<div class="form-group">

							Email<br>
							<div class="input-group">
							<input type="text" name="email_login" class="form-control" placeholder="indirizzo email" value="<?php echo set_value('email_login'); ?>" />
							<div class="input-group-addon email-cart-input"><span class="fa fa-at"></span></div>
							</div>
							<br>

							Password<br>
							<div class="input-group">
								<input type="password" name="password_login" class="form-control"/>
								<div class="input-group-addon"><span class="fa fa-lock"></span></div>
							</div>

							<button type="submit" class="btn btn-default get">Login</button>
						</div>
					</div><!--/login form-->

					<?php echo form_error('email_login');?>
				</div>
        </form>
			</div>
      <!-- ./ LOGIN -->

		</div>
</section><!--/form-->
<style>
	.container-login-cart { min-height: 70vh; }
	@media screen and (max-width:1000px){
		.signup-form , .login-form { padding:15px; }
		.container-login-cart { min-height: 80vh; }
	}
</style>
