<div class="jumbotron col-lg-9 col-md-9 col-sm-9 col-xs-12">
<h2>
  Registrazione
</h2>

<?php
  $nascondi = '';
  if ( isset($is_user) ){
    if ( $is_user ){
    echo '<h4 class="title">Attenzione la mail '.$_POST['email'].' e\' gia\' registrata. <br>Se non ricordi la password <a href="password/reset">clicca qui</a> oppure torna indietro per registrarti con un altro indirizzo e-mail.</h4>
    <p><small>
    <strong>Se desideri ricevere assistenza puoi contattare il nostro servizio clienti:</strong>
    <ul>
    <ol>- al numero 371 3590817 (disponibile anche su whatsapp e telegram)</ol>
    <ol>- tramite la chat online</ol>
    <ol>- al nostro indirizzo e-mail servizioclienti@sticasa.com</ol></ul></small></p>';
    $nascondi = 'hide';
    } else {
    echo '<p>Per completare la registrazione è necessario inserire i dati richiesti nel seguente modulo.</p>';
    $nascondi = '';
    }
  }
?>
<style>
  .form-inline { font-size:.8em; font-weight:bold;}
  .form-registrazione p { font-size:.7em!important; color:red; margin-bottom:-30px;}
</style>
<div class="form-registrazione <?=$nascondi?>">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
      <form action="registrami" method="post">
        <div class="bill-to">
          <div class="form-three form-inline">
            Email<br>

            <input type="text" name="o_email" id="o_email" value="<?php echo set_value('o_email'); ?>" class="form-control" placeholder="Email*">
            <?php echo form_error('o_email'); ?>
            Password<br>
            <input type="password" name="o_password" id="o_password" value="<?php echo set_value('o_password'); ?>" class="form-control" placeholder="Password*">
            <?php echo form_error('o_password'); ?>
            Conferma Password<br>
            <input type="password" name="o_password_conf" id="o_password_cont" value="<?php echo set_value('o_password_conf'); ?>" class="form-control" placeholder="Password*">
            <?php echo form_error('o_password_conf'); ?>
            <br>
            Nome<br>
            <input type="text" class="form-control" name="o_nome" id="o_nome" placeholder="Nome *" value="<?php echo set_value('o_nome'); ?>">
            <?php echo form_error('o_nome'); ?>
            <br>
            Cognome<br>
            <input type="text" class="form-control"  name="o_cognome" id="o_cognome"  placeholder="Cognome *" value="<?php echo set_value('o_cognome'); ?>">
            <?php echo form_error('o_cognome'); ?>
            <br>
            Azienda<br>
            <input type="text" name="o_azienda" id="o_azienda" value="<?php echo set_value('o_azienda'); ?>" class="form-control" placeholder="Azienda">
            <?php echo form_error('o_azienda'); ?>
            <br>
            Indirizzo<br>
            <input type="text" name="o_indirizzo" id="o_indirizzo" value="<?php echo set_value('o_indirizzo');?>" class="form-control" placeholder="Indirizzo *">
            <?php echo form_error('o_indirizzo'); ?>
            <br>
            CAP<br>
            <input type="text" name="o_cap" id="o_cap" value="<?php echo set_value('o_cap');?>" class="form-control" placeholder="CAP *"
            ><?php echo form_error('o_cap'); ?>
            <br>
            COMUNE<br>
            <input type="text" name="o_citta" id="o_citta" value="<?php echo set_value('o_citta');?>" class="form-control" placeholder="Citt&agrave; *" data-msg="Citta'">
            <?php echo form_error('o_citta'); ?>
            <br>
            Provincia<br>
            <select name="o_pv" id="o_pv" class="form-control">
              <option value="">Seleziona ...</option>
              <?php
                foreach ( $province AS $pv ){
                  $selected = '';
                  if ( $pv['ac_sigla'] == set_value('o_pv') ){
                    $selected = ' selected';
                  }
                  echo '<option value="'.$pv['ac_sigla'].'"'.$selected.'>'.$pv['ac_provincia'].'</option>';
                }
              ?>
            </select>
            <br>
            Nazione<br>
            <select name="o_nazione" id="o_nazione" class="form-control">
              <?php
                if ( set_value('o_nazione') == '' ){
                  $forcedcty = 'ITALIA';
                } else {
                  $forcedcty = set_value('o_nazione');
                }
                foreach ( $this->config->item('country') AS $cty ){
                  $selected = '';

                  if ( $cty == $forcedcty ){
                    $selected = ' selected';
                  }
                  echo '<option value="'.$cty.'"'.$selected.'>'.$cty.'</option>';
                }
              ?>
            </select>
            <br>
            Telefono<br>
            <input type="text" name="o_telefono" id="o_telefono" value="<?php echo set_value('o_telefono');?>" class="form-control required" placeholder="Telefono *">
            <?php echo form_error('o_telefono'); ?>
            <br>
            Mobile<br>
            <input type="text" name="o_mobile" id="o_mobile" value="<?php echo set_value('o_mobile');?>" class="form-control required" placeholder="Mobile *">
            <?php echo form_error('o_mobile'); ?>
            <br>
            Codice Fiscale<br>
            <input type="text" name="o_cf" id="o_cf" value="<?php echo set_value('o_cf');?>" class="form-control required" placeholder="Codice Fiscale *">
            <?php echo form_error('o_cf'); ?>
            <br>
            P.IVA<br>
            <input type="text" name="o_piva" id="o_piva" value="<?php echo set_value('o_piva');?>" class="form-control required" placeholder="Partita IVA"><br>
            <br>
            <?php
              $redirect = '';
              if ( isset($_POST['redirect'] ) ){
                $redirect = $_POST['redirect'];
              }
            ?>
            <input type="hidden" name="redirect" value="<?=$redirect?>">
        </div>
        <div class="text-center col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <button class="btn get salva_profilo" data-status="<?=$profilo_salvato?>">Salva</button>
      </div>
    </form>
      </div>
    </div>
  </div>
</div>

</div>

<script>
$(document).ready(function(){

  if ( $('.salva_profilo').data('status') )  {
    doNotification('Il tuo account','Profilo salvato');
  }
});
</script>
