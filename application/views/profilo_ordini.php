<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">

<h2 class="title text-center">I tuoi Ordini</h2>
<table class="table table-striped table-bordered">
  <thead>
    <tr>
      <th>Ordine</th>
      <th>Data</th>
      <th>Pagamento</th>
      <th>Totale</th>
      <th>Stato</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
  <?php
  foreach ( $ordini AS $ord ){
    $stato = 'RICEVUTO';
    if ( $ord['ac_pagamento'] == "BONIFICO" && $ord['bl_status'] == 1 ){
      $stato = 'Bonifico non ricevuto';
    } else {
      if ( $ord['bl_status'] == 3 ){
        $stato = 'IN LAVORAZIONE';
      }
      if ( $ord['bl_status'] > 3 ){
        $stato = 'SPEDITO/EVASO';
      }
    }
    $ref = explode('/',$ord['ac_nrordine']);
    echo '
    <tr class="pointer">
      <td>
        '.$ord['ac_nrordine'].'
      </td>
      <td>
        '.$ord['data_ordine'].'
      </td>
      <td>
        '.$ord['ac_pagamento'].'
      </td>
      <td class="text-right">
        '.number_format((float)$ord['ac_totale'],2).'
      </td>
      <td>
        '.$stato.'
      </td>
      <td>
        <a href="'.base_url().'profilo/ordine/'.$ref[0].'/'.$ref[1].'"><span class="fa fa-search"></span></a>
      </td>
    </tr>';
    }
  ?>
</tbody>
</table>
</div>
