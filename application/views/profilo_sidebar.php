<div class="clearfix"></div>
<div class="col-lg-3 col-sm-3 col-xs-12 sito-sidebar">
  <h2 class="title"><?php echo $this->session->order['O_NOME'].' '.$this->session->order['O_COGNOME']?></h2>
  <div class="left-sidebar">
    <div class="panel-title shop-menu">
      <a href="<?=base_url()?>profilo/account" class="menu-categorie">Il mio account</a>
    </div>
    <div class="panel-title shop-menu">
      <a href="<?=base_url()?>profilo/ordini" class="menu-categorie">I miei ordini</a>
    </div>
    <div class="panel-title shop-menu">
      <a href="<?=base_url()?>profilo/password" class="menu-categorie">Modifica Password</a>
    </div>
    <div class="panel-title shop-menu">
      <a href="<?=base_url()?>logout" class="menu-categorie">Esci</a>
    </div>
  </div>
</div>
