<div id="paypal-button"></div>

<script src="https://www.paypalobjects.com/api/checkout.js"></script>

<script>
    paypal.Button.render({

        env: 'production', // Or 'production'

        client: {
            sandbox: 'AcvmsUTKhd_KkDaXZdINgc435AhfvuABsmUkwtbY9fzIUbC9M3i8Km-wPRipra5KAYXkoefUahsGtW5P',
            production: 'ARyhRZgzqHBYndlBQSHFLdQJxqs1PfrpRTIlEBEzYOJXu6--VaRdGA0PwhTewFmJavI4Mj81fgofxmwR',
            //sandbox:    'ATQR9ZoRlWTqRt1Z7vByk15Bn-RXvLn3xI7R722n2wm4Jq5C4uZnRz2fSKFTeXhj2jb5GF8YEMr2NRoK',
        },


				locale: 'it_IT',
				style: {
					shape: 'rect',
					color: 'blue'
				},


        commit: true, // Show a 'Pay Now' button

        payment: function(data, actions) {
            return actions.payment.create({
                payment: {
                    transactions: [
                        {
                            amount: { total: '<?=number_format($this->session->order['ORDER_TOTAL'],2)?>', currency: 'EUR' },
                            "invoice_number":  "<?=$this->session->order['id']?>"
                        }
                    ]
                }
            });
        },

        onAuthorize: function(data, actions) {
            return actions.payment.execute().then(function(payment) {
								location.href = "<?=base_url()?>ordine/paypal";
            });
        }

    }, '#paypal-button');
</script>
<!--
<form name="payPalFrm" id="payPalFrm" action="<?=$this->config->item('paypal_url')?>" method="post" target="_self">
	<input type="hidden" name="cmd" value="_cart">
	<input type="hidden" name="upload" value="1">
	<input type="hidden" name="business" value="<?=$this->config->item('paypal_user')?>">
	<input type="hidden" name="currency_code" value="EUR">
	<input type="hidden" name="custom" value="<?=$this->session->order['id']?>">
	<input type="hidden" name="sito" value="1">
	<input type="hidden" name="lc" value="IT">
	<input type="hidden" name="item_name_1" value="STICASA/STIKID ORDINE <?=$this->session->order['id']?>">
	<input type="hidden" name="item_number_1" value="<?=$this->session->order['id']?>">
	<input type="hidden" name="amount_1" id="amount_1" value="<?=number_format($this->session->order['ORDER_TOTAL'],2)?>">
	<input type="hidden" name="cancel" value="<?=$this->config->item('paypal_cancel')?>">
	<input type="hidden" name="return" value="<?=$this->config->item('paypal_return')?>">
	<input type="hidden" name="notify_url" value="<?=$this->config->item('paypal_notify')?>">
	<button class="btn get">PAGA ORA</button>
</form>
<script src="https://js.braintreegateway.com/v2/braintree.js"></script>
-->
