<?php
  //print_r ( $upsell );
 ?>
<div class="clearfix">
<div class="recommended_items col-lg-12 col-md-12 col-sm-12 col-xs-12"><!--recommended_items-->
  <h2 class="title text-center">I nostri clienti hanno scelto anche</h2>
    <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel" style="border:0px!important;">
      <div class="carousel-inner">
        <div class="item active">
          <?php
            $endrow = 4;
            if ( count($upsell) < 4 ){
              $endrow = count($upsell);
            }
            $n = 0;
            foreach ( $upsell AS $u ){
              if ( $n < $endrow ){
              $uri = base_url().''.uri_cat($u['ac_categoria'],0);
              if ( $u['id_lingua_exclusive'] == 1){
                $uri = base_url().''.uri_cat($u['ac_categoria_lang'],1);
              }
              $sconto = $u['ac_coefficiente_sconto'];
              $prezzo = calcolo_prezzo($p['ac_W_min'],$p['ac_H_min'],1);
              $prezzoOriginale = '';
              $imgSconto = '';
              if ( (float)$sconto < 1 ){
                $prezzoOriginale = $prezzo;
                $prezzo = calcolo_prezzo($p['ac_W_min'],$p['ac_H_min'],(float)$p['ac_coefficiente_sconto');

                $imgSconto = '<img src="//'.$this->config->item('static_url').'images/offerta-speciale-ecommerce.jpg" class="newarrival" alt="" style="margin-left:0px;width:50px;height:50px;z-index:9999;" />';
              }
              echo '
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="product-image-wrapper">
                  <div class="single-products">
                    <div class="productinfo text-center">
                      <img src="//'.$this->config->item('static_url').'images/ambienti/small/'.$u['ac_immagine'].'" alt="" />
                      <h2><small>da</small> &euro;
                      '.number_format($prezzo,2).'
                      </h2>
                      <p>'.$u['ac_prodotto'].'</p>
                      <a href="'.$uri.'" class="btn btn-default add-to-cart product-view" id="product-view" name="'.$u['id_prodotto'].'">
                      </a>
                      '.$imgSconto.'
                    </div>
                  </div>
                </div>
              </div>';

            }

            }
          ?>
        <!--
        <cfset start = 5>

          <cfloop index="i" from="#start#" to="#qryUpsell.recordcount#" step="4">
            <div class="item">
            <cfloop query="qryUpsell" startrow="#start#" endrow="#start+3#">
              <cfset uri = "#application.homedir##replace(ac_categoria,' ','-','ALL')#/sticker-#replace(ac_prodotto,' ','-','ALL')#/#id_prodotto#">

              <div class="col-sm-3">
                <div class="product-image-wrapper">
                  <div class="single-products">
                    <div class="productinfo text-center">
                      <img src="http://#application.static#images/ambienti/small/#ac_immagine#" alt="" />
                      <h2><small>da</small> &euro; #DecimalFormat(round(ac_prezzo*ac_coefficiente_sconto))#</h2>
                      <p>#Left(ac_prodotto,25)#</p>
                      <a href="#uri#" class="btn btn-default add-to-cart product-view" id="product-view" name="#id_prodotto#"><i class="fa fa-search"></i>#session.language.vedi#</a>
                                              <cfif ac_coefficiente_sconto LT 1>
                                                  <img src="//#application.static#images/offerta-speciale-ecommerce.jpg" class="newarrival" alt="" style="margin-left:0px;width:50px;height:50px;z-index:9999;" />
                                              </cfif>
                    </div>

                  </div>
                </div>
              </div>

            </cfloop>
            </div>
            <cfset start = start + 4>
          </cfloop>
        -->


      </div>
      <!--
              <cfif qryUpsell.recordcount GT 4>
      <a class="left recommended-item-control" href="##recommended-item-carousel" data-slide="prev">
      <i class="fa fa-angle-left"></i>
      </a>
      <a class="right recommended-item-control" href="##recommended-item-carousel" data-slide="next">
      <i class="fa fa-angle-right"></i>
      </a>
              </cfif>
            -->
    </div>
</div><!--/recommended_items-->
