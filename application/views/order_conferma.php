<div class="pay-information">
  <div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-8 text-center jumbotron">
      <h1 class="title">CONFERMA ORDINE</h1>
      <h2>Grazie per aver effettuato l'ordine</h2><br>
      <br>
      <p class="text-left">
      A breve riceverai una mail di conferma dell'ordine
      <br>
      <br>
      Per qualsiasi chiarimento o comunicazione puoi contattarci via mail al seguente indirizzo:<br>
      servizioclienti@sticasa.com
      </p>
    </div>
    <div class="col-sm-2"></div>
  </div>
</div>

<!-- Trustpilot conversion script -->
<script type="text/javascript"> (function(c,o,n,v,e,r,t){c['TPConversionObject']=e;c[e]=c[e]||function(){c[e].buid='51962fa40000640005319baf',(c[e].q=c[e].q||[]).push(arguments)};r=o.createElement(n),t=o.getElementsByTagName(n)[0];r.async=1;r.src=v;t.parentNode.insertBefore(r,t)})(window,document,'script','https://widget.trustpilot.com/conversion/conversion.js','tpConversion'); tpConversion('amount', "<?=$this->session->order['ORDER_TOTAL']?>"); // importo totale dell'ordine, ad es. '13.00' tpConversion('currency', 'EUR'); // la valuta del tuo negozio, ad esempio EUR tpConversion('basket_size', '1'); // numero totale di articoli acquistati </script> <!-- End Trustpilot conversion script -->
