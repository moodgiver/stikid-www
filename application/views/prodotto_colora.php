<?php
	$domain = 'stikid.com';
	$static_url = '//cdn.stikid.com/';
	if ( $this->config->item('sito') == 1 ){
		$domain = 'sticasa.com';
		$static_url = '//cdn.sticasa.com/';
	}
 ?>
	<div class="scelta-colori">

		<input type="hidden" id="url" class="url" value="<?=$static_url?>images/stickers/<?=$p['ac_codice_prodotto']?>x210.png">
		<div class="sticker" style="width:210px;height:210px">
			<canvas id="canvas"></canvas>
		</div>
		<h5 style="margin-top:20px">Colore sticker <span class="fa fa-arrow-right"></span> <span class="current-color">White</span></h5>
		<div class="clearfix" style="margin-top:10px">
		<?php
      foreach ( $colori AS $c ){
        echo '<div class="colore btn-action" data-action="color-canvas" data-hex="'.$c['ac_rgb'].'" data-color="'.$c['ac_colore'].'" name="'.$c['ac_rgb'].';'.$c['ac_colore'].'" style="background:#'.$c['ac_rgb'].';z-index:1000;border:1px solid #cecece;" title="'.$c['ac_colore'].'"></div>';
      }
     ?>
		</div>

		<h5>Colore superficie
      <input type="color" name="color" class="selector superficie" data-action="background-canvas" data-target=".sticker" value="#ffffff"/>
    </h5>
	</div>

<style>
#canvas { pointer-events:none; }
#canvas , .colore , .scelta-colori { z-index:2; }</style>
<script>
document.domain = "<?=$domain?>"
/*
function draw(r,g,b) {
        var canvas = document.getElementById('canvas');
        canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
        var ctx = canvas.getContext('2d');

        // create new image object to use as pattern
        var img = new Image();
				img.crossOrigin = "*";
        img.src = document.getElementById('url').value;
        img.onload = function(){
			ctx.drawImage(img,0,0);
			var imgd = ctx.getImageData(0, 0, 210, 210),
		    pix = imgd.data,
		    uniqueColor = [r,g,b]; // Blue for an example, can change this value to be anything.

			// Loops through all of the pixels and modifies the components.
			for (var i = 0, n = pix.length; i <n; i += 4) {
		      pix[i] = uniqueColor[0];   // Red component
		      pix[i+1] = uniqueColor[1]; // Blue component
		      pix[i+2] = uniqueColor[2]; // Green component
			}
			ctx.putImageData(imgd, 0, 0);

        }

      }

	  function HexToRGB(Hex){
	 	var Long = parseInt(Hex.replace(/^#/, ""), 16);
		return {
 			R: (Long >>> 16) & 0xff,
			G: (Long >>> 8) & 0xff,
			B: Long & 0xff
		 };
	  }
*/
$(document).ready ( function(){
	draw(0,0,0);
});

  /*
  $('.superficie').change ( function(){
  	console.log ( $(this).val() );
  	$('.sticker').css('background', $(this).val() );
  });
  */

</script>
