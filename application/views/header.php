<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
  <meta name="author" content="STIKID">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?=$title?></title>
	<?= meta($meta); ?>
  <link rel="shortcut icon" type="image/png" href="<?=base_url()?>favicon.ico"/>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

	<link href="<?=base_url()?>public/css/bootstrap.min.css" rel="stylesheet">
  <!--<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/public/css/font-awesome.min.css">-->
  <link href="<?=base_url()?>public/css/prettyPhoto.css" rel="stylesheet">
	<link href="<?=base_url()?>public/css/megamenu.css" rel="stylesheet">
  <link href="<?=base_url()?>public/css/price-range.css" rel="stylesheet">
  <link href="<?=base_url()?>public/css/animate.css" rel="stylesheet">
	<link href="<?=base_url()?>public/css/main.css" rel="stylesheet">
	<link href="<?=base_url()?>public/css/responsive.css" rel="stylesheet">
	<link href="<?=base_url()?>public/css/style.css" rel="stylesheet">
	<link href="<?=base_url()?>public/css/menuheader.css" rel="stylesheet">
	<link href="<?=base_url()?>public/js/lightbox/css/lightbox.css" rel="stylesheet" />
	<link rel="stylesheet" href="<?=base_url()?>public/js/jquery-ui.min.css">
	<link rel="stylesheet" href="<?=base_url()?>public/js/theme.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="<?=base_url()?>public/js/slider.js"></script>
	<script src="<?=base_url()?>public/js/gen_validatorv4.js"></script>
	<script src="https://use.fontawesome.com/12802e9216.js"></script>
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->


	<!-- TrustBox script -->

	<script type="text/javascript"src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js"async></script>

	<!-- End Trustbox script -->
	<?php
	foreach ( $header_hooks AS $script ){
		if ( $script['int_position'] == 1 ){
			if ( $script['type'] == 1 ){
				echo '<script>'.$script['ac_content'].'</script>';
			}
			if ( $script['type'] == 2 ){
				echo $script['ac_content'];
			}
		}
	}
	?>
</head>
<body style="width:100%;">
	<?php
	foreach ( $header_hooks AS $script ){
		if ( $script['int_position'] == 2 ){
			if ( $script['type'] == 1 ){
				echo '<script>'.$script['ac_content'].'</script>';
			}
			if ( $script['type'] == 2 ){
				echo $script['ac_content'];
			}
		}
	}
	?>
	<header id="header"><!--header-->
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row" style="padding:0px!important;">
					<?php
						include_once('header-content.php');
					?>
				</div>
			</div>
		</div>
	</header>
