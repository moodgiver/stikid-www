<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <h2 class="title">Reimposta password</h2>
  <div class="col-lg-3 col-md-3">
  </div>
  <div class="col-lg-6 col-md-6">
    <p>Stai per reimpostare la password per <label class="badge"><?php echo $user[0]['ac_email']; ?></label>
    </p>

  <?php
    if ( validation_errors() ){
      echo '<br><div class="alert alert-danger">'.validation_errors().'</div>';
    }
  ?>
  <form action="<?=base_url()?>password/modifica" method="post">
    <div class="form-group text-center">
      <label>Nuova password</label>
      <div class="input-group" style="max-width:50vw;">
        <input type="hidden" name="uuid" value="<?=$user[0]['password_reset_uuid']?>">
        <input type="password" name="password_change" class="form-control" placeholder="nuova password" value="<?=set_value('password_change')?>"/>
        <div class="input-group-addon email-input"><span class="fa fa-lock"></span></div>
      </div>
      <br>
      <label>Conferma nuova password</label>
      <div class="input-group" style="max-width:50vw;">
        <input type="password" name="password_change_confirm" class="form-control" placeholder="nuova password" value="<?=set_value('password_change_confirm')?>"/>
        <div class="input-group-addon email-input"><span class="fa fa-lock"></span></div>
      </div>
      <br>
      <button type="submit" class="btn get">Reimposta Password</button>

    </div>
  </form>
</div>
</div>
