<?php
  if ( count($banner_hooks) > 0 ){
    echo '<div class="col-lg-9 col-sm-9 col-xs-12">';
    foreach ( $banner_hooks AS $banner ){
      $uri = '#';
      if ( $banner['url'] != '' ){
        $uri = $banner['url'];
      }
      if ( $banner['filtro'] == 'all' || $banner['filtro'] == $mode ) {
        if ( $banner['posizione'] == 'header' ){
            echo '<a href="'.$uri.'"><img src="'.$this->config->item('static_url').'images/banners/'.$banner['banner'].'" class="clearfix" style="width:100%;height:auto;"></a>';
        }
      }
    }
    echo '</div>';
  }
?>
