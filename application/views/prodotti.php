
  <?php
    if ( $lista == 'venduti' ){
      echo '
      <div class="clearfix" style="margin:30px 0 30px 0;"></div>
      <h2 class="title text-center" >I pi&ugrave; venduti</h2>';
      $prodotti = $venduti;
    }
    if ( $lista == 'offerte' ){
      if ( $mode != 'offerte' ){
        echo '
        <div class="clearfix" style="margin:30px 0 30px 0;"></div>';
      }
      echo '<h2 class="title text-center" >Prodotti in offerta</h2>';
      $prodotti = $offerte;
    }

    if ( $lista == 'upsell' ){
      echo '
      <div class="clearfix" style="margin:30px 0 30px 0;"></div>';
      echo '<h2 class="title text-center" >I nostri clienti hanno scelto ancheofferta</h2>';
      $prodotti = $upsell;
    }
    if ( $lista == 'collezione' ){
      if ( isset($view) && $view == 'ricerca' ){
        echo '<h2 class="title text-center" >RICERCA - '.$_POST['search'].'</h2>';
      } else {
        if ( $prodotti_collezione[0]['ac_categoria_lang'] != '' ){
          echo '<h2 class="title text-center" >ADESIVI MURALI - '.$prodotti_collezione[0]['ac_categoria_lang'].'</h2>';
        } else {
          echo '<h2 class="title text-center" >ADESIVI MURALI '.$prodotti_collezione[0]['ac_categoria'].'</h2>';
        }
      }
      $prodotti = $prodotti_collezione;
    }


      foreach ( $prodotti AS $p ){

        /*$aImg = explode(',',$p['ac_immagini_demo']);
        $img = $aImg[0];
        if ( $img == '' ){
          $img = $aImg[1];
        }
        */
        $img = $p['ac_immagine'];
        $cat = str_replace(' ','-',$p['ac_categoria']);
        if ( $p['ac_categoria_lang'] != '' ){
          $cat = str_replace(' ','-',$p['ac_categoria_lang']);

        }
        $cat = strtolower($cat);
        //$cat = $p['ac_link'];
        $prod = str_replace(' ','-',$p['ac_prodotto']);
        $prod = strtolower(str_replace("''","",$prod));
        $scontato = 'color:#fff;';
        $prezzoOriginale = calcolo_prezzo($p['ac_W_min'],$p['ac_H_min'],1);
        $prezzoScontato = $prezzoOriginale;
        $scontoBox = '';
        if ( (float)$p['ac_coefficiente_sconto'] < 1 ){
          $scontato = 'text-decoration:line-through;font-size:1em;color:red;';
          $prezzoScontato = calcolo_prezzo($p['ac_W_min'],$p['ac_H_min'],(float)$p['ac_coefficiente_sconto']);
          $scontoBox = '<img src="'.$this->config->item('static_url').'images/offerta-speciale-ecommerce.jpg" class="newarrival" alt="" style="margin-left:0px;margin-top:0px;width:65px;height:65px" />';
        }
        $link = '<a href="'.base_url().''.$cat.'/'.$prod.'-adesivo-murale/'.$p['id_prodotto'].'">';
        echo '
        <div class="col-lg-3 col-sm-3 col-xs-6">
          <div class="product-image-wrapper collezione-home-offerta">
            '.$link.'
            <div class="single-products">
              <div class="productinfo text-center">
                <!-- immagine prodotto -->
                <img class="imgprodotto" src="'.$this->config->item('static_url').'images/ambienti/small/'.$p['ac_immagine'].'">
                <!-- prodotto -->
                <p itemprop="name" style="margin-bottom:2px;line-height:1em;margin-top:5px;height:35px;max-height:35px;">'.$p['ac_prodotto'].'</p>
                <!-- prezzo -->
                <h4 class="prezzo-scontato" style="min-height:4vh;margin-top:5px;color:#a9a9a9;max-height:15px">
                <h2 style="'.$scontato.'padding:0;margin-top:-10px;margin-bottom:-5px;line-height:10px;">da<span itemprop="priceCurrency" content="EUR"> &euro;  '.$prezzoOriginale.'</span>
                </h2>
                <h2 itemprop="price" style="max-height:25px;margin-top:5px;"><small>da</small><span itempropr="priceCurrency" content="EUR"> &euro; '.$prezzoScontato.'</span></h2>
                </h4>
                '.$scontoBox.'
              </div>
            </div>
            </a>
          </div>
        </div>';
        //<h4>'.$p['ac_prodotto'].'</h4>
      }

  ?>
  <script>
  $(document).ready(function(){
    var cw = $('.imgprodotto').width();
    $('.imgprodotto').css({'height':cw+'px'});
  });
  </script>
