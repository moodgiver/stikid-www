<header class="header" style="margin-top:30px;margin-bottom:30px;">
<nav id='cssmenu'>
<div id="head-mobile"></div>
<div class="button solo-mobile">
</div>
<ul style="">
<li class=''><a href='<?=base_url()?>'>Home</a></li>

<li><a href='#'>Adesivi Murali</a>
  <ul>
    <?php
      foreach ( $collezioni AS $c ){
        $cat = $c['ac_categoria'];
        $uri = uri_cat($cat,0);
        if ( $c['ac_categoria_lang'] != '' ){
          $cat = $c['ac_categoria_lang'];
          $uri = uri_cat($cat,1);
        }
        echo '<li><a href="'.base_url().''.$uri.'">'.$cat.'</a></li>';
      }
    ?>
  </ul>
</li>
<li><a href="/offerte">Offerte</a></li>
<li><a href="<?=base_url();?>pagina/Contatti/70"><span class="fa fa-envelope"></span> Contattaci</a></li>
<li class="solo-mobile"><a href="cart/conferma"><span class="fa fa-shopping-cart"></span> Carrello</a></li>
<?php
if ( $this->session->user['islogged'] ){
  echo '
  <li class="solo-mobile"><a href="'.base_url().'profilo/account"><span class="fa fa-user"></span> Il tuo account</a>
  </li>
  <li class="solo-mobile"><a href="'.base_url().'logout"><span class="fa fa-sign-out"></span>  Esci</a>';
} else {
  echo '
  <li class="solo-mobile"><a href="'.base_url().'login"><span class="fa fa-lock"></span> Accedi</a>
  </li>
  <li class="solo-mobile"><a href="'.base_url().'registrati"><span class="fa fa-user"></span>  Registrati</a>';
}
?>
</ul>
</nav>
</header>
