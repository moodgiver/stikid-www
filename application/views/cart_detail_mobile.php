<table class="table table-condensed table-cart mobile-cart">
  <tr>
    <td colspan="12">
      <h3>Il tuo carrello</h3>
  <?php
    $totale = 0;
    $totale_carrello = 0;
    foreach ( $this->session->cart_items AS $itm ){

      $prezzo = ((float)$item['qty']*(float)$item['prezzo']);
      $totale = $totale + $prezzo;
      $totale_carrello += (float)$itm['prezzo']*(float)$itm['qty'];
      echo '
      <div class="col-xs-7">
        <h5><a href="#uri#"><strong>'.$itm['prodotto'].'</strong></a></h5>'.
        $itm['collezione'].'<br>
        <strong>'.$itm['w'].' x '.$itm['h'].'height# cm</strong><br>
        '.$itm['colore'].'<h4>
        &euro; '.number_format($itm['prezzo'],2).'</h4>
      </div>
      <div class="col-xs-5 text-center" style="padding-top:20px">
        <div class="cart_quantity_button">
          <input type="hidden" class="item_qty_'.$itm['id'].'" value="'.$itm['qty'].'">
            <h4>
            <span class="fa fa-minus cart-btn  btn-qty-a" data-type="remove" data-id="'.$itm['id'].'" data-key="'.$itm['uuid'].'" data-price="'.$itm['prezzo'].'"></span>
            <span class="qty_'.$itm['id'].'">'.$itm['qty'].'</span>
            <span class="fa fa-plus cart-btn btn-qty-a" data-type="add" data-id="'.$itm['id'].'" data-key="'.$itm['uuid'].'" data-price="'.$itm['prezzo'].'"></span>
            </h4>
        </div>
      </div>';
      $totale_ordine = $totale + 3.90;
    }
    echo 'TOTALE '.$totale;
    echo '
  <div class="col-xs-6 text-right">
    <h4>Totale carrello</h4>
  </div>
  <div class="col-xs-6 text-right">
    <h4><span>&euro;</span> <span  class="totale_carrello">'.$totale.'</span></h4>
  </div>
  <div class="col-xs-6 text-right btn-input-sconto">
    <h4>Coupon</h4>
  </div>
  <div class="col-xs-6 text-right">
    <h4><span>&euro;</span> <span class="cart_coupon_discount">0.00</span></h4>
  </div>
  <div class="input-discount">
  <div class="col-xs-9">
    <input type="text" name="coupon" class="form-control coupon_codice" placeholder="Hai un codice sconto? Inseriscilo qui" size="6">
  </div>
  <div class="col-xs-3">
    <button class="btn btn-default btn-coupon-a">OK</button>
  </div>
  </div>

  <div class="col-xs-6 text-right">
    <h4>Spedizione</h4>
  </div>
  <div class="col-xs-6 text-right">
    <h4><span>&euro;</span> <span class="cart_shipment">0.00</span></h4>
  </div>
  <div class="col-xs-12">
  <p style="color:##000"></p>
  </div>


  <div class="col-xs-6">
    <h3>Totale</h3>
  </div>
  <div class="col-xs-6 text-right">
    <h3><span>&euro;</span> <span class="totale_ordine">'. number_format($totale_ordine,2).'</span></h3>
  </div>';
  ?>
</td>
</tr>
</table>
<style>
h4 , h3 { color: #999; }
h4 span , h3 span { color: #8BC53F; }
.cart-actions { text-align:center!important; }
.cart_coupon_discount { color: ##000; font-size:1em;}
.get { font-size:1em; }
.container { padding-left:0px!important; padding-right:0px!important; }
</style>
