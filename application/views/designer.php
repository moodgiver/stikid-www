
<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">

  <h2 class="title text-center">I DESIGNER - <?=$designer[0]['ac_nome_designer']?> <?=$designer[0]['ac_cognome_designer']?></h2>

  <div class="col-lg-3 col-xs-12">
    <img src="<?=$this->config->item('static_url')?>images/designers/<?=$designer[0]['ac_image_designer']?>" width="150" height="150">
  </div>
  <div class="col-lg-9 col-xs-12">
    <p><?=$designer[0]['ac_info']?></p>
    <p><em><?=$designer[0]['ac_descrizione']?></em></p>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix" style="margin-top:20px">

      <?php
        foreach ( $designer AS $item ){
          $uri = uri_prodotto($item['ac_categoria'],$item['ac_prodotto'],$item['id_prodotto'],0);
          if ( $item['ac_categoria_lang'] != '' ){
            $uri = uri_prodotto($item['ac_categoria_lang'],$item['ac_prodotto'],$item['id_prodotto'],1);
          }
          echo '
          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6" style="margin-bottom:10px;">
          <a href="'.base_url().''.$uri.'"><img src="'.$this->config->item('static_url').'images/ambienti/small/'.$item['ac_immagine'].'" border="1" alt="'.$item['ac_prodotto'].'" title="'.$item['ac_prodotto'].'" style="width:100%"></a>
          </div>
          ';
        }
      ?>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
    <a href="javascript:void(0)" onclick="history.back()"><button class="btn get">Torna alla lista designers</button></a>
  </div>
</div>
