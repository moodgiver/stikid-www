
<h5>Dati Cliente</h5>

<div class="form-three form-inline" data-toggle="tooltip" title="I campi contrassegnati in rosso sono obbligatori">

    <span class="text-danger">Nome</span><br>
    <input type="text" class="form-control" name="o_nome" id="o_nome" placeholder="Nome *" value="<?php echo $this->session->order['O_NOME']; ?>">
    <?php echo form_error('o_nome'); ?>
    <span class="text-danger">Cognome</span><br>
    <input type="text" class="form-control"  name="o_cognome" id="o_cognome"  placeholder="Cognome *" value="<?php echo $this->session->order['O_COGNOME']; ?>">
    <?php echo form_error('o_cognome'); ?>
    Azienda<br>
    <input type="text" name="o_azienda" id="o_azienda" value="<?php echo $this->session->order['O_AZIENDA']; ?>" class="form-control" placeholder="Azienda">
    <?php echo form_error('o_azienda'); ?>
    Email<br>
    <input type="text" name="o_email" id="o_email" value="<?php echo $this->session->order['O_EMAIL']; ?>" readonly class="form-control" placeholder="Email*">
    <?php echo form_error('o_email'); ?>
    <span class="text-danger">Indirizzo</span><br>
    <input type="text" name="o_indirizzo" id="o_indirizzo" value="<?php echo $this->session->order['O_INDIRIZZO']; ?>" class="form-control" placeholder="Indirizzo *">
    <?php echo form_error('o_indirizzo'); ?>
    <span class="text-danger">CAP</span><br>
    <input type="text" name="o_cap" id="o_cap" value="<?php echo $this->session->order['O_CAP'];?>" class="form-control" placeholder="CAP *"
    ><?php echo form_error('o_cap'); ?>
    <span class="text-danger">COMUNE</span><br>
    <input type="text" name="o_citta" id="o_citta" value="<?php echo $this->session->order['O_CITTA'];?>" class="form-control" placeholder="Citt&agrave; *" data-msg="Citta'">
    <?php echo form_error('o_citta'); ?>
    <span class="text-danger">Provincia</span><br>
    <select name="o_pv" id="o_pv" class="form-control">
      <option value="">Seleziona ...</option>
      <?php
        foreach ( $province AS $pv ){
          $selected = '';
          if ( $pv['ac_sigla'] == $this->session->order['O_PV'] ){
            $selected = ' selected';
          }
          echo '<option value="'.$pv['ac_sigla'].'"'.$selected.'>'.$pv['ac_provincia'].'</option>';
        }
      ?>
    </select>
    <?php echo form_error('o_pv'); ?>
    <br>
    <span class="text-danger">Nazione</span><br>
    <select name="o_nazione" id="o_nazione" class="form-control">
      <?php
        if ( $this->session->order['O_NAZIONE'] == '' ){
          $forcedcty = 'ITALIA';
        } else {
          $forcedcty = $this->session->order['O_NAZIONE'];
        }
        foreach ( $this->config->item('country') AS $cty ){
          $selected = '';

          if ( $cty == $forcedcty ){
            $selected = ' selected';
          }
          echo '<option value="'.$cty.'"'.$selected.'>'.$cty.'</option>';
        }
      ?>
    </select>
    <?php echo form_error('o_nazione'); ?>
    <br>
    Telefono<br>
    <input type="text" name="o_telefono" id="o_telefono" value="<?php echo $this->session->order['O_TELEFONO'];?>" class="form-control required" placeholder="Telefono">
    <?php echo form_error('o_telefono'); ?>
    <span class="text-danger">Mobile</span><br>
    <input type="text" name="o_mobile" id="o_mobile" value="<?php echo $this->session->order['O_MOBILE'];?>" class="form-control required" placeholder="Mobile *">
    <?php echo form_error('o_mobile'); ?>
    <span class="text-danger">Codice Fiscale</span><br>
    <input type="text" name="o_cf" id="o_cf" value="<?php echo $this->session->order['O_CF'];?>" class="form-control required" placeholder="Codice Fiscale *">
    <?php echo form_error('o_cf'); ?>
    P.IVA<br>
    <input type="text" name="o_piva" id="o_piva" value="<?php echo $this->session->order['O_PIVA'];?>" class="form-control required" placeholder="Partita IVA">
    <?php echo form_error('o_piva'); ?>
    <br>
    <br>
    <button class="btn btn-default btn-shipto">Spedisci ad altro indirizzo</button>

</div>
<script>
$(document).ready(function(){

  $('[data-toggle="tooltip"]').tooltip();

})
</script>
