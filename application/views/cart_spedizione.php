<h2>Spedizione Ordine
  <span class="shopper-order">
    <?php
    $time = time();
    echo $this->session->order['id'].'/'.mdate('%Y',$time);
    echo '</span> - &euro; <span class="importo_da_pagare">'.number_format($this->session->order['ORDER_TOTAL'],2).'</span><span class="shopper-order"></span>';
    $shipto = 0;
    if ( isset($_POST['o_shipto']) ){
      $shipto = $_POST['o_shipto'];
    }
    ?>
  </span>
</h2>
<form action="<?=base_url()?>cart/checkout" method="post" id="userInfo">
<div class="shopper-informations">
  <div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 clearfix">
      <div class="bill-to">
        <?php
          if ( $this->session->order['O_NOME'] != '' ){
            include_once('cart_spedizione_cliente.php');
          } else {
            include_once('cart_spedizione_registrazione.php');
          }
        ?>

      </div>
    </div>
    <?php
      include_once('cart_spedizione_extra.php');
    ?>

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <br><br>
    <textarea name="message" class="form-control" placeholder="Se hai qualche indicazione particolare scrivi qui" style="width:100%;height:40px;"></textarea>
  </div>
</div>

<div class="col-sm-12 text-center">
  <a href="conferma" class="get btn">Torna al carrello</a>
  <button type="submit" class="btn get">Acquista</button>
</div>
</form>
<style>
  .bill-to p { font-size:.8em; color:red; }
</style>
<script>
$(document).ready(function(){
  if ( $('.shipto').val() == 1 ){
    $('.ship-to').removeClass('hide');
  }
  $('.btn-shipto').on('click',function(e){
    e.preventDefault();
    if ( $('.ship-to').hasClass('hide') ){
      $('.ship-to').removeClass('hide');
      $('.shipto').val(1);
    } else {
      $('.ship-to').addClass('hide');
      $('.shipto').val(0);
    }
  });

});
</script>
<div class="clearfix"></div>
<br>
<br>
