<div class="jumbotron col-lg-9 col-md-9 col-sm-9 col-xs-12">
<h2>
  <?php
  echo $this->session->order['O_NOME'].' '.$this->session->order['O_COGNOME'];
  ?>
</h2>

<div class="shopper-informations">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
      <p>Il tuo account</p>
      <form action="salva" method="post">
      <div class="bill-to">
        <div class="form-three form-inline">
            Nome<br>
            <input type="text" class="form-control" name="o_nome" id="o_nome" placeholder="Nome *" value="<?php echo $this->session->order['O_NOME']; ?>">
            <?php echo form_error('o_nome'); ?>
            <br>
            Cognome<br>
            <input type="text" class="form-control"  name="o_cognome" id="o_cognome"  placeholder="Cognome *" value="<?php echo $this->session->order['O_COGNOME']; ?>">
            <?php echo form_error('o_cognome'); ?>
            <br>
            Azienda<br>
            <input type="text" name="o_azienda" id="o_azienda" value="<?php echo $this->session->order['O_AZIENDA']; ?>" class="form-control greenField" placeholder="Azienda">
            <?php echo form_error('o_azienda'); ?>
            <br>
            Email<br>
            <input type="text" name="o_email" id="o_email" value="<?php echo $this->session->order['O_EMAIL']; ?>" class="form-control" placeholder="Email*">
            <?php echo form_error('o_email'); ?>
            <br>
            Indirizzo<br>
            <input type="text" name="o_indirizzo" id="o_indirizzo" value="<?php echo $this->session->order['O_INDIRIZZO']; ?>" class="form-control" placeholder="Indirizzo *">
            <?php echo form_error('o_indirizzo'); ?>
            <br>
            CAP<br>
            <input type="text" name="o_cap" id="o_cap" value="<?php echo $this->session->order['O_CAP'];?>" class="form-control" placeholder="CAP *"
            ><?php echo form_error('o_cap'); ?>
            <br>
            COMUNE<br>
            <input type="text" name="o_citta" id="o_citta" value="<?php echo $this->session->order['O_CITTA'];?>" class="form-control" placeholder="Citt&agrave; *" data-msg="Citta'">
            <?php echo form_error('o_citta'); ?>
            <br>
            Provincia<br>
            <select name="o_pv" id="o_pv" class="form-control">
              <option value="">Seleziona ...</option>
              <?php
                foreach ( $province AS $pv ){
                  $selected = '';
                  if ( $pv['ac_sigla'] == $this->session->order['O_PV'] ){
                    $selected = ' selected';
                  }
                  echo '<option value="'.$pv['ac_sigla'].'"'.$selected.'>'.$pv['ac_provincia'].'</option>';
                }
              ?>
            </select>
            <br>
            Nazione<br>
            <select name="o_nazione" id="o_nazione" class="form-control">
              <?php
                if ( $this->session->order['O_NAZIONE'] == '' ){
                  $forcedcty = 'ITALIA';
                } else {
                  $forcedcty = $this->session->order['O_NAZIONE'];
                }
                foreach ( $this->config->item('country') AS $cty ){
                  $selected = '';

                  if ( $cty == $forcedcty ){
                    $selected = ' selected';
                  }
                  echo '<option value="'.$cty.'"'.$selected.'>'.$cty.'</option>';
                }
              ?>
            </select>
            <br>
            Telefono<br>
            <input type="text" name="o_telefono" id="o_telefono" value="<?php echo $this->session->order['O_TELEFONO'];?>" class="form-control required" placeholder="Telefono *"><br>
            Mobile<br>
            <input type="text" name="o_mobile" id="o_mobile" value="<?php echo $this->session->order['O_MOBILE'];?>" class="form-control required" placeholder="Mobile *"><br>
            Codice Fiscale<br>
            <input type="text" name="o_cf" id="o_cf" value="<?php echo $this->session->order['O_CF'];?>" class="form-control required" placeholder="Codice Fiscale *"><br>
            P.IVA<br>
            <input type="text" name="o_piva" id="o_piva" value="<?php echo $this->session->order['O_PIVA'];?>" class="form-control required" placeholder="Partita IVA"><br>
            <br>

        </div>
        <div class="text-center col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <button class="btn get salva_profilo" data-status="<?=$profilo_salvato?>">Salva</button>
      </div>
    </form>
      </div>
    </div>
  </div>
</div>
</div>

<script>
$(document).ready(function(){

  if ( $('.salva_profilo').data('status') )  {
    doNotification('Il tuo account','Profilo salvato');
  }
});
</script>
