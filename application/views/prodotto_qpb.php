<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <h4 class="title">
  <span class="collezione"><a href="<?=base_url()?>">HOME </a>
  <?php
    $cat = $prodotto[0]['ac_categoria'];
    $uriCat = uri_cat($prodotto[0]['ac_categoria'],2);
    if  ( $prodotto[0]['ac_categoria_lang'] != '' ){
      $cat = $prodotto[0]['ac_categoria_lang'];
      $uriCat = uri_cat($prodotto[0]['ac_categoria_lang'],2);
    }
    echo '<a href="'.base_url().''.$uriCat.'"><span class="fa fa-angle-right"></span> Quadri per bambini <span class="fa fa-angle-right"></span> '.$cat.'</a> <a href=""><span class="fa fa-angle-right"></span> '.$prodotto[0]['ac_prodotto'].'</a>';
  ?>
  </h4>
</div>
<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
  <?php
    $static = $this->config->item('static_url');
    $p = $prodotto[0];
    $promo = '';
    if ( (float)$prodotto[0]['ac_coefficiente_sconto'] < 1 ){
      $promo = '<img src="'.$static.'images/offerta-speciale-ecommerce.jpg" class="newarrival" alt="" style="margin-left:15px" />';
    }
    echo '
    <img id="product-image" src="'.$this->config->item('static_url').'images/canvas/preview/'.$p['ac_immagine'].'" alt="'.$p['ac_prodotto'].'" title="'.$p['ac_prodotto'].'" class="immagine-evidenza" data-lighbox="stikid">';
    include_once ( 'prodotto_gallery.php' );
    echo '<h4>'.$p['ac_prodotto'].'</h4>';
    echo 'by <a href="'.base_url().'designers/'.$p['designer'].'/'.$p['id_designer'].'"><em>'.$p['designer'].'</em></a></p>
    <p>Codice: '.$p['ac_codice_prodotto'].'</p>
    ';
    echo $promo;
  ?>
</div>
<?php
  include_once ( 'prodotto_qpb_sidebar.php' );
  echo '<div class="clearfix"></div><div class="col-lg-12">';
  include_once ( 'prodotto_tabs_qpb.php' );
  echo '</div>'
?>
