<br><br>
<!-- Nav tabs -->

<ul class="nav nav-tabs" role="tablist">
  <?php
    $s = 1;
    $active = 'active';
    foreach ( $tabs AS $tab ){
      echo ' <li role="presentation" class="'.$active.'"><a href="#tab_'.$s.'" aria-controls="tab_'.$s.'" role="tab" data-toggle="tab">'.$tab['ac_titolo'].'</a></li>';
      $s++;
      $active = '';
    }
  ?>
</ul>

<!-- Tab panes -->
<div class="tab-content">
<?php
  $active = 'active';
  $riga = 1;
  $fade = '';
  foreach ( $tabs AS $tab ){
    echo '<div role="tabpanel" class="tab-pane '.$fade.''.$active.'" id="tab_'.$riga.'"><div class="col-lg-12 tab-content">'.$tab['ac_testo'].'</div></div>';
    $riga++;
    $active = '';
    $fade = 'fade ';
  }
?>
</div>
