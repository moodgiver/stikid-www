
<section id="cart_items">

  <div class="container" style="left:0;width:100%;">
    <div class="table-responsive cart_info">
      <table class="table table-condensed table-cart  desktop-cart">
        <thead>
          <tr class="cart_heading">
            <td class="image mobile-hide">Prodotto</td>
            <td class="description"></td>
            <td class="price">Prezzo</td>
            <td class="quantity">Q.t&agrave;</td>
            <td class="total">Totale &euro;</td>
            <td></td>
          </tr>
        </thead>
        <tbody class="cart_body">
          <?php
            $cart = $_SESSION['cart_items'];
            $static = $this->config->item('static_url');
            $totale = 0;

            foreach ( $cart AS $item ){

              $prezzo = ((float)$item['qty']*(float)$item['prezzo']);
              $totale = $totale + $prezzo;
              $telaio = '';
              $folder = 'ambienti/small/';
              if ( $item['telaio'] != '' ){
                $telaio = "Telaio ".$item['telaio'].' cm';
                $folder = 'canvas/preview/';
              }
              echo '
              <tr>
                <td style="width:10%"><img src="'.$static.'images/'.$folder.''.$item['thumb'].'" style="width:100%"/></td>
                <td>
                  <h4>'.$item['prodotto'].'</h4>
                  <p>'.$item['collezione'].'</p>
                  <p>'.$item['colore'].'</p>
                </td>
                <td>
                  <strong>'.$item['w'].' x '.$item['h'].' cm</strong>
                  <p>'.$telaio.'</p>
                </td>
                <td>
                <div class="cart_quantity_button">
                  <input type="hidden" class="item_qty_'.$item['id'].'" value="#itm.qty#">
                <span class="fa fa-minus cart-btn  btn-qty-a" data-type="remove" data-id="'.$item['id'].'" data-key="'.$item['uuid'].'" data-price="'.$item['prezzo'].'"></span>
                  <span class="qty_'.$item['id'].'">'.$item['qty'].'</span>
                <span class="fa fa-plus cart-btn btn-qty-a" data-type="add" data-id="'.$item['id'].'" data-key="'.$item['uuid'].'" data-price="'.$item['prezzo'].'"></span>
                x '.number_format($item['prezzo'],2).'
                </div>

                </td>
                <td class="text-right">
                  <h4 class="prezzo_val total_item total_'.$item['id'].'">'.number_format($prezzo,2).'</h4>
                </td>
              </tr>
              ';
            }
            $totale_ordine = $totale + 3.90;
          ?>
          <!--- totale carrello --->

          <tr class="cart_total">
            <td class="image"></td>
            <td class="description"></td>
            <td class="price"></td>
            <td class="quantity text-right">Totale</td>
            <td class="total text-right"><h3 class="totale_carrello"><?=$totale?></h3></td>
            <td></td>
          </tr>

          <!--- coupon --->
          <tr class="cart_total">
            <td class="image"></td>
            <td class="cart_description text-right">
               <input type="text" name="coupon" class="form-control coupon_codice" placeholder="Hai un codice sconto? Inseriscilo qui" size="6">
            </td>
            <td class="cart_price">
              <button class="btn btn-default btn-coupon-a">OK</button>
            </td>
            <td class="quantity text-right">
              Coupon
            </td>
            <td class="cart_price text-right">
              <input type="hidden" class="cart_coupon_value" value="0">
               <p class="cart_coupon_discount">0.00</p>
            </td>
            <td class="cart_delete">

            </td>
          </tr>
          <!--- spedizione --->
          <tr class="cart_total">

            <td colspan="2">
            <p style="color:##000">
                <!--
                <cfif>
                <small class="limite_spese_spedizione" style="color:##ff0000">
                Ti mancano ancora &euro; <span class="quanto_manca_valore">#DecimalFormat(quanto_manca)#</span> per avere le spese di spedizione GRATUITE
                </small>
                <cfelse>
                Spese spedizione GRATUITE
                </cfif>
              -->
                </p>
            </td>

            <td class="cart_price">
            </td>
            <td class="quantity text-right">
              Spedizione
            </td>
            <td class="cart_price text-right">
               <p class="cart_shipment">0.00</p>
            </td>
            <td class="cart_delete">

            </td>
          </tr>

          <!--- totale ordine --->
          <cfset totale_ordine = totale_carrello + totale_spedizione - totale_coupon>
          <tr class="cart_total">
            <td class="image"></td>
            <td class="description"></td>
            <td class="price"></td>
            <td class="quantity text-right">Totale</td>
            <td class="total text-right"><h2 class="totale_ordine"><?=$totale_ordine?></h2></td>
            <td></td>
          </tr>

        </tbody>
      </table>

      <input type="hidden" class="tot_carrello" name="totale_carrello" value="<?=$totale?>">
      <input type="hidden" class="tot_spedizione" name="totale_spedizione" value="3.90">
      <input type="hidden" class="tot_coupon" name="totale_coupon" value="0">
      <input type="hidden" class="tot_ordine" name="totale_ordine" value="<?=$totale_ordine?>">
      <input type="hidden" class="quanto_manca" value="0">
      <input type="hidden" class="limite_spesa" value="0">
    </div>

    <div class="col-sm-12 col-xs-12 cart-actions text-center clearfix">
      <a href="<?=base_url()?>"><button class="btn btn-default btn-shop get">Continua shopping</button></a>
      <a href="<?=$static?>cart/spedizione"><button class="btn btn-default btn-goto-checkout get">Ordina</button></a>
      <br><br>

    </div>
  </div>
</section>

<script>
$(document).ready(function(){
  var w = $(window).width();
  if ( w < 640 ){
    $('.mobile-cart').removeClass('hidden');
    $('.desktop-cart').remove();
  } else {
    $('.mobile-cart').remove();
  }
  ricalcolaTotali();

  $('.btn-coupon-a').click(function(){
    ricalcolaCoupon();
  });
  $('.btn-qty-a').on('click',function(e){
    e.preventDefault();
    var tot_cart = $('.totale_carrello');
    var tot_sped = $('.totale_spedizione');
    var tot_coup = $('.totale_coupon');
    var tot_ord = $('.totale_ordine');
    var id = $(this).data('id');
    var uuid = $(this).data('key');
    var qty = parseInt($('.qty_' + id ).html());
    var prezzo = parseFloat($(this).data('price'));
    var totale_carrello = parseFloat($('.totale_carrello').val());
    var totale_ordine = parseFloat($('.totale_ordine').val());
    console.log ( 'quantita =>' , qty );
    if ( $(this).data('type') == 'add' ){
      qty++;
    } else {
      qty--;
    }
    $.post ( routing ,
      {
        mode: 'cart-item-qty',
        qty: qty,
        uuid: uuid
      },function(response){
        if ( parseInt(qty) < 1 ){
          location.reload();
          //$('.riga_' + uuid).remove();
          //ricalcolaTotali();
        } else {

          $('.qty_' + id).html(qty);
          var totale_riga = prezzo*qty;
          $('.total_' + id).html(totale_riga.toFixed(2));
          ricalcolaTotali();
        }
      }
    );
  });

  function ricalcolaTotali(){
    var totale = 0;
    $('.total_item').each ( function(){
      totale += parseFloat($(this).html());
    });
    console.log ( totale );
    $('.totale_carrello').html(totale.toFixed(2));
    $('.tot_carrello').val(totale);
    ricalcolaCoupon();
  }

  function ricalcolaCoupon(){
    var coupon = $('.coupon_codice').val();
    var totale_da_scontare = $('.tot_carrello').val();
    if ( coupon != '' ){

      $.post ( routing ,
      {
        mode: 'coupon',
        coupon: coupon,
        totale: totale_da_scontare
      }, function ( discount ){
        console.log ( "sconto" , eval(discount) );
        if ( eval(discount) > 0 ){
          coupon_value = (totale_da_scontare*discount)/100;
          $('.cart_coupon_discount').html ( coupon_value.toFixed(2));
          $('.tot_coupon').val(coupon_value);
          ricalcolaSpedizione(totale_da_scontare);
        } else {
          $('.tot_coupon').val('0');
          $('.cart_coupon_discount').html ('0.00');
          ricalcolaSpedizione(totale_da_scontare);
          doNotification ( "Coupon" , "Coupon non valido" );
        }
      }
      );
    } else {
      ricalcolaSpedizione(totale_da_scontare);
    }

  }

  function  ricalcolaSpedizione(totale_da_scontare){
    console.log ( totale_da_scontare );
    $.post ( routing ,
      {
        mode: 'spedizioni',
        totale: totale_da_scontare
      }, function ( result ){
        console.log ( result );
        var spedizioni = result.trim().replace('\t','');
        $('.cart_shipment').html ( parseFloat(spedizioni).toFixed(2) );
        if ( parseFloat(spedizioni) == 0 ){
          $('.limite_spese_spedizione').html('');
        } else {
          var limite = parseFloat($('.limite_spesa').val());
          var manca = parseFloat(limite - parseFloat(totale_da_scontare));
          $('.limite_spese_spedizione').html('Ti mancano ancora &euro;' + manca.toFixed(2) + ' per avere le spese di spedizione GRATUITE');
        }
        $('.tot_spedizione').val(spedizioni);
        ricalcolaTotale();
      }
    );
  }

  function ricalcolaTotale(){
    var totale_ordine = 0;
    totale_ordine = parseFloat($('.tot_carrello').val()) + parseFloat($('.tot_spedizione').val()) - parseFloat($('.tot_coupon').val());
    $('.totale_ordine').html ( totale_ordine.toFixed(2) );
    $('.tot_ordine').val(totale_ordine);
    var myOrdine = {
      carrello: parseFloat($('.tot_carrello').val()),
      spedizione: parseFloat($('.tot_spedizione').val()),
      coupon: parseFloat($('.tot_coupon').val()),
      coupon_code: $('.coupon_codice').val(),
      ordine: totale_ordine
    }

    sessionStorage.setItem('stikid-ordine',JSON.stringify(myOrdine));

    $.post ( routing ,
      {
        mode: 'preorder-cart',
        carrello: parseFloat($('.tot_carrello').val()),
        spedizione: parseFloat($('.tot_spedizione').val()),
        coupon_value: parseFloat($('.tot_coupon').val()),
        coupon: $('.coupon_codice').val(),
        totale: totale_ordine
      }
    );

  }
});
</script>
