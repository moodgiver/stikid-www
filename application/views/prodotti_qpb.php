
  <?php

      if ( $prodotti_collezione[0]['ac_categoria_lang'] != '' ){
        echo '<h2 class="title text-center" >QUADRI PER BAMBINI - '.$prodotti_collezione[0]['ac_categoria_lang'].'</h2>';
      } else {
        echo '<h2 class="title text-center">QUADRI PER BAMBINI - '.$prodotti_collezione[0]['ac_categoria'].'</h2>';
      }
      $prodotti = $prodotti_collezione;



      foreach ( $prodotti AS $p ){

        $img = $p['ac_immagine'];
        $cat = str_replace(' ','-',$p['ac_categoria']);
        if ( $p['ac_categoria_lang'] != '' ){
          $cat = str_replace(' ','-',$p['ac_categoria_lang']);
        }
        $prod = str_replace(' ','-',$p['ac_prodotto']);
        $prod = str_replace("''","",$prod);
        $scontato = 'display:none;';
        //$prezzoOriginale = $p['ac_prezzo'];
        $prezzoOriginale = prezzo_base_qpb($p['ac_materiale']);
        $prezzoScontato = $prezzoOriginale;
        $scontoBox = '';
        if ( (float)$p['ac_coefficiente_sconto'] < 1 ){
          $scontato = 'text-decoration:line-through;font-size:.9em;';
          $prezzoScontato = $prezzoOriginale*(float)$p['ac_coefficiente_sconto'];
          $scontoBox = '<img src="'.$this->config->item('static_url').'images/offerta-speciale-ecommerce.jpg" class="newarrival" alt="" style="margin-left:0px;margin-top:0px;width:65px;height:65px" />';
        }
        $link = '<a href="'.base_url().''.$cat.'/'.$prod.'-quadro-per-bambino/'.$p['id_prodotto'].'">';
        echo '
        <div class="col-lg-3 col-sm-3 col-xs-6">
          <div class="product-image-wrapper collezione-home-offerta">
            '.$link.'
            <div class="single-products">
              <div class="productinfo text-center">
                <!-- immagine prodotto -->
                <img class="imgprodotto" src="'.$this->config->item('static_url').'images/canvas/preview/'.$img.'">
                <!-- prodotto -->
                <p itemprop="name" style="margin-bottom:2px;line-height:1em;margin-top:5px;height:20px;max-height:20px;">'.$p['ac_prodotto'].'</p>
                <!-- prezzo -->
                <h4 class="prezzo-scontato" style="min-height:4vh;margin-top:5px;color:#a9a9a9;min-height:10vh">
                <h2 style="'.$scontato.'padding:0;margin-top:-10px;margin-bottom:-5px;color:#999;line-height:10px;"><small>da</small><span itemprop="priceCurrency" content="EUR"> &euro;  '.number_format($prezzoOriginale,2).'</span>
                </h2>
                <h2 itemprop="price" style="min-height:4vh;margin-top:5px;"><small>da</small><span itempropr="priceCurrency" content="EUR"> &euro; '.number_format($prezzoScontato,2).'</span></h2>
                </h4>
                '.$scontoBox.'
              </div>
            </div>
            </a>
          </div>
        </div>';
        //<h4>'.$p['ac_prodotto'].'</h4>
      }

  ?>
  <script>
  $(document).ready(function(){
    var cw = $('.imgprodotto').width();
    $('.imgprodotto').css({'height':cw+'px'});
  });
  </script>
