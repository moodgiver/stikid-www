<div class="col-lg-3 col-sm-3 col-xs-12 sito-sidebar">
  <div class="left-sidebar" style="margin-left:-15px;">
    <?php
      if ( !$this->config->item('isqpb') ){
        echo '<h2 class="title">ADESIVI MURALI</h2>';
      foreach ( $collezioni AS $collezione ){
        //$adesivi = 'wall-stickers';
        //$link = str_replace(" ","-",$collezione['ac_categoria']);
        $c = $collezione['ac_categoria'];
        if ( $collezione['ac_categoria_lang'] != '' ){
        //  $adesivi = 'adesivi-murali';
        //  $link = str_replace(" ","-",$collezione['ac_categoria_lang']);
          $c = $collezione['ac_categoria_lang'];
        }
        $link = $collezione['ac_link'];
        echo '<div class="panel-title shop-menu">
      		<a href="'.base_url().$link.'" class="menu-categorie" name="'.$collezione['id_categoria'].';'.$c.'">'.$c.'</a>
      	</div>';
      }
      echo '<br><br>';
      }
    ?>
    <div class="clearfix"></div>


    <?php
      if ( $this->config->item('sito') == 4 ){
        echo '<h2>Quadri per bambini</h2>';
      foreach ( $collezioni_qpb AS $collezione ){
        $tipo = 'quadri-per-bambini';
        $link = str_replace(" ","-",$collezione['ac_categoria']);
        $c = $collezione['ac_categoria'];
        if ( $collezione['ac_categoria_lang'] != '' ){
          $adesivi = 'adesivi-murali';
          $link = str_replace(" ","-",$collezione['ac_categoria_lang']);
          $c = $collezione['ac_categoria_lang'];
        }
        $link = $collezione['ac_link'];
        echo '<div class="panel-title shop-menu">
      		<a href="'.base_url().$link.'" class="menu-categorie" name="'.$collezione['id_categoria'].';'.$c.'">'.$c.'</a>
      	</div>';
      }
    }

    ?>
    <div class="clearfix"></div>
    <br><br>
    <?php

      foreach ( $sidebar_menu AS $menu ){
        $link = base_url().'pagina/'.$menu['ac_url'];
        if ( $menu['ac_livello'] == -1 ){
          $link = 'http:'.$menu['ac_url'];
        }
        echo '<div class="panel-title shop-menu"><a href="'.$link.'" class="top-menu">'.$menu['ac_titolo'].'</a></div>';

      }

      if ( count($banner_hooks) > 0 ){
        echo '<div class="col-lg-12 text-center" style="margin-top:30px;">';
        foreach ( $banner_hooks AS $banner ){
          if ( $banner['posizione'] == 'sidebar' && ($banner['int_sito'] == $this->config->item('sito') || $banner['int_sito'] == 0) ){
            $uri = '#';
            if ( $banner['url'] != '' ){
              $uri = $banner['url'];
            }
            echo '<a href="'.$uri.'"><img src="'.$this->config->item('static_url').'images/banners/'.$banner['banner'].'" class="clearfix" style="width:100%;height:auto;"></a>';
          }
        }
        echo '</div>';
      }

     ?>


  </div>

</div>
