<?php
  $cart = $this->session->cart_items;
  $order = $this->session->order;
foreach ( $cart AS $itm ){
  $imgURL = 'http://static.stikid.com/images/ambienti/small/';
  $telaio = '';
  if ( $itm['telaio'] != '' ){
    $imgURL = 'http://static.stikid.com/images/canvas/preview/';
    $telaio = '<br>Telaio '.$itm['telaio'].' cm';
  }
  $totale_riga = number_format((float)$itm['qty']*(float)$itm['prezzo'],2);
  $content = '';
  $content .= '
      <tr>
        <td width="110">
          <img src="'.$imgURL.''.$itm['thumb'].'" width="100">
        </td>
        <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top">'.$itm['prodotto'].'<br><small>Collezione: '.$itm['collezione'].'</small>'.str_replace('|','<br>',$itm['testo']).'</td>
        <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top">'.$itm['colore'].'</td>
        <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top"><cfif itm.telaio EQ "">'.$itm['w'].' x '.$itm['h'].''.$telaio.'</td>
        <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="center">'.$itm['qty'].'</td>
        <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">'.$itm['prezzo'].'</td>
        <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">'.$totale_riga.'</td>
      </tr>
      <tr>
        <td colspan="7"><hr></td>
      </tr>';
}


$message = '
<!DOCUMENT html>
<html>
<head>
  <link href="http://www.stikid.com/css/style.css" rel="stylesheet">
  <style>
    td , th , tbody , thead { font-family:Verdana,Helvetica; font-size:1em;}
    .col-center { text-align:center; }
    .col-right { text-align:right; }
  </style>
</head>
<body style="background:#fff">
<img src="http://static.stikid.com/images/email-logo.jpg" alt="Home" title="Home"/>
<h2>STICASA/STIKID Ordine nr. '.$order['id'].'/'.this_year().'</h2>
<table width="100%" cellpadding="4" cellspacing="0" border="0" style="border:1px solid ##cecece;background:#fff">
  <thead style="background:##cecece">
    <th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"><strong>Prodotto</strong></th>
    <th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"></th>
    <th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"><strong>Colore</strong></th>
    <th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"><strong>LxA cm</strong></th>
    <th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"><strong>Q.ta\'</strong></th>
    <th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"><strong>P.U.</strong></th>
    <th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"><strong>Totale</strong></th>
  </thead>
  <tbody class="print">';
      $message .= $content;

      $message .= '
      <tr>
        <td colspan="6" align="right" style="font-size:1em;font-family:Verdana,Helvetica" valign="top">Totale</td>
        <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">'.number_format($order['CART_TOTAL'],2).'</td>
      </tr>
      <tr>
        <td colspan="6" style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">Coupon</td>
        <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">'.number_format($order['COUPON_VALUE'],2).'</td>
      </tr>
      <tr>
        <td colspan="6" style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">Totale</td>
        <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">'.number_format($order['SHIPPING'],2).'</td>
      </tr>
      <tr>
        <td colspan="6" style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">Totale</td>
        <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">'.number_format($order['ORDER_TOTAL'],2).'</td>
      </tr>
      <tr>
        <td colspan="7"><hr></td>
      </tr>

  </tbody>
  <tfoot class="print-footer">
    <tr>
      <td colspan="7" style="font-size:1em;font-family:Verdana,Helvetica" valign="top"><h3>Cliente</h3>
      '.$order['O_NOME'].'&nbsp;'.$order['O_COGNOME'].'<br>
      '.$order['O_INDIRIZZO'].'<br>
      '.$order['O_CAP'].' '.$order['O_CITTA'].' '.$order['O_PV'].' '.$order['O_NAZIONE'].'<br>
      Telefono: '.$order['O_TELEFONO'].'<br>
      Cellulare: '.$order['O_MOBILE'].'
      </td>
    </tr>';
    if ( $order['SHIPTO'] ){
      $message .= '
      <tr>
      <td colspan="7" style="font-size:1em;font-family:Verdana,Helvetica" valign="top"><h3>Spedisci a</h3>
      '.$ordine_summary[0]['ac_firstname_delivery'].' '.$ordine_summary[0]['ac_lastname_delivery'].'<br>
      '.$ordine_summary[0]['ac_address_delivery'].'<br>
      '.$ordine_summary[0]['ac_zip_delivery'].' '.$ordine_summary[0]['ac_city_delivery'].' '.$ordine_summary[0]['ac_state_delivery'].' '.$ordine_summary[0]['ac_country_delivery'].'<br>
      Telefono: '.$ordine_summary[0]['ac_phone_delivery'].'<br>
      Cellulare: '.$ordine_summary[0]['ac_mobile_delivery'].'
      </td>
      </tr>';
    }
    $message .= '<tr>
      <td colspan="7" style="font-size:1em;font-family:Verdana,Helvetica" valign="top">
      <br><br>
      Per qualsiasi richiesta o chiarimento si prega di contattare l\'assistenza clienti scrivendo un\'e-mail a  servizioclienti@sticasa.com e indicando il numero d\'ordine nr. '.$order['id'].'/'.this_year().'
      </td>
    </tr>';
    if ( $pagamento[0] == 'BONIFICO' ){
      $message .= '
        <tr>
          <td colspan="7"  style="font-size:1em;font-family:Verdana,Helvetica" valign="top">
          <small>Dati Bonifico (per pagamenti tramite bonifico)<br>
          <strong>DUAL SRL - IBAN IT64Q0200801634000102842164 - UNICREDIT</strong><br>
          Indicare nel bonifico</br>
          ORDINE STICASA/STIKID NR. '.$order['id'].'/'.this_year().'</small>
          </td>
        </tr>';
    } else {
      $message .= '
      <tr>
      <td colspan="7"  style="font-size:1em;font-family:Verdana,Helvetica" valign="top">
      PAGAMENTO: CC/PAYPAL
      </td>
      </tr>';
    }
$message .= '</tfoot>
</table>
</body>
</html>';
echo $message;
?>
