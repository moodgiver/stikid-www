<h2>Pagamento Ordine
  <?php
    $time = time();
    echo $this->session->order['id'].'/'.mdate('%Y',$time);
    echo '</span> - &euro; <span class="importo_da_pagare">'.number_format($this->session->order['ORDER_TOTAL'],2).'</span><span class="shopper-order"></span>';
  ?>
</h2>
<div class="pay-information hide">
  <div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-3 text-center">
      <div class="jumbotron get">
        <h3><span class="fa fa-map-o"></span> BONIFICO</h3>
      </div>
      <a href="<?=base_url();?>ordine/bonifico" class="maschera"><button class="btn get"  data-action="bonifico" data-order="<?=$this->session->order['id']?>" data-userid="<?=$this->session->user['customer_id']?>">ORDINA ADESSO</button></a>
      <p>
      <br><br>

      </p>
    </div>
    <div class="col-sm-2"></div>
    <div class="col-sm-3 text-center">
      <div class="jumbotron get">
        <h2><span class="fa fa-cc-visa"></span> <span class="fa fa-cc-mastercard"></span> <span class="fa fa-cc-paypal"></span></h2>
      </div>
      <?php
        include_once ( 'cart_paypal.php' );
      ?>
      <!--- <cfinclude template="/bin/templates/view/view.cart.2checkout.cfm"> --->
      <p>
      <br>
      <br>
      Paga direttamente con la tua carta di credito attraverso PayPal<br>
      <!--
      Maggiori informazioni <a href="#application.homedir#pagina/pagamenti">clicca qui</a>-->
      </p>
    </div>
    <div class="col-sm-2"></div>
  </div>
</div>
<div style="position:fixed;top:0;left:0;width:100%;height:100%;background:#444;opacity:.8" class="hide" id="maschera">
  <div align="center">
    <h3 style="color:white">Registrazione ordine in corso ...</h3>
  </div>
</div>
<script>
$(document).ready (function(){
  $('.pay-information').removeClass('hide');
  $('.maschera').on('click',function(){
    $('#maschera').removeClass('hide');
  })
})
</script>
