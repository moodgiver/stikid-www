
  <?php

    if ( $lista == 'promo' ){
      if ( $mode != 'promo' ){
        echo '
        <div class="clearfix" style="margin:30px 0 30px 0;"></div>';
      }
      echo '<h2 class="title text-center" >Promozione</h2>';
      $prodotti = $promo;
    }


      foreach ( $prodotti AS $p ){

        $img = $p['ac_immagine'];
        $cat = str_replace(' ','-',$p['ac_categoria']);
        if ( $p['ac_categoria_lang'] != '' ){
          $cat = str_replace(' ','-',$p['ac_categoria_lang']);

        }
        $cat = strtolower($cat);
        //$cat = $p['ac_link'];
        $prod = str_replace(' ','-',$p['ac_prodotto']);
        $prod = strtolower(str_replace("''","",$prod));
        $scontato = 'color:#fff;';
        $prezzoOriginale = calcolo_prezzo($p['ac_W_min'],$p['ac_H_min'],1);
        $prezzoScontato = $prezzoOriginale;
        $qty_available = $p['available_promo'];
        $scontoBox = '';
        if ( (float)$p['sconto_promo'] < 1 ){
          $scontato = 'text-decoration:line-through;font-size:1em;color:red;';
          $prezzoScontato = number_format((int)($prezzoOriginale*(float)$p['sconto_promo']),2);
          $scontoBox = '<img src="'.$this->config->item('static_url').'images/offerta-speciale-ecommerce.jpg" class="newarrival" alt="" style="margin-left:0px;margin-top:0px;width:65px;height:65px" />';
        }
        $link = '<a href="'.base_url().''.$cat.'/'.$prod.'-adesivo-murale/'.$p['id_prodotto'].'">';
        echo '
        <div class="col-lg-3 col-sm-3 col-xs-6">
          <div class="product-image-wrapper collezione-home-offerta">
            '.$link.'
            <div class="single-products">
              <div class="productinfo text-center">
                <!-- immagine prodotto -->
                <img class="imgprodotto" src="'.$this->config->item('static_url').'images/ambienti/small/'.$p['ac_immagine'].'">
                <!-- prodotto -->
                <p itemprop="name" style="margin-bottom:2px;line-height:1em;margin-top:5px;height:35px;max-height:35px;">'.$p['ac_prodotto'].'</p>
                <!-- prezzo -->
                <h4 class="prezzo-scontato" style="min-height:4vh;margin-top:5px;color:#a9a9a9;max-height:15px">
                <h2 style="'.$scontato.'padding:0;margin-top:-10px;margin-bottom:-5px;line-height:10px;">da<span itemprop="priceCurrency" content="EUR"> &euro;  '.$prezzoOriginale.'</span>
                </h2>
                <h2 itemprop="price" style="max-height:25px;margin-top:5px;"><small>da</small><span itempropr="priceCurrency" content="EUR"> &euro; '.$prezzoScontato.'</span></h2>
                </h4>
                '.$scontoBox.'<small>Solo '.$qty_available.' pezzi rimasti.</small>
              </div>
            </div>
            </a>
          </div>
        </div>';
        //<h4>'.$p['ac_prodotto'].'</h4>
      }

  ?>
  <script>
  $(document).ready(function(){
    var cw = $('.imgprodotto').width();
    $('.imgprodotto').css({'height':cw+'px'});
  });
  </script>
