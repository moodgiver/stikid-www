<?php
//print_r ( $ordine )
?>
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <h2 class="title">Ordine <?=$ordine[0]['ac_nrordine']?></h2>
    <div class="table-responsive">

      <table class="table table-condensed table-cart desktop-cart">

        <thead>
          <tr class="cart_heading">
            <td class="image mobile-hide">Prodotto</td>
            <td class="description"></td>
            <td class="price">Dim LxA cm</td>
            <td class="quantity">Q.t&agrave;</td>
            <td class="total">Totale &euro;</td>
            <td></td>
          </tr>
        </thead>

        <tbody class="cart_body">

          <?php

            //$cart = $_SESSION['cart_items'];
            $static = $this->config->item('static_url');
            $totale = 0;

            foreach ( $ordine AS $item ){

              $prezzo = ((float)$item['ac_qty']*(float)$item['ordine_prezzo']);
              $totale = $totale + $prezzo;
              $telaio = '';
              $folder = 'ambienti/small/';
              if ( $item['ac_telaio'] != '' ){
                $telaio = "Telaio ".$item['ac_telaio'].' cm';
                $folder = 'canvas/preview/';
              }
              echo '
              <tr>
                <td style="width:10%"><img src="'.$static.'images/'.$folder.''.$item['ac_immagine'].'" style="width:100%"/></td>
                <td>
                  <h4>'.$item['ac_prodotto'].'</h4>
                  <p>'.$item['ac_colore'].'</p>
                </td>
                <td>
                  <strong>'.$item['ac_width'].' x '.$item['ac_height'].' cm</strong>
                  <p>'.$telaio.'</p>
                </td>
                <td>
                <span>'.$item['ac_qty'].'</span>
                </div>

                </td>
                <td class="text-right">
                  <h4 class="prezzo_val">'.number_format($prezzo,2).'</h4>
                </td>
              </tr>
              ';
              echo '<br>';
            }
          ?>
        </tbody>

          <tr class="cart_total">
            <td class="image"></td>
            <td class="description"></td>
            <td class="price"></td>
            <td class="quantity text-right">Totale</td>
            <td class="total text-right"><h3 class="totale_carrello"><?=number_format($ordine[0]['ac_totale'],2)?></h3></td>
            <td></td>
          </tr>

          <!--- coupon --->
          <tr class="cart_total">
            <td class="image"></td>
            <td class="cart_description text-right">
            </td>
            <td class="cart_price">

            </td>
            <td class="quantity text-right">
              Coupon
            </td>
            <td class="cart_price text-right">
               <p class="cart_coupon_discount"><?=number_format($ordine[0]['ordine_sconto'],2)?></p>
            </td>
            <td class="cart_delete">

            </td>
          </tr>
          <!--- spedizione --->
          <tr class="cart_total">

            <td colspan="2">
            <p style="color:##000">
                </p>
            </td>

            <td class="cart_price">
            </td>
            <td class="quantity text-right">
              Spedizione
            </td>
            <td class="cart_price text-right">
               <p class="cart_shipment"><?=number_format($ordine[0]['ac_spesespedizione'],2)?></p>
            </td>
            <td class="cart_delete">

            </td>
          </tr>

          <!--- totale ordine --->
          <tr class="cart_total">
            <td class="image"></td>
            <td class="description"></td>
            <td class="price"></td>
            <td class="quantity text-right">Totale</td>
            <td class="total text-right"><h2 class="totale_ordine"><?=number_format((float)$ordine[0]['ac_totale']+(float)$ordine[0]['ac_spesespedizione']-(float)$ordine[0]['ordine_sconto'],2)?></h2></td>
            <td></td>
          </tr>

        </tbody>
      </table>

      <?php
        //include_once ( 'cart_detail_mobile.php' );
      ?>

    </div>

    <div class="col-sm-12 col-xs-12 cart-actions text-center clearfix">
      <a href="<?=base_url()?>profilo/ordini"><button class="btn get">Torna alla lista ordini</button></a>
    </div>
  </div>
