<header class="header">
<nav id='cssmenu'>
<div id="head-mobile"></div>
<div class="button solo-mobile">
</div>
<ul style="background:#8bc53f;">
<li class=''><a href='<?=base_url()?>'>Home</a></li>
<?php
  if ( $this->config->item('isqpb') ){
    echo '<li><a href="#">Quadri per Bambini</a>';
    echo '<ul>';
        foreach ( $collezioni_qpb AS $c ){
          $cat = $c['ac_categoria'];
          $uri = uri_cat($cat,2);
          if ( $c['ac_categoria_lang'] != '' ){
            $cat = $c['ac_categoria_lang'];
            $uri = uri_cat($cat,2);
          }
          $uri = $c['ac_link'];
          echo '<li><a href="'.base_url().''.$uri.'">'.$cat.'</a></li>';
        }
    echo '</ul></li>';
  }
?>
<li><a href='#'>Adesivi Murali</a>
  <ul>
    <?php
      foreach ( $collezioni AS $c ){
        $cat = $c['ac_categoria'];
        $uri = uri_cat($cat,0);
        if ( $c['ac_categoria_lang'] != '' ){
          $cat = $c['ac_categoria_lang'];
          $uri = uri_cat($cat,1);
        }
        $uri = $c['ac_link'];
        echo '<li><a href="'.base_url().''.$uri.'">'.$cat.'</a></li>';
      }
    ?>
  </ul>
</li>

    <?php
    if ( !$this->config->item('isqpb') ){
      echo '
      <li><a href="#">Quadri per Bambini</a>
      <ul>';
      foreach ( $collezioni_qpb AS $c ){
        $cat = $c['ac_categoria'];
        $uri = uri_cat($cat,2);
        if ( $c['ac_categoria_lang'] != '' ){
          $cat = $c['ac_categoria_lang'];
          $uri = uri_cat($cat,2);
        }
        $uri = $c['ac_link'];
        echo '<li><a href="'.base_url().''.$uri.'">'.$cat.'</a></li>';
      }
      echo '
      </ul>
      </li>
      ';
      echo '
      <li><a href="#">Il Piccolo Principe</a>
        <ul>
          <li><a href="'.base_url().'il-piccolo-principe-adesivi-murali">Adesivi Murali</a></li>
          <li><a href="'.base_url().'il-piccolo-principe-quadri-per-bambini">Quadri per Bambini</a></li>
        </ul>
      </li>
      <li><a href="#">Giulio Coniglio</a>
        <ul>
          <li><a href="'.base_url().'giulio-coniglio-adesivi-murali">Adesivi Murali</a></li>
          <li><a href="'.base_url().'giulio-coniglio-quadri-per-bambini">Quadri per Bambini</a></li>
        </ul>
      </li>
      <li><a href="'.base_url().'mega-stickers-adesivi-murali">Mega Stickers</a></li>';
    }
    ?>
    <li><a href="<?=base_url();?>pagina/contatti"><span class="fa fa-envelope"></span> Contattaci</a></li>
    <li class="solo-mobile"><a href="cart/conferma"><span class="fa fa-shopping-cart"></span> Carrello</a></li>

<?php
if ( $this->session->user['islogged'] ){
  echo '
  <li class="solo-mobile"><a href="'.base_url().'profilo/account"><span class="fa fa-user"></span> Il tuo account</a>
  </li>
  <li class="solo-mobile"><a href="'.base_url().'logout"><span class="fa fa-sign-out"></span>  Esci</a>';
} else {
  echo '
  <li class="solo-mobile"><a href="'.base_url().'login"><span class="fa fa-lock"></span> Accedi</a>
  </li>
  <li class="solo-mobile"><a href="'.base_url().'registrati"><span class="fa fa-user"></span>  Registrati</a>';
}
?>

</ul>
</nav>
</header>
