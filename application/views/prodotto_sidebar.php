
<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
  <div class="col-sm-12 misure-container box-info-prodotto">
  <?php
    $w = (int)$p['ac_W_min'];
    $h = (int)$p['ac_H_min'];
    $wMax = (int)$p['ac_W_max'];
    $ratio = (float)$p['ac_W_min']/(float)$p['ac_H_min'];
    $cs = $p['ac_coefficiente_sconto'];
    echo '<input type="hidden" class="ac_w_min" value="'.$p['ac_W_min'].'">
    <input type="hidden" class="ac_h_min" value="'.$p['ac_H_min'].'">
    <input type="hidden" class="ratio"	  value="'.$ratio.'">
    <input type="hidden" class="coefficiente" value="'.$p['ac_coefficiente_sconto'].'">';
    if ( $p['bl_colore'] ){
      require_once ( 'prodotto_colora.php' );
    }

    if ( $w != $wMax ){
      echo '<h5>Scegli tra</h5>';
      include_once('prodotto_misure_smlxl.php');
      echo '
      <h5 style="color:#777"><small>Sposta il cursore verso destra o sinistra per modificare le dimensioni del foglio adesivo</small></h5>
      <div class="clearfix"></div>
      <h5>Misura <span class="misure">L '.$p['ac_W_min'].' x A '.$p['ac_H_min'].' cm</span></h5>
      <div class="clearfix"></div>
      <input class="selector-misure pointer" data-action="sticker-misure" type="range" value="'.$w.'" min="'.$w.'" max="'.$wMax.'" step="1" data-category="#id_categoria#" style="position:absolute;left:0;width:100%;font-size:2em;height:1em;margin-bottom:20px;"/>';
    } else {
      echo '<h5>Misura <br><span class="misure">'.$p['ac_W_min'].' x '.$p['ac_H_min'].' cm</span>';
      echo '</h5>';
    }

    echo '
    <div class="clearfix" style="margin-top:50px;></div>';

    $prezzoOriginale = $this->ecommerce->prezzo_prodotto($w,$h);
    if ( $cs < 1 ){
      echo '
      <div class="clearfix"></div>
      <h2 class="prezzo prezzo-barrato">
      <span class="prezzo-originale" style="font-size:1em;color:red;text-decoration:line-through;">&euro; '.number_format($prezzoOriginale,2).'</span>
      </h2>';
    }
    $prezzo = number_format((int)($prezzoOriginale*$cs),2);
    echo '
    <div class="clearfix"></div>
    <h2 class="prezzo" style="font-size:1.5em;">
    <span class="prezzo_val prezzo_val_scontato">&euro; '.$prezzo.'</span>
    </h2>';


    $spedizione = $this->config->item('spese_spedizione');

    $color = 'Black';

    if ( $p['bl_colore'] == 0 ){
      $color = 'Multicolor';
    }
    echo '
      <p>Pi&uacute; &euro; '.number_format($spedizione,2).' spese di spedizione</p>
    ';
    echo '<button type="button" class="btn cart btn-aggiungi-al-carrello btn-action" data-action="addToCart" data-id="'.$p['id_prodotto'].'" data-sconto="'.$cs.'" data-uuid="" style="width:100%"><i class="fa fa-shopping-cart"></i> AGGIUNGI AL CARRELLO</button>';


  ?>

  <div class="col-sm-12 text-center dati-prodotto-carrello">
    <?php

    echo '<input type="hidden" class="id_prodotto" value="'.$p['id_prodotto'].'">
    <input type="hidden" class="ac_prodotto"	value="'.$p['ac_prodotto'].'">
    <input type="hidden" class="thumb"		value="'.$p['ac_immagine'].'">
    <input type="hidden" class="ac_width"		value="'.$w.'">
    <input type="hidden" class="ac_height"	value="'.$h.'">
    <input type="hidden" class="ac_prezzo"	value="'.$prezzo.'">
    <input type="hidden" class="ac_colore"			value="'.$color.'">
    <input type="hidden" class="telaio" value="">
    <input type="hidden" class="currentTesto" value="">
    <input type="hidden" class="currentWidth" value="">
    <input type="hidden" class="currentHeight" value="">
    <input type="hidden" class="currentFont" value="">
    <input type="hidden" class="currentColorName" value="">
    <input type="hidden" class="currentTotale" value="">
    <input type="hidden" class="ac_font" value="">
    <input type="hidden" class="ac_testo" value="">
    <input type="hidden" class="ac_collezione" value="'.$p['ac_categoria'].'">';
    ?>

  </div>

  <div class="col-lg-12 col-xs-12 box-info-prodotto">
      <h4>Soddisfatti o rimborsati</h4>
      <p><small>Se il tuo Stickers non aderisce, puoi scegliere se ottenere il rimborso o la sostituzione del tuo ordine.</small></p>
  </div>
  <div class="col-lg-12 col-xs-12 box-info-prodotto">
      <h4>Spedito in 11 giorni</h4>
      <p><small>Per acquisti con bonifico bancario i tempi di lavorazione decorrono dalla data di ricezione del pagamento.</small></p>
  </div>

</div>
</div>

<script>
$(document).ready(function(){

  $('.selector-misure').on('change',function(){
    $('.btn-aggiungi-al-carrello').addClass('hide')
    var ratio = $('.ratio').val();
		var w = $(this).val();
		var h = eval(w) / eval(ratio);
    $('.misure').html ( 'L ' + w + ' x A ' + parseInt(h+1) + ' cm')
    $.post ( routing ,
      {
        mode: 'calcolo_prezzo',
        w: w,
        h: h
      }, function(result){
        var p = parseInt(result).toFixed(2);
        $('.ac_width').val(w);
        $('.ac_height').val(Math.round(h));
        $('.ac_prezzo').val("&euro; " + p);
        if ( $('.coefficiente').val() != 0 ){
          var prezzo_scontato = parseInt(p * $('.coefficiente').val()).toFixed(2);
          $('.ac_prezzo').val( prezzo_scontato );
          $('.prezzo-originale').html("&euro; " + p );
          $('.prezzo_val_scontato').html ( "&euro;  " + prezzo_scontato );
        }
        $('.btn-aggiungi-al-carrello').removeClass('hide')
      }
    )

  })
})
</script>
