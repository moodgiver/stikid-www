<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {


  function __construct(){
    parent::__construct();
    date_default_timezone_set("Europe/Rome");
    $this->load->library('uuid');
    $this->load->library('form_validation');
    $this->load->helper(array('form', 'url'));
  }

  function index(){
    $this->form_validation->set_rules('email_login', 'Email', 'email|required');
    $this->form_validation->set_rules('password_login', 'Password', 'required|callback_login_user');
    $this->form_validation->set_message('password_login','{field} non corretti. Riprovare');
    if ($this->form_validation->run() == FALSE)

      {
        if ( count($this->session->cart_items) > 0 ){
          if ( $this->session->user['islogged'] ){
            if ( $this->session->order['id'] == '' ){
              $data['ordine_nr'] = ordine_nr();
              $this->ecommerce->set_session_order_nr($data["ordine_nr"]);
            }
            $data = common_data();
            $data['mode'] = 'cart';
            $data['view'] = 'spedizione';
            $this->load->view('main',$data);
          } else {

            $data = common_data();
            $data['mode'] = 'cart';
            $data['view'] = 'spedizione';
            $this->load->view('main',$data);
          }
        }
      }
    else
      {
        //redirect(base_url().'cart/spedizione');
      }
  }

  public function signin(){
    $this->form_validation->set_rules('email_login', 'Email', 'email|required');
    $this->form_validation->set_rules('password_login', 'Password', 'required|callback_login_user');
    $this->form_validation->set_message('login_user','Email o password non corretti. Riprovare');
    $result = $this->form_validation->run('login');
    if ( !isset($_POST['normal']) ){
      $this->reload_page();
    } else {
      redirect(base_url().'profilo/account');
    }
  }

  public function check_email($str){

    $is_email = check_register($str);
    if ( $is_email ){
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  public function login_user($str){
    $is_user = check_login($str);
    if ( $is_user != 0 ){
      $utente = $is_user[0];
      $new_data = array (
          'customer_id' => $utente['idregistrazione'],
          'customer'    => $utente['ac_nome'].' '.$utente['ac_cognome'],
          'islogged'    => true,
          'email'       => $utente['ac_email'],
          'session_id'  => session_id(),
          'ip'          => $_SERVER['REMOTE_ADDR'],
          'user_agent'  => $this->input->user_agent()
        );
        $this->session->set_userdata('user',$new_data);

        $order = $this->session->userdata('order');
        $order['O_ID']				 = $utente['idregistrazione'];
        $order['O_NOME']       = $utente['ac_nome'];
        $order['O_COGNOME']    = $utente['ac_cognome'];
        $order['O_AZIENDA']    = $utente['ac_azienda'];
        $order['O_INDIRIZZO']  = $utente['ac_indirizzo'];
        $order['O_CITTA']      = $utente['ac_citta'];
        $order['O_CAP']        = $utente['ac_cap'];
        $order['O_PV']         = $utente['ac_pv'];
        $order['O_NAZIONE']    = $utente['ac_nazione'];
        $order['O_TELEFONO']   = $utente['ac_telefono'];
        $order['O_MOBILE']     = $utente['ac_cellulare'];
        $order['O_EMAIL']      = $utente['ac_email'];
        $order['O_CF']         = $utente['ac_cf'];
        $order['O_PIVA']       = $utente['ac_piva'];

        $order['OS_NOME']      = '';
        $order['OS_COGNOME']   = '';
        $order['OS_AZIENDA']   = '';
        $order['OS_INDIRIZZO'] = '';
        $order['OS_CITTA']     = '';
        $order['OS_CAP']       = '';
        $order['OS_PV']        = '';
        $order['OS_NAZIONE']   = '';
        $order['OS_TELEFONO']  = '';
        $order['OS_MOBILE']    = '';

        $this->session->set_userdata('order',$order);
        $this->reload_page();
      } else {
      	redirect(base_url().'errore/login');
        //echo '<script language=\"javascript\">alert("Impossibile accedere. Controllare email o password")</script>';
        //$data = common_data();
        //$data['mode'] = 'error';
        //$data['view'] = 'login';
        //$this->load->view('main',$data);
        //$this->load->view('main',$data);
        //return 'error';
      }

  }

  public function reload_page(){
    $data = common_data();
    $data['mode'] = 'home';
    $data['view'] = '';
    $this->load->view('main',$data);
  }

}
