<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

  function __construct(){
 		parent::__construct();
		date_default_timezone_set("Europe/Rome");
    $this->load->model('Site_model');
		$this->load->library('uuid');
  }

  public function index()
	{

		$data['mode'] = 'cart';
    $data['view'] = 'detail';
    $data['collezioni'] = $this->Site_model->collezioni();
    $data['collezioni_home'] = $this->Site_model->collezioni_home();
    $data['venduti'] = $this->Site_model->piu_venduti();
    $data['offerte'] = $this->Site_model->offerte();
    $data['footer_1'] = $this->Site_model->pagine_footer(1);
    $data['footer_2'] = $this->Site_model->pagine_footer(2);
    $data['footer_3'] = $this->Site_model->pagine_footer(3);
    $data['footer_4'] = $this->Site_model->pagine_footer(4);
    //$this->load->model('Site_model');
		$this->load->view('main',$data);
	}
}
