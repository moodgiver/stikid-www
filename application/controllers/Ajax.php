<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Ajax extends CI_Controller {

  function __construct(){
 		parent::__construct();
		date_default_timezone_set("Europe/Rome");
    $this->load->model('Site_model');
    $this->load->model('Order_model');
    $this->load->model('User_model');
    $this->load->library('ecommerce');
    $this->load->library('uuid');
  }

  function index(){
    $form = $_POST;
    $mode = $_POST['mode'];

    switch ($mode){
      case 'calcolo_prezzo':
        //echo $this->ecommerce->price ( $form['w'] , $form['h'] , $form['cs'] );
        if ( isset($_POST['w']) ){
          $w = $_POST['w'];
          $h = $_POST['h'];
        }
        echo $this->ecommerce->prezzo_prodotto($w,$h);
      break;

      case 'session_addtocart':
        $this->ecommerce->add_item_to_cart($form);
      break;

      case 'cart-item-qty':
        $this->ecommerce->cart_item_qty($form);
      break;

      case 'coupon':
        $this->ecommerce->calcola_coupon($form);
      break;

      case 'spedizioni':
        echo $this->ecommerce->calcola_spedizioni($form['totale']);
      break;

      case 'register':
        //check is user exists
        $isUser = false;
        $isUser = check_register($_POST);
        echo $isUser;
      break;
    }
  }

  public function clear_cart_items(){
		$items = [];
		$this->session->set_userdata('cart_items', $items );
	}

  public function clear_session(){
    $this->session->sess_destroy();
    redirect(base_url(), 'refresh');
  }

  public function codifica_passwords(){
    $result = $this->User_model->codifica_passwords();
    echo $result;
  }

  public function email_test(){
    $this->load->library('email'); // load email library



    $this->email->from('servizioclienti@sticasa.com', 'STICASA / STIKID');
  	//$CI->email->bcc('sticasa@sticasa.com');
  	$this->email->to('swina.allen@gmail.com');
  	$this->email->subject("TEST DA STIKID SHOP");
    $this->email->message("HELLO");
  	$noerr = $this->email->send();

    if ( $noerr ){
      $params = array(
        'tipo'    => 'TEST EMAIL',
        'email'   => 'swina.allen@gmail.com',
        'ordine'  => 'TEST',
        'subject' => "TEST INVIO EMAIL",
        'message' => "TEST",
        'status'  => $noerr
      );
      $this->db->insert('tbl_emails_logs',$params);
      echo 'Test di invio mail effettuato con successo';
    } else {
      echo 'Test di invio mail fallito. Verificare la configurazione';
    }
}


function decodifica_password(){
	$this->User_model->decodifica_password();
  }

}
