<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Password extends CI_Controller {


  function __construct(){
 		parent::__construct();
		date_default_timezone_set("Europe/Rome");
    $this->load->model('Site_model');
		$this->load->model('User_model');
		$this->load->library('uuid');
    $this->load->library('mailer');
  }

  function reset(){
    $data = common_data();
		$data['mode'] = 'password';
	  $data['view'] = 'reset';
		$this->load->view('main',$data);
  }

  function request(){
    $data = common_data();
    $data['mode'] = 'password';
    $data['view'] = 'reset';
    if ( check_register($_POST['email']) ){
      $temp_password = $this->random_password();
      $reset_uuid = $this->uuid->v4();
      $this->User_model->reset_password($_POST['email'],$temp_password,$reset_uuid);
      if ( $this->mailer->send_reset_password($_POST['email'],$reset_uuid) ){
        $data['email'] = 'Abbiamo spedito le informazioni per reimpostare la tua password a  '.$_POST['email'];
      }
    } else {
      $data['error'] = true;
      $data['email'] = 'Non esiste un utente con questa email. Inserire una email registrata';
    }
    $this->load->view('main',$data);
  }

  function update($uuid){
    $user = $this->User_model->get_user_reimposta_password($uuid);
    if ( count($user) > 0 ){
      $data = common_data();
      $data['mode'] = 'password';
      $data['view'] = 'modifica';
      $data['user'] = $user;
      $this->load->view('main',$data);
    }
  }

  function modifica(){
    if ( $this->form_validation->run('passwordchange') == FALSE){
      $user = $this->User_model->get_user_reimposta_password($_POST['uuid']);
      $data = common_data();
      $data['mode'] = 'password';
      $data['view'] = 'modifica';
      $data['user'] = $user;
      $this->load->view('main',$data);
    } else {
      $this->User_model->change_password();
      $data = common_data();
      $data['mode'] = 'password';
      $data['view'] = 'changed';
      $this->load->view('main',$data);
    }
  }

  function random_password() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $password = array();
    $alpha_length = strlen($alphabet) - 1;
    for ($i = 0; $i < 8; $i++)
    {
        $n = rand(0, $alpha_length);
        $password[] = $alphabet[$n];
    }
    return implode($password);
  }

}
