<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

  function __construct(){
 		parent::__construct();
		header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Credentials: true");
		date_default_timezone_set("Europe/Rome");
    $this->load->model('Site_model');
		$this->load->model('Order_model');
		$this->load->model('User_model');

		$this->load->library('uuid');
		$this->load->library('form_validation');
		$this->ecommerce->init_session();
  }

	public function index()
	{
		//load common data queries
		$data = common_data ();
		//mode homepage
		$data['mode'] = 'home';
		//load slides
		$data['slides'] = $this->Site_model->slider();
		//load collezioni homepage
		$data['collezioni_home'] = $this->Site_model->collezioni_home();
		//load prodotti piu venduti
		$data['venduti'] = $this->Site_model->piu_venduti();
		//load offerte
		$data['offerte'] = $this->Site_model->offerte();
		$this->load->view('main',$data);

	}

	public function collection($collezione){
		$data = common_data ();
		$data['mode'] = 'collezione';
		if ( strtolower($collezione) != 'sticky-text' ){
			//query collezione (vecchio nome per mantenere backlink)
			$data['prodotti_collezione'] = $this->Site_model->collezione($collezione);
		} else {
			$data['mode'] = 'sticky-text';
			$data['stickytext_description'] = $this->Site_model->stickytext_description();
			$data['fonts'] = $this->Site_model->fonts();
			$data['colori'] = $this->Site_model->colori();
		}
		$this->load->view('main',$data);

	}

	public function collection_new($collezione){
		$data = common_data ();
		$data['mode'] = 'collezione';
		//query collezione (nuovi nomi)
		$data['prodotti_collezione'] = $this->Site_model->collezione_new($collezione);
		$this->load->view('main',$data);
	}

	public function quadriperbambini($collezione){
		$data = common_data ();
		$data['mode'] = 'collezioneqpb';
		$data['prodotti_collezione'] = $this->Site_model->collezione_qpb($collezione);
		$this->load->view('main',$data);
	}

	public function prodotto($id){
		$data = common_data();
		$data['mode'] = 'prodotto';
		$data['prodotto'] = $this->Site_model->prodotto($id);
		$data['upsell'] = $this->Site_model->prodotti_upsell($id);
		$data['descrizione'] = $this->Site_model->prodotto_descrizione($id);
		$data['gallery'] = $this->Site_model->prodotto_gallery($id);
		$data['colori'] = $this->Site_model->colori();
		$data['tabs'] = $this->Site_model->prodotto_tabs();
		$this->load->view('main',$data);
	}

	public function prodottoqpb($id){
		$data = common_data();
		$data['mode'] = 'prodottoqpb';
		$data['prodotto'] = $this->Site_model->prodotto($id);
		$data['descrizione'] = $this->Site_model->prodotto_descrizione($id);
		$data['gallery'] = $this->Site_model->prodotto_gallery($id);
		$data['tabs'] = $this->Site_model->prodotto_tabs_qpb();
		$this->load->view('main',$data);
	}

	public function offerte(){
		$data = common_data();
		$data['mode'] = 'offerte';
		$data['offerte'] = $this->Site_model->offerte();
		$this->load->view('main',$data);
	}

  public function promozione(){
		$data = common_data();
		$data['mode'] = 'promo';
		$data['promo'] = $this->Site_model->promo();
		$this->load->view('main',$data);
	}

	public function simulatore($id){
		$data = common_data();
		$data['mode'] = 'simulatore';
		$data['prodotto'] = $this->Site_model->prodotto($id);
		$data['sim_stickers'] = $this->Site_model->prodotto_simulatore($id);
		$this->load->view('main',$data);
	}

	public function upload_foto(){
		$tempFile = $_FILES['file']['tmp_name'];
		$myFile = explode ('.' , $_FILES['file']['name']);
		$fileName = $this->session->__ci_last_regenerate.'.'.$myFile[count($myFile)-1];
		$targetPath = getcwd() . "/public/users/upload/temp/";
		$targetFile = $targetPath . $fileName ;
		$this->session->set_userdata('simulatore' , base_url().'public/users/upload/temp/'.$fileName );
		move_uploaded_file($tempFile, $targetFile);
	}

	public function cart(){
		$data = common_data();
		$data['mode'] = 'cart';
	  $data['view'] = 'detail';
		$this->load->view('main',$data);
	}

	public function cart_spedizione(){
		$data = common_data();
		$data['mode'] = 'cart';
	  $data['view'] = 'spedizione';
		$data['province'] = province();
		if ( count($this->session->cart_items) > 0 ){
			if ( $this->session->user['islogged'] ){
				if ( $this->session->order['id'] == '' ){
					//genera nuovo numero d'ordine progressivo
					$data['ordine_nr'] = ordine_nr();
					$this->set_session_ordernr($data["ordine_nr"]);
				}
				$this->load->view('main',$data);
			} else {
				$this->load->view('main',$data);
			}
		} else {
			redirect(base_url());
		}
	}

	public function cart_checkout(){
		$data = common_data();
		$data['mode'] = 'cart';
	  $data['view'] = 'checkout';
		if ( $this->session->user['islogged'] &&  count($this->session->cart_items) > 0 ){

			if ($this->form_validation->run('profile') == FALSE){
				$data['view'] = 'spedizione';
				$data['province'] = province();
				$this->load->view('main',$data);
			} else {
				$this->User_model->save_customer();
				$this->Order_model->save_preorder();
				$this->load->view('main',$data);
			}

		} else {
			redirect(base_url());
		}
	}


	public function profile_rules(){

	}

	//pagina di conferma dell'ordine
	function ordine_conferma(){
		$data = common_data();
		$data['mode'] = 'order';
		$data['view'] = 'paypal';
		$this->load->view('main',$data);
		$this->reset_order_session();
		$items = [];
		$this->session->set_userdata('cart_items', $items );
	}

	public function bonifico_conferma(){
		$data = common_data();
		$data['mode'] = 'order';
		$data['view'] = 'bonifico';
		if ( $this->session->order['id'] != '' ){
			if ( $this->ecommerce->registra_ordine('BONIFICO') ){

				redirect(base_url().'ordine/grazie');
				//$this->load->view('main',$data);

			} else {
				$data['mode'] = 'cart';
				$data['view'] = 'detail';
				$this->load->view('main',$data);
			}
		} else {
			redirect(base_url());
		}
 	}

	public function paypal_conferma(){
		$data = common_data();
		$data['mode'] = 'order';
		$data['view'] = 'paypal';
		if ( $this->session->order['id'] != '' ){
			if ( $this->ecommerce->registra_ordine('PAYPAL') ){
				redirect(base_url().'ordine/grazie');
				//$this->load->view('main',$data);
			} else {
				$data['mode'] = 'cart';
				$data['view'] = 'checkout';
				$this->load->view('main',$data);
			}
		} else {
			redirect(base_url());
		}
 	}



	function paypal($str){
		$data = common_data();
		$data['mode'] = 'cart';
		$data['view'] = 'paypal_cancel';
		if ( $this->session->order['id'] != '' ){
			$this->load->view('main',$data);
		} else {
			redirect(base_url());
		}
	}

	function paypal_notify(){
		return true;
	}

	function registrazione(){
		$data = common_data();
	  $data['mode'] = 'registrazione';
	  $data['view'] = 'modulo';
		$data['is_user'] = check_register($_POST['email']);
		$_POST['o_email'] = $_POST['email'];
		$_POST['o_password'] = $_POST['password'];
		$_POST['o_password_conf'] = $_POST['password'];
		$data['profilo_salvato'] = false;
		$data['province'] = province();
		$this->load->view('main',$data);
	}

	function registrami(){
		$data = common_data();
	  $data['mode'] = 'registrazione';
	  $data['view'] = 'modulo';
		$data['profilo_salvato'] = false;
		$data['province'] = province();
		if ($this->form_validation->run('profile') == TRUE){
			$data['profilo_salvato'] = true;

			$registrazione = $this->User_model->registra_utente();
			if ( $registrazione ){
				send_email_registrazione();
				if ( $_POST['redirect'] != '' ){
					redirect(base_url().''.$_POST['redirect'] );
				} else {
					redirect(base_url().'profilo/account', 'refresh');
				}
			}
		} else {
			$this->load->view('main',$data);
			//echo validation_errors();
		}

	}

	function profilo($str){
		$data = common_data();
		$data['mode'] = 'profilo';
		$data['view'] = $str;
		$data['province'] = province();
		$data['profilo_salvato'] = false;
		if ( $str == 'ordini' ){
			$data['ordini'] = $this->User_model->ordini_utente();
		}
		if ( $str == 'salva' ){
			if ($this->form_validation->run('profile') == TRUE){
				$data['profilo_salvato'] = true;
				$this->User_model->save_customer();
			}
			$this->load->view('main',$data);
		}
		if ( $str == 'password' ){
			$uuid = $this->uuid->v4();
			$this->User_model->set_password_reset_uuid($uuid);
			redirect(base_url().'utente/account/reset/'.$uuid);
		}
		if ( $this->session->user['islogged'] ){
			$this->load->view('main',$data);
		} else {
			redirect(base_url());
		}
	}

	function ordine($nr,$anno){
		if ( $this->session->user['islogged'] ){
			$ordine_nr = $nr.'/'.$anno;
			$data = common_data();
			$data['mode'] = 'profilo';
			$data['view'] = 'ordine';
			$data['ordine'] = $this->User_model->ordine($ordine_nr);
			$this->load->view('main',$data);
		} else {
			redirect(base_url());
		}
	}

	/*
	function pagina($titolo,$id){
		$data = common_data();
		$data['mode'] = 'pagina';
		$data['view'] = 'normale';
		$data['pagina'] = $this->Site_model->pagina($id);
		if ( $data['pagina'][0]['ac_link'] == 'designers' ){
			$data['view'] = 'designers';
			$data['designers'] = $this->Site_model->designers();
		}
		$this->load->view('main',$data);
	}
	*/

	function pagina($url){
		$data = common_data();
		$data['mode'] = 'pagina';
		$data['view'] = 'normale';
		$data['pagina'] = $this->Site_model->pagina($url);
		if ( $data['pagina'][0]['hook'] == 'designers' ){
			$data['view'] = 'designers';
			$data['designers'] = $this->Site_model->designers();
		}
		$this->load->view('main',$data);
	}


	function designer($nome,$id){
		$data = common_data();
		$data['mode'] = 'designer';
		$data['view'] = 'scheda';
		$data['designer'] = $this->Site_model->designer($id);
		$this->load->view('main',$data);
	}

	function ricerca(){
		$data = common_data();
		$data['mode'] = 'search';
		$data['view'] = 'ricerca';
		$data['prodotti_collezione'] = $this->Site_model->search();
		$this->load->view('main',$data);
	}

	function send_mail(){
		$this->load->library('email'); // load email library
		$this->email->from('no-reply@sticasa.com', 'STICASA / STIKID');
		//$this->email->bcc('tantipremi@indual.it');
		$this->email->to('swina.allen@gmail.com');
		$this->email->subject("ORDINE STICASA/STIKID");
		$message = "TEST";
		$this->email->message($message);
		$noerr = $this->email->send();
		if ( $noerr ){
			echo 'mail test OK';
		} else {
			echo $noerr;
		}
	}


	private function check_session(){
		//imposta sessioni
		if ( !$this->session->userdata('user') ){
			$session_data = array (
				'islogged' => false,
				'email' => '',
				'customer' => '',
				'customer_id' => 0
			);
			$this->session->set_userdata('user', $session_data);
		}
		if ( !$this->session->userdata('order') ){
			$this->create_order_session();
		}
		if ( !$this->session->userdata('cart_items') ){
			$items = [];
			$this->session->set_userdata('cart_items', $items );
		}
	}

	private function create_order_session(){

		$session_order = array(
			'OS_AZIENDA'		=> '',
			'OS_CAP'				=> '',
			'OS_CITTA' 			=> '',
			'OS_COGNOME'		=> '',
			'OS_EMAIL' 			=> '',
			'OS_INDIRIZZO' 	=> '',
			'OS_MOBILE' 		=> '',
			'OS_NAZIONE'		=> '',
			'OS_NOME' 			=> '',
			'OS_PV' 				=> '',
			'OS_TELEFONO' 	=> '',
			'O_AZIENDA' 		=> '',
			'O_CAP' 				=>'',
			'O_CITTA' 			=> '',
			'O_COGNOME' 		=> '',
			'O_EMAIL' 			=> '',
			'O_INDIRIZZO' 	=> '',
			'O_MOBILE' 			=> '',
			'O_NAZIONE' 		=> '',
			'O_NOME' 				=> '',
			'O_PV' 					=> '',
			'O_TELEFONO' 		=> '',
			'SHIPPING' 			=> 0,
			'id' 						=> '',
			'SHIPTO' 				=> false,
			'CART_TOTAL' 		=> 0,
			'COUPON' 				=> '&nbsp;',
			'COUPON_VALUE' 	=> 0,
			'MESSAGE' 			=> '',
			'ORDER_TOTAL' 	=> 0,
			'FATTURA' 			=> false,
			'CF' 						=> '',
			'PIVA' 					=> '',
			'REGALO'				=> false,
			'BIGLIETTO_TESTO'	=> '',
			'MESSAGE' 			=> '',
			'ISCART'				=> 1,
			'items' 				=> []
		);
		$items = [];
		$this->session->set_userdata('order', $session_order);
	}

	private function reset_order_session(){
		$ordine = $this->session->order;
		$ordine['id'] = '';
		$ordine['ORDER_TOTAL'] 	= 0;
		$ordine['SHIPPING'] 		= 0;
		$ordine['SHIPTO'] 			= false;
		$ordine['CART_TOTAL'] 	= 0;
		$ordine['COUPON']				= '';
		$ordine['COUPON_VALUE'] = 0;
		$ordine['MESSAGE'] 			=	0;
		$ordine['FATTURA'] 			= false;
		$this->session->set_userdata('order', $ordine);
	}

	private function set_session_ordernr($nr){
		$ordine_data = $this->session->order;
		$ordine_data['id'] = $nr;
		$this->session->set_userdata('order',$ordine_data);
		return true;
	}

	private function prodImage($lista){
		$aImg = explode(',',$lista);
		return $aImg[0];
	}

	function debug(){
		$data = common_data();
		$data['mode'] = 'debug';
		$data['view'] = 'debug';
		$data['debug_order'] = $this->session->order;
		$data['debug_cart'] = $this->session->cart_items;
		$data['debug_user']= $this->session->user;
		$this->load->view('main',$data);

	}

	function password_hash_all($s,$e){
		/*$this->load->library('encryption');
		$key = bin2hex($this->encryption->create_key(16));
		echo $key;*/
		$this->db->select("*");
		$sql = "SELECT * FROM tbl_registrazioni WHERE ac_email <> '' AND ac_password <> '' LIMIT ?,?";
		$filter = array ( (int)$s , (int)$e );
		$query = $this->db->query($sql,$filter);
		$users = $query->result_array();
		echo '<div style="max-width:500px;width:500px">';
		foreach ( $users AS $row ){
			$last_id = $row['idregistrazione'];
			echo '.';
			$data = array (
				'ac_password' => $this->encryption->encrypt($row['ac_pwd'])
			);
			$this->db->where('idregistrazione',$row['idregistrazione']);
			$this->db->update('tbl_registrazioni',$data);
		}
		echo '</br>=>'.$last_id.'</br>';
		$start = (int)$s+400;
		echo '</div><a href="'.base_url().'admin/hash/'.$start.'/400">Next 400</a>';
	}

      function errore($s){
        if ( $s == 'login' ){
	 $data = common_data();
         $data['mode'] = 'error';
         $data['view'] = 'login';
         $this->load->view('main',$data);
      }
     }

}
