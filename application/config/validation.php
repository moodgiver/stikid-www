<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array(
        'cart_registrazione' => array(
                array(
                        'field' => 'cart_email_registrazione',
                        'rules' => 'required|valid_email'
                ),
                array(
                        'field' => 'cart_password_registrazione',
                        'rules' => 'required'
                ),

        ),
        'cart_registrazione' => array(
                array(
                        'field' => 'cart_email_login',
                        'rules' => 'required|valid_email'
                ),
                array(
                        'field' => 'cart_password_login',
                        'rules' => 'required'
                ),

        ),
);
