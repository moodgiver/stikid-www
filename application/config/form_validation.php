<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array (
  'passwordchange' => array(
    array(
      'field' => 'password_change',
      'label' => 'Password',
      'errors'=> array(
        'required'    => 'Password obbligatoria',
        'min_length'  => 'La password deve essere di almeno 8 caratteri'
      ),
      'rules' => 'required|min_length[8]'
    ),
    array(
      'field' => 'password_change_confirm',
      'label' => 'Conferma Password',
      'errors'=> array(
        'required'    => 'Password obbligatoria',
        'matches'  => 'La password di conferma non coincide'
      ),
      'rules' => 'required|matches[password_change]'
    ),
  ),

  'login' => array (
      array(
        'field' => 'email_login',
        'label' => 'Email',
        'errors'=> array (
          'required'    => 'Indirizzo email obbligatorio',
          'valid_email' => 'Indirizzo email non valido'
        ),
        'rules' => 'required|valid_email'
      ),
      array(
        'field' => 'password_login',
        'label' => 'Password',
        'errors'=> array (
          'required'    => 'Password obbligatoria'
        ),
        'rules' => 'required'
      ),
  ),
  'profile' => array(
    array(
    'field' => 'o_nome',
    'label' => 'Nome',
    'errors'=> array (
        'required'  => 'Il nome è obbligatorio',
        'alpha'     => 'Il nome deve contenere solo caratteri'
    ),
    'rules' => 'required|alpha_numeric_spaces'
  ),
  array(
    'field' => 'o_cognome',
    'label' => 'Cognome',
    'errors'=> array (
        'required'  => 'Il cognome è obbligatorio',
        'alpha'     => 'Il cognome deve contenere solo caratteri'
    ),
    'rules' => 'required|alpha_numeric_spaces'
  ),
  array(
    'field' => 'o_azienda',
    'label' => 'Azienda',
    'errors'=> array (
      'alpha_numeric' => 'Il campo puo\' contenere caratteri alfanumerici'
    ),
    'rules' => 'alpha_numeric_spaces'
  ),
  array(
    'field' => 'o_indirizzo',
    'label' => 'Indirizzo',
    'errors'=> array (
      'required'  => 'Indirizzo obbligatorio',
    ),
    'rules' => 'required'
  ),
  array(
    'field' => 'o_cap',
    'label' => 'CAP',
    'errors'=> array (
      'required'  => 'CAP obbligatorio',
      'alpha_numeric' => 'Il CAP puo\' contenere caratteri alfanumerici'
    ),
    'rules' => 'required|alpha_numeric|max_length[8]',
  ),
  array(
    'field' => 'o_citta',
    'label' => 'CITTA',
    'errors'=> array (
      'required'  => 'Citta\' obbligatoria',
      'alpha' => 'Il campo puo\' contenere solo caratteri alfabetici'
    ),
    'rules' => 'required|alpha_numeric_spaces',
  ),
  array(
    'field' => 'o_pv',
    'label' => 'PV',
    'errors'=> array (
      'required'  => 'Provincia obbligatoria',
      'alpha' => 'Il campo puo\' contenere solo caratteri alfabetici'
    ),
    'rules' => 'required|alpha',
  ),
  array(
    'field' => 'o_nazione',
    'label' => 'NAZIONE',
    'errors'=> array (
      'required'  => 'Nazione obbligatoria',
      'alpha' => 'Il campo puo\' contenere solo caratteri alfabetici'
    ),
    'rules' => 'required|alpha_numeric_spaces',
  ),
  array(
    'field' => 'o_telefono',
    'label' => 'TELEFONO',
    'errors'=> array (
      'numeric'  => 'Il campo puo\' contenere solo numeri',
      'max_length' => 'Massima lunghezza 15 numeri'
    ),
    'rules' => 'numeric|max_length[15]',
  ),
  array(
    'field' => 'o_mobile',
    'label' => 'CELLULARE',
    'errors'=> array (
      'required'  => 'Numero cellulare obbligatorio',
      'numeric'  => 'Il campo puo\' contenere solo numeri',
      'max_length' => 'Massima lunghezza 15 numeri'
    ),
    'rules' => 'required|numeric|max_length[15]',
  ),
  array(
    'field' => 'o_cf',
    'label' => 'Codice Fiscale',
    'errors'=> array (
        'required'      => 'Il codice fiscale è obbligatorio',
        'alpha_numeric_spaces' => 'Sono permessi solo caratteri e numeri',
    ),
    'rules' => 'required|alpha_numeric_spaces'
  ),
  array(
    'field' => 'o_piva',
    'label' => 'P.IVA',
    'rules' => 'alpha_numeric_spaces|max_length[20]'
  )
  //controllo form spedizione ad altro Indirizzo
  ),
  'extra' => array(
    array(
    'field' => 'o_nome',
    'label' => 'Nome',
    'errors'=> array (
        'required'  => 'Il nome è obbligatorio',
        'alpha'     => 'Il nome deve contenere solo caratteri'
    ),
    'rules' => 'required|alpha_numeric_spaces'
  ),
  array(
    'field' => 'o_cognome',
    'label' => 'Cognome',
    'errors'=> array (
        'required'  => 'Il cognome è obbligatorio',
        'alpha'     => 'Il cognome deve contenere solo caratteri'
    ),
    'rules' => 'required|alpha_numeric_spaces'
  ),
  array(
    'field' => 'o_azienda',
    'label' => 'Azienda',
    'errors'=> array (
      'alpha_numeric' => 'Il campo puo\' contenere caratteri alfanumerici'
    ),
    'rules' => 'alpha_numeric'
  ),
  array(
    'field' => 'o_indirizzo',
    'label' => 'Indirizzo',
    'errors'=> array (
      'required'  => 'Indirizzo obbligatorio',
    ),
    'rules' => 'required'
  ),
  array(
    'field' => 'o_cap',
    'label' => 'CAP',
    'errors'=> array (
      'required'  => 'CAP obbligatorio',
      'alpha_numeric' => 'Il CAP puo\' contenere caratteri alfanumerici'
    ),
    'rules' => 'required|alpha_numeric|max_length[8]',
  ),
  array(
    'field' => 'o_citta',
    'label' => 'CITTA',
    'errors'=> array (
      'required'  => 'Citta\' obbligatoria',
      'alpha' => 'Il campo puo\' contenere solo caratteri alfabetici'
    ),
    'rules' => 'required|alpha_numeric_spaces',
  ),
  array(
    'field' => 'o_pv',
    'label' => 'PV',
    'errors'=> array (
      'required'  => 'Provincia obbligatoria',
      'alpha' => 'Il campo puo\' contenere solo caratteri alfabetici'
    ),
    'rules' => 'required|alpha_numeric_spaces',
  ),
  array(
    'field' => 'o_nazione',
    'label' => 'NAZIONE',
    'errors'=> array (
      'required'  => 'Nazione obbligatoria',
      'alpha' => 'Il campo puo\' contenere solo caratteri alfabetici'
    ),
    'rules' => 'required|alpha_numeric_spaces',
  ),
  array(
    'field' => 'o_telefono',
    'label' => 'TELEFONO',
    'errors'=> array (
      'numeric'  => 'Il campo puo\' contenere solo numeri',
      'max_length' => 'Massima lunghezza 15 numeri'
    ),
    'rules' => 'numeric|max_length[15]',
  ),
  array(
    'field' => 'o_mobile',
    'label' => 'CELLULARE',
    'errors'=> array (
      'required'  => 'Numero cellulare obbligatorio',
      'numeric'  => 'Il campo puo\' contenere solo numeri',
      'max_length' => 'Massima lunghezza 15 numeri'
    ),
    'rules' => 'required|numeric|max_length[15]',
  ),
  array(
    'field' => 'o_cf',
    'label' => 'Codice Fiscale',
    'errors'=> array (
        'required'      => 'Il codice fiscale è obbligatorio',
        'alpha_numeric_spaces' => 'Sono permessi solo caratteri e numeri',
    ),
    'rules' => 'required|alpha_numeric_spaces'
  ),
  array(
    'field' => 'o_piva',
    'label' => 'P.IVA',
    'rules' => 'alpha_numeric_spaces'
  ),
    array(
    'field' => 'os_nome',
    'label' => 'Nome',
    'errors'=> array (
        'required'  => 'Nome destinatario obbligatorio',
        'alpha_numeric_spaces'     => 'Il nome deve contenere solo caratteri'
    ),
    'rules' => 'required|alpha_numeric_spaces'
    ),
    array(
    'field' => 'os_cognome',
    'label' => 'Cognome',
    'errors'=> array (
        'required'  => 'Il cognome del destinatario obbligatorio',
        'alpha'     => 'Il cognome deve contenere solo caratteri'
    ),
    'rules' => 'required|alpha_numeric_spaces'
    ),
    array(
    'field' => 'os_azienda',
    'label' => 'Azienda',
    'errors'=> array (
      'alpha_numeric' => 'Il campo puo\' contenere caratteri alfanumerici'
    ),
    'rules' => 'alpha_numeric'
    ),
    array(
    'field' => 'os_indirizzo',
    'label' => 'Indirizzo',
    'errors'=> array (
      'required'  => 'Indirizzo destinatario obbligatorio'
    ),
    'rules' => 'required'
    ),
    array(
    'field' => 'os_cap',
    'label' => 'CAP',
    'errors'=> array (
      'required'  => 'CAP destinatario obbligatorio',
      'alpha_numeric' => 'Il CAP puo\' contenere caratteri alfanumerici'
    ),
    'rules' => 'required|alpha_numeric|max_length[8]'
    ),
    array(
    'field' => 'os_citta',
    'label' => 'CITTA',
    'errors'=> array (
      'required'  => 'Citta\' destinatario obbligatoria',
      'alpha' => 'Il campo puo\' contenere solo caratteri alfabetici'
    ),
    'rules' => 'required|alpha_numeric_spaces'
    ),
    array(
    'field' => 'os_pv',
    'label' => 'PV',
    'errors'=> array (
      'required'  => 'Provincia destinatario obbligatoria',
      'alpha' => 'Il campo puo\' contenere solo caratteri alfabetici'
    ),
    'rules' => 'required|alpha_numeric_spaces'
    ),
    array(
    'field' => 'os_nazione',
    'label' => 'NAZIONE',
    'errors'=> array (
      'required'  => 'La nazione del destinatario e\' obbligatoria',
      'alpha' => 'Il campo puo\' contenere solo caratteri alfabetici'
    ),
    'rules' => 'required|alpha_numeric_spaces'
    ),
    array(
    'field' => 'os_telefono',
    'label' => 'TELEFONO',
    'errors'=> array (
      'numeric'  => 'Il telefono puo\' contenere solo numeri',
      'max_length' => 'Massima lunghezza 15 numeri'
    ),
    'rules' => 'numeric|max_length[15]',
    ),
    array(
    'field' => 'os_mobile',
    'label' => 'CELLULARE',
    'errors'=> array (
      'required'  => 'Recapito telefonico destinatario obbligatorio',
      'numeric'  => 'Il campo puo\' contenere solo numeri',
      'max_length' => 'Massima lunghezza 15 numeri'
    ),
    'rules' => 'required|numeric|max_length[15]',
  )
  )

);
