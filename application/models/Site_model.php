<?php

class Site_Model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function header_meta(){
		$sql = "SELECT * FROM tbl_settings WHERE int_sito = ? AND type = 0 ORDER BY int_position";
		$filter = array ( $this->config->item('sito') );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function header_hooks(){
		$sql = "
		SELECT * FROM tbl_settings WHERE int_sito = ? AND bl_attivo = 1";
		$filter = array(4);
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function header_top_right(){
		$sql = "SELECT tbl_pagine.* , tbl_pagine_posizione.posizione
		FROM tbl_pagine_posizione
		INNER JOIN tbl_pagine ON tbl_pagine_posizione.id_pagina = tbl_pagine.id_pagina
		WHERE tbl_pagine_posizione.posizione = ? AND tbl_pagine_posizione.int_sito = ?";
		$filter = array ( 'header_top_right' , $this->config->item('sito') );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function sidebar_menu(){
		$sql = "SELECT tbl_pagine.* , tbl_pagine_posizione.posizione
		FROM tbl_pagine_posizione
		INNER JOIN tbl_pagine ON tbl_pagine_posizione.id_pagina = tbl_pagine.id_pagina
		WHERE tbl_pagine_posizione.posizione = ? AND tbl_pagine_posizione.int_sito = ?";
		$filter = array ( 'sidebar' , $this->config->item('sito') );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function slider(){
		$sql= "SELECT * FROM tbl_slider
		WHERE
		int_sito = ?
		AND
		bl_attivo = 1
		ORDER BY int_order";
		$filter = array ( $this->config->item('sito') );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function banners(){
		$sql = "SELECT * FROM tbl_banners WHERE attivo = 1 AND ( int_sito = ? OR int_sito = 0)";
		$filter = array ( $this->config->item('sito') );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	//for collezioni menu
	function collezioni(){
    $sql = "SELECT *
      FROM tbl_categorie
      WHERE
      id_famiglia = ?
      AND
      bl_attivo = 1
      ORDER BY int_ordine,
      ac_categoria";
		$filter = array ( $this->config->item('sito') );
		$query = $this->runQuery ( $sql , $filter );
		return $query;
    //$query = $this->db->query($sql,$this->config->item('sito'));
    //return($query->result_array());
  }

	function collezioni_qpb(){
		$sql = "SELECT *
		FROM tbl_categorie
		WHERE
			id_famiglia = ?
			AND
			bl_attivo = 1
		ORDER BY
			int_ordine,
			ac_categoria";
		$filter = array ( $this->config->item ( 'qpb') );
		$query = $this->runQuery ( $sql , $filter );
		return $query;
	}

	//collezioni homepage
	function collezioni_home(){
		$sql = "SELECT
			tbl_prodotti.* ,
			tbl_categorie.ac_categoria ,
			tbl_categorie.ac_categoria_lang,
			tbl_categorie.id_lingua_exclusive,
			tbl_categorie.ac_image AS immagine,
			tbl_prodotti.ac_immagine AS immagine_collezione,
			tbl_categorie.ac_mode,
			tbl_categorie.ac_link
		FROM
			tbl_prodotti
		LEFT JOIN tbl_categorie ON tbl_prodotti.id_categoria = tbl_categorie.id_categoria
		WHERE
			tbl_prodotti.bl_stato = 1
		AND
			tbl_prodotti.id_master = 0
		AND
			tbl_categorie.bl_attivo = 1
		AND
			tbl_categorie.id_famiglia = ?
		GROUP BY tbl_prodotti.id_categoria
		ORDER BY tbl_categorie.ac_categoria , tbl_prodotti.int_ordine";
		if ( !$this->config->item('isqpb') ){
			$filter = array($this->config->item('sito'));
		} else {
			$filter = array(8);
		}
		$query = $this->runQuery($sql,$filter);
		return $query;
		//print_r ( $query );
	}

	//collezioni homepage
	function collezioni_home_qpb(){
		$sql = "SELECT
			tbl_prodotti.* ,
			tbl_categorie.ac_categoria ,
			tbl_categorie.ac_categoria_lang,
			tbl_categorie.id_lingua_exclusive,
			tbl_categorie.ac_image AS immagine,
			tbl_prodotti.ac_immagine AS immagine_collezione,
			tbl_categorie.ac_mode
		FROM
			tbl_prodotti
		LEFT JOIN tbl_categorie ON tbl_prodotti.id_categoria = tbl_categorie.id_categoria
		WHERE
			tbl_prodotti.bl_stato = 1
		AND
			tbl_prodotti.id_master = 0
		AND
			tbl_categorie.bl_attivo = 1
		AND
			tbl_categorie.id_famiglia = ?
		GROUP BY tbl_prodotti.id_categoria
		ORDER BY tbl_categorie.ac_categoria , tbl_prodotti.int_ordine";
		$filter = array(8);
		$query = $this->runQuery($sql,$filter);
		return $query;
		//print_r ( $query );
	}

	function collezione($name){
		$c = str_replace('-',' ',$name);
		$sql = "SELECT *
      FROM tbl_categorie
      WHERE
      ac_categoria = ?
      AND
      bl_attivo = 1";
		$filter = array($c);
		$query = $this->runQuery($sql,$filter);
		$id = $query[0]['id_categoria'];
		$sql = "SELECT
			tbl_prodotti.* ,
			tbl_prodotti_descrizione.ac_descrizione AS prodotto_desc,
			tbl_categorie.ac_categoria ,
			tbl_categorie.ac_categoria_lang,
			tbl_categorie.id_lingua_exclusive,
			tbl_categorie.ac_link,
			tbl_categorie.ac_meta_title AS collezione_meta_title,
			tbl_categorie.ac_meta_keys AS collezione_meta_keys,
			tbl_categorie.ac_meta_description AS collezione_meta_description,
			tbl_categorie.ac_SEO_H1 AS SEO_H1,
			tbl_categorie.ac_SEO_Description AS SEO_DESC,
			tbl_designers.*,
			CONCAT(tbl_designers.ac_nome_designer , ' ' , tbl_designers.ac_cognome_designer) AS designer
		FROM
			tbl_prodotti
		LEFT JOIN tbl_categorie ON tbl_prodotti.id_categoria = tbl_categorie.id_categoria
		LEFT JOIN tbl_designers ON tbl_prodotti.id_designer = tbl_designers.id_designer
		LEFT JOIN tbl_prodotti_descrizione ON tbl_prodotti.id_prodotto = tbl_prodotti_descrizione.id_prodotto
		WHERE
			tbl_prodotti.id_categoria = ?
		AND
			bl_stato = 1
		AND
			id_master = 0

		AND tbl_categorie.bl_attivo = 1
		GROUP BY tbl_prodotti.id_prodotto
		ORDER BY tbl_prodotti.int_ordine";
		$filter = array ( $id );
		$qry = $this->runQuery($sql,$filter);
		return $qry;
	}

	function collezione_new($name){
		$c = str_replace('-',' ',$name);
		$sql = "SELECT *
      FROM tbl_categorie
      WHERE
      ac_categoria_lang = ?
      AND
      bl_attivo = 1";
		$filter = array($c);
		$query = $this->runQuery($sql,$filter);
		$id = $query[0]['id_categoria'];

		$sql2 = "SELECT
			tbl_prodotti.* ,
			tbl_prodotti_descrizione.ac_descrizione AS prodotto_desc,
			tbl_categorie.ac_categoria ,
			tbl_categorie.ac_categoria_lang,
			tbl_categorie.id_lingua_exclusive,
			tbl_categorie.ac_link,
			tbl_categorie.ac_meta_title AS collezione_meta_title,
			tbl_categorie.ac_meta_keys AS collezione_meta_keys,
			tbl_categorie.ac_meta_description AS collezione_meta_description,
			tbl_categorie.ac_SEO_H1 AS SEO_H1,
			tbl_categorie.ac_SEO_Description AS SEO_DESC,
			tbl_designers.*,
			CONCAT(tbl_designers.ac_nome_designer , ' ' , tbl_designers.ac_cognome_designer) AS designer
		FROM
			tbl_prodotti
		LEFT JOIN tbl_categorie ON tbl_prodotti.id_categoria = tbl_categorie.id_categoria
		LEFT JOIN tbl_designers ON tbl_prodotti.id_designer = tbl_designers.id_designer
		LEFT JOIN tbl_prodotti_descrizione ON tbl_prodotti.id_prodotto = tbl_prodotti_descrizione.id_prodotto
		WHERE
			tbl_prodotti.id_categoria = ?
		AND
			bl_stato = 1
		AND
			id_master = 0
		AND
			tbl_prodotti_descrizione.id_lingua = 2
		AND tbl_categorie.bl_attivo = 1
		ORDER BY tbl_prodotti.int_ordine";
		$filter = array ( $id );
		$qry = $this->runQuery($sql2,$filter);
		return $qry;
	}

	function collezione_qpb($name){
		$c = str_replace('-',' ',$name);
		$sql = "SELECT *
      FROM tbl_categorie
      WHERE
      ac_categoria = ?
      AND
      bl_attivo = 1
			AND
			id_famiglia = ?";
		$filter = array($c,$this->config->item('qpb'));
		$query = $this->runQuery($sql,$filter);

		$id = $query[0]['id_categoria'];

		$sql2 = "SELECT
			tbl_prodotti.* ,
			tbl_prodotti_descrizione.ac_descrizione AS prodotto_desc,
			tbl_categorie.ac_categoria ,
			tbl_categorie.ac_categoria_lang,
			tbl_categorie.id_lingua_exclusive,
			tbl_categorie.ac_link,
			tbl_categorie.ac_meta_title AS collezione_meta_title,
			tbl_categorie.ac_meta_keys AS collezione_meta_keys,
			tbl_categorie.ac_meta_description AS collezione_meta_description,
			tbl_categorie.ac_SEO_H1 AS SEO_H1,
			tbl_categorie.ac_SEO_Description AS SEO_DESC,
			tbl_designers.*
		FROM
			tbl_prodotti
		LEFT JOIN tbl_categorie ON tbl_prodotti.id_categoria = tbl_categorie.id_categoria
		LEFT JOIN tbl_designers ON tbl_prodotti.id_designer = tbl_designers.id_designer
		LEFT JOIN tbl_prodotti_descrizione ON tbl_prodotti.id_prodotto = tbl_prodotti_descrizione.id_prodotto
		WHERE
			tbl_prodotti.id_categoria = ?
		AND
			bl_stato = 1
		AND
			id_master = 0
		AND tbl_categorie.bl_attivo = 1
		ORDER BY tbl_prodotti.int_ordine";
		$filter = array ( $id );
		$qry = $this->runQuery($sql2,$filter);
		return $qry;
	}

	function prodotti_upsell($id){
		$sql = "SELECT
		tbl_prodotti.* ,
		tbl_categorie.ac_categoria,
		tbl_categorie.ac_categoria_lang,
		tbl_categorie.id_lingua_exclusive,
		tbl_categorie.id_famiglia,
		tbl_categorie.ac_link,
		CONCAT(tbl_designers.ac_nome_designer , ' ' , tbl_designers.ac_cognome_designer) AS designer,
		tbl_prodotti_descrizione.ac_descrizione AS prodotto_description
		FROM tbl_prodotti
		LEFT JOIN tbl_categorie ON tbl_prodotti.id_categoria = tbl_categorie.id_categoria
		LEFT JOIN tbl_designers ON tbl_prodotti.id_designer = tbl_designers.id_designer
		LEFT JOIN tbl_prodotti_descrizione ON tbl_prodotti.id_prodotto = 		tbl_prodotti_descrizione.id_prodotto
		WHERE
			tbl_prodotti.id_prodotto 	IN ( SELECT id_prodotto_upsell FROM tbl_prodotti_upsell WHERE id_prodotto = ? )
		AND
			tbl_prodotti.bl_stato 		= 1
		AND
			tbl_categorie.bl_attivo 	= 1
		GROUP BY tbl_prodotti.id_prodotto";
		$filter = array($id);
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function prezzi_qpb($lista){
		$misure = explode(',',$lista);
		$this->db->select('*');
		$this->db->from('tbl_misure_canvas');
		$this->db->where_in('id_misura', $misure);
		$query = $this->db->get();
		return ($query->result_array());
	}

	function prodotti($id){
		$sql = "SELECT
			tbl_prodotti.* ,
			tbl_prodotti_descrizione.ac_descrizione AS prodotto_desc,
			tbl_categorie.ac_categoria ,
			tbl_categorie.ac_categoria_lang,
			tbl_categorie.id_lingua_exclusive,
			tbl_categorie.ac_link,
			tbl_categorie.ac_meta_title AS collezione_meta_title,
			tbl_categorie.ac_meta_keys AS collezione_meta_keys,
			tbl_categorie.ac_meta_description AS collezione_meta_description,
			tbl_categorie.ac_SEO_H1 AS SEO_H1,
			tbl_categorie.ac_SEO_Description AS SEO_DESC,
			tbl_designers.*,
			CONCAT(tbl_designers.ac_nome_designer , ' ' , tbl_designers.ac_cognome_designer) AS designer
		FROM
			tbl_prodotti
		LEFT JOIN tbl_categorie ON tbl_prodotti.id_categoria = tbl_categorie.id_categoria
		LEFT JOIN tbl_designers ON tbl_prodotti.id_designer = tbl_designers.id_designer
		LEFT JOIN tbl_prodotti_descrizione ON tbl_prodotti.id_prodotto = tbl_prodotti_descrizione.id_prodotto
		WHERE
			tbl_prodotti.id_categoria = ?
		AND
			bl_stato = 1
		AND
			id_master = 0
		AND
			tbl_prodotti_descrizione.id_lingua = 2
		AND tbl_categorie.bl_attivo = 1
		ORDER BY tbl_prodotti.int_ordine";
		$filter = array($id);
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function prodotto($id){
		$sql="SELECT
		tbl_prodotti.* ,
		tbl_categorie.ac_categoria,
		tbl_categorie.ac_categoria_lang,
		tbl_categorie.id_lingua_exclusive,
		tbl_categorie.id_famiglia,
		CONCAT(tbl_designers.ac_nome_designer , ' ' , tbl_designers.ac_cognome_designer) AS designer,
		tbl_prodotti_descrizione.ac_descrizione AS prodotto_description
		FROM tbl_prodotti
		LEFT JOIN tbl_categorie ON tbl_prodotti.id_categoria = tbl_categorie.id_categoria
		LEFT JOIN tbl_designers ON tbl_prodotti.id_designer = tbl_designers.id_designer
		LEFT JOIN tbl_prodotti_descrizione ON tbl_prodotti.id_prodotto = tbl_prodotti_descrizione.id_prodotto
		WHERE
		tbl_prodotti.id_prodotto 	= ?
		AND
		tbl_prodotti.bl_stato 		= 1
		GROUP BY tbl_prodotti.id_prodotto";
		$filter = array ( $id );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function prodotto_descrizione($id){
		$sql = "SELECT * FROM tbl_prodotti_descrizione WHERE id_prodotto = ? AND id_lingua = 2";
		$filter = array ( $id );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}


	function prodotto_gallery($id){
		$sql = "SELECT * FROM tbl_prodotti_immagini WHERE id_prodotto = ?";
		$filter = array ( $id );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function prodotto_tabs(){
		$sql = "SELECT tbl_pagine.* , tbl_pagine_posizione.posizione
		FROM tbl_pagine_posizione
		INNER JOIN tbl_pagine ON tbl_pagine_posizione.id_pagina = tbl_pagine.id_pagina
		WHERE tbl_pagine_posizione.posizione = ? AND tbl_pagine_posizione.int_sito = ?";
		$filter = array ( 'tab_prodotto' , $this->config->item('sito') );
		$query = $this->runQuery($sql,$filter);
		return $query;
		//$sql = "SELECT * FROM tbl_contenuti WHERE id_sezione = -1";
		//$filter = array('');
		//$query = $this->runQuery($sql,$filter);
		//return $query;
	}

	function prodotto_tabs_qpb(){
		$sql = "SELECT tbl_pagine.* , tbl_pagine_posizione.posizione
		FROM tbl_pagine_posizione
		INNER JOIN tbl_pagine ON tbl_pagine_posizione.id_pagina = tbl_pagine.id_pagina
		WHERE tbl_pagine_posizione.posizione = ? AND tbl_pagine_posizione.int_sito = ?";
		$filter = array ( 'tab_prodotto' , 8 );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function prodotto_simulatore($id){
		$sql = "SELECT * FROM tbl_prodotti_simulatore WHERE id_prodotto = ?";
		$filter = array ( $id );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function fonts(){
		$sql = "SELECT * FROM tbl_fonts WHERE status = ?";
		$filter = array (1);
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function stickytext_description(){
		$sql = "Select
tbl_categorie.ac_meta_title,
tbl_categorie.ac_meta_keys,
tbl_categorie.ac_meta_description,
tbl_categorie.ac_link,
tbl_categorie.ac_SEO_H1,
tbl_categorie.ac_SEO_Description AS stickytext_description
FROM tbl_categorie
WHERE id_categoria = ?";
		$filter = array(24);
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function spedizioni(){
		$sql = "SELECT * FROM tbl_spedizioni";
		$query = $this->db->query($sql);
    return($query->result_array());
	}

	function coupon($coupon){
		$now = date('Y-m-d',time());
		$sql = "SELECT * FROM tbl_coupons WHERE ac_codice = ?
		AND
			bl_attivo = 1";
		$filter = array ( $coupon );
		$query = $this->runQuery($sql,$filter);
		if ( count($query) > 0 ){
			$coupon_data = array (
				'valore' => $query[0]['ac_valore'],
				'tipo'	 => $query[0]['bl_valore']
			);
			return $coupon_data;
			//return $query[0]['ac_valore'];
		} else {
			return 0;
		}
	}


	function piu_venduti(){
		$anno = date("Y", time());
		$now = date('Y-m-d',time());
		$anno = $anno-2;
		$sql = "SELECT
			COUNT(tbl_ordini.id_prodotto) AS venduti ,
			tbl_prodotti.*,
			tbl_categorie.ac_categoria,
			tbl_categorie.ac_categoria_lang,
			tbl_categorie.id_lingua_exclusive,
			tbl_categorie.ac_mode,
			tbl_categorie.ac_link,
			CONCAT(tbl_designers.ac_nome_designer , ' ' , tbl_designers.ac_cognome_designer) AS designer
			FROM tbl_ordini
			INNER JOIN tbl_prodotti ON tbl_ordini.id_prodotto = tbl_prodotti.id_prodotto
			INNER JOIN tbl_categorie ON tbl_prodotti.id_categoria = tbl_categorie.id_categoria
			LEFT JOIN tbl_designers ON tbl_prodotti.id_designer = tbl_designers.id_designer
			WHERE
				( YEAR(dt_ordine) >= ? AND dt_ordine <= ? )
				AND
				tbl_ordini.bl_status > 1
				AND tbl_categorie.id_famiglia = ?
				AND tbl_prodotti.bl_stato = 1
				GROUP BY tbl_prodotti.id_prodotto
				ORDER BY venduti DESC
				LIMIT 0,12";
			$filter = array ( $anno , (string)$now , $this->config->item('sito') );
			if ( $this->config->item('isqpb') ){
				$filter = array ( $anno , (string)$now , 8 );
			}
			$query = $this->runQuery($sql,$filter);
			return $query;
	}

	function offerte (){
		$sql = "SELECT
			tbl_prodotti.* ,
			tbl_categorie.ac_categoria ,
			tbl_categorie.ac_categoria_lang,
			tbl_categorie.id_lingua_exclusive,
			tbl_categorie.ac_image AS immagine,
			tbl_categorie.ac_mode
		FROM
			tbl_prodotti
		LEFT JOIN tbl_categorie ON tbl_prodotti.id_categoria = tbl_categorie.id_categoria

		WHERE
			tbl_prodotti.bl_stato = 1
		AND
			tbl_prodotti.id_master = 0
		AND
			tbl_categorie.bl_attivo = 1
		AND
			tbl_categorie.id_famiglia = ?
					AND
							(tbl_prodotti.ac_coefficiente_sconto < 1 AND tbl_prodotti.ac_coefficiente_sconto > 0)

		ORDER BY tbl_categorie.ac_categoria , tbl_prodotti.int_ordine";
		$filter = array ( $this->config->item('sito') );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function promo (){
		$sql = "SELECT
			tbl_prodotti.* ,
			tbl_categorie.ac_categoria ,
			tbl_categorie.ac_categoria_lang,
			tbl_categorie.id_lingua_exclusive,
			tbl_categorie.ac_image AS immagine,
			tbl_categorie.ac_mode,
			tbl_prodotti_promo.ac_prezzo_promo AS prezzo_promo,
			tbl_prodotti_promo.ac_sconto AS sconto_promo,
			tbl_prodotti_promo.ac_qty AS qty_promo,
			(tbl_prodotti_promo.ac_qty-tbl_prodotti_promo.ac_qty_used) AS available_promo
		FROM
			tbl_prodotti
		LEFT JOIN tbl_categorie ON tbl_prodotti.id_categoria = tbl_categorie.id_categoria
		INNER JOIN tbl_prodotti_promo ON tbl_prodotti.id_prodotto = tbl_prodotti_promo.id_prodotto
		WHERE
			tbl_prodotti.bl_stato = 1
		AND
			tbl_prodotti.id_master = 0
		AND
			tbl_categorie.bl_attivo = 1
		AND
			tbl_categorie.id_famiglia = ?
		ORDER BY tbl_categorie.ac_categoria , tbl_prodotti.int_ordine";
		$filter = array ( $this->config->item('sito') );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}
	/*
	function pagina($id){
		$sql = "SELECT
			tbl_contenuti.*,
			tbl_sezioni.ac_sezione,
			tbl_sezioni.ac_link,
			tbl_aree.int_sito
		FROM tbl_contenuti
		INNER JOIN tbl_sezioni ON tbl_contenuti.id_sezione = tbl_sezioni.id_sezione
		INNER JOIN tbl_aree ON tbl_sezioni.id_area = tbl_aree.id_area
		WHERE
				tbl_sezioni.id_sezione = ?
		AND
			int_lang = 2
		AND
			tbl_contenuti.bl_attivo = 1";
		$filter = array ( $id );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}
	*/

	function pagina($url){
		$sql = "SELECT
			tbl_pagine.*
		FROM tbl_pagine
		WHERE
				tbl_pagine.ac_url = ?
		AND
			tbl_pagine.bl_status = 1";
		$filter = array ( $url);
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function designers(){
		$sql = "SELECT * FROM tbl_designers WHERE bl_status = 1 AND bl_sticasa = ? ORDER BY ac_cognome_designer, ac_nome_designer";
		$filter = array ( 1 );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function designer($id){
		$sql = "SELECT
		tbl_designers.* ,
		tbl_prodotti.id_categoria,
		tbl_prodotti.ac_prodotto,
		tbl_prodotti.int_ordine,
		tbl_prodotti.id_prodotto,
		tbl_prodotti.ac_immagine,
		tbl_prodotti.ac_ratio,
		tbl_categorie.int_ordine,
		tbl_categorie.ac_categoria,
		tbl_categorie.ac_categoria_lang
		FROM tbl_designers
		INNER JOIN tbl_prodotti ON tbl_designers.id_designer = tbl_prodotti.id_designer
		INNER JOIN tbl_categorie ON tbl_prodotti.id_categoria = tbl_categorie.id_categoria
		WHERE tbl_designers.id_designer = ? AND tbl_prodotti.bl_stato = 1 AND tbl_prodotti.id_master = 0
		AND tbl_designers.bl_sticasa = 1
		AND tbl_categorie.id_famiglia = ?
		ORDER BY tbl_designers.id_designer , tbl_categorie.int_ordine";
		$filter = array ( $id , $this->config->item('sito') );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}
/*
	function pagine_footer($colonna){
		$sql = "
		Select
			tbl_aree.ac_area,
			tbl_aree.int_sito,
			tbl_aree.bl_menu,
			tbl_sezioni.id_sezione,
			tbl_sezioni.int_subfolder,
			tbl_sezioni.ac_sezione,
			tbl_sezioni.ac_descrizione,
			tbl_sezioni.bl_attivo
		From
			tbl_sezioni
			LEFT Join tbl_contenuti ON tbl_sezioni.id_sezione = tbl_contenuti.id_sezione
			INNER JOIN tbl_aree ON tbl_sezioni.id_area = tbl_aree.id_area
			WHERE
				tbl_aree.int_lang = 2
				AND
				tbl_aree.int_sito = ?
				AND
				tbl_sezioni.bl_attivo = 1
				AND tbl_sezioni.ac_menu = ?
			GROUP BY id_sezione
			ORDER BY tbl_sezioni.int_ordine";
		$filter = array ( $this->config->item('sito'), $colonna );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}
*/
	function pagine_footer($id){
		$sql = "SELECT tbl_pagine.* , tbl_pagine_posizione.posizione
		FROM tbl_pagine_posizione
		INNER JOIN tbl_pagine ON tbl_pagine_posizione.id_pagina = tbl_pagine.id_pagina
		WHERE tbl_pagine_posizione.posizione = ? AND tbl_pagine_posizione.int_sito = ?";
		$filter = array ( 'footer_col_'.$id , $this->config->item('sito') );
		$query = $this->runQuery($sql,$filter);
		return $query;
		/*$filter = 'footer_col_'.$id;
		$this->db->select('*');
		$this->db->where('ac_menu',$filter);
		$this->db->order_by('int_ordine');
		$query = $this->db->get('tbl_pagine');
		return($query->result_array());*/
	}

	function colori(){
		$sql = "SELECT * FROM tbl_colori WHERE bl_stato = ?";
		$filter = array (1);
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function pricing (){
		$sql = 'SELECT * FROM tbl_pricing';
		$query = $this->db->query($sql);
    return($query->result_array());
	}

	function search(){
		$sql = "SELECT
			tbl_prodotti.* ,
			tbl_prodotti_descrizione.ac_descrizione AS prodotto_desc,
			tbl_categorie.ac_categoria ,
			tbl_categorie.ac_categoria_lang,
			tbl_categorie.id_lingua_exclusive,
			tbl_categorie.ac_meta_title AS collezione_meta_title,
			tbl_categorie.ac_meta_keys AS collezione_meta_keys,
			tbl_categorie.ac_meta_description AS collezione_meta_description,
			tbl_categorie.ac_SEO_H1 AS SEO_H1,
			tbl_categorie.ac_SEO_Description AS SEO_DESC,
			tbl_designers.*,
			CONCAT(tbl_designers.ac_nome_designer , ' ' , tbl_designers.ac_cognome_designer) AS designer
		FROM
			tbl_prodotti
		LEFT JOIN tbl_categorie ON tbl_prodotti.id_categoria = tbl_categorie.id_categoria
		LEFT JOIN tbl_designers ON tbl_prodotti.id_designer = tbl_designers.id_designer
		LEFT JOIN tbl_prodotti_descrizione ON tbl_prodotti.id_prodotto = tbl_prodotti_descrizione.id_prodotto
		WHERE
			tbl_prodotti.ac_prodotto LIKE ?
		AND
			bl_stato = 1
		AND
			id_master = 0
		AND
			tbl_prodotti_descrizione.id_lingua = 2
		AND tbl_categorie.bl_attivo = 1
		ORDER BY tbl_prodotti.int_ordine";
		$filter = array('%'.$_POST['search'].'%');
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function runQuery($sql,$filter){
    $query = $this->db->query($sql,$filter);
    return($query->result_array());
	}

}
