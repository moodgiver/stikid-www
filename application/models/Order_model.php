<?php

class Order_Model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

  public function numero_ordine(){
    $this->db->query('LOCK TABLES tbl_nrordini  WRITE');
    $sql = "SELECT * FROM tbl_nrordini";
    $query = $this->db->query($sql);
    $orderdata = $query->result_array();
    $id = $orderdata[0]['id_nrordine'];
    $nrordine = (int)$orderdata[0]['ac_nrordine'];
    $nrordine++;
    $params = array (
      'ac_nrordine' => (string)$nrordine
    );
    $this->db->set(	$params );
		$this->db->where ( 'id_nrordine' , $id );
		$this->db->update ( 'tbl_nrordini' );
    $this->db->query('UNLOCK TABLES');
    return $nrordine;
  }

  public function save_preorder(){
    $ordine_delete = (string)$this->session->order['id'].'/'.mdate('%Y',time());
    $ordine_data = mdate('%Y-%m-%d %h:%i %a',time());
    $this->db->where('ac_nrordine',$ordine_delete);
    $this->db->delete('tbl_ordini_temp');
    foreach ( $this->session->cart_items AS $item ){

      $cart_line = array (
      'id_lingua'     => 2,
      'ac_nrordine'   => (string)$this->session->order['id'].'/'.mdate('%Y',time()),
      'ac_uuid'       => (string)$this->session->user['customer_id'],
      'ac_cfid'       => (string)$this->session->user['ip'],
      'dt_ordine'     => adesso(),
      'id_utente'     => (string)$this->session->user['customer_id'],
      'id_prodotto'   => $item['id'],
      'ac_width'      => $item['w'],
      'ac_height'     => $item['h'],
      'ac_ratio'      => '',
      'ac_area'       => (float)$item['w']*(float)$item['h'],
      'ac_prezzo'     => $item['prezzo'],
      'ac_cambio'     => '1',
      'ac_colore'     => $item['colore'],
      'bl_flip'       => false,
      'ac_testo'      => $item['testo'],
      'ac_telaio'     => $item['telaio'],
      'ac_fontsize'   => $item['font'],
      'ac_qty'        => $item['qty'],
      'ac_totale'     => (string)(float)$item['prezzo']*(float)$item['qty'],
      'bl_status'     => 1,
      'ac_crop'       => 'nocrop',
      'ac_biglietto'  => ''
      );
      $this->db->insert('tbl_ordini_temp', $cart_line );
    }
    $shipto = $_POST['o_shipto'];

    $this->db->where('ac_nrordine',$ordine_delete);
    $this->db->delete('tbl_ordini_summary_temp');

		$myordine = array (
      'id_utente'					    => $this->session->user['customer_id'],
      'ac_nrordine'					  => (string)$this->session->order['id'].'/'.mdate('%Y',time()),
      'ac_totale'					    => $this->session->order['ORDER_TOTAL'],
      'ac_spesespedizione'		=> $this->session->order['SHIPPING'],
      'ac_sconto'					    => $this->session->order['COUPON_VALUE'],//(string)$coupon_value,
      'ac_sconto_riservato'		=> '0',
      'ac_coupon'					    => $this->session->order['COUPON'],
      'ac_pagamento'				  => '',
      'dt_pagamento'			    => adesso(),
      'bl_fattura'					  => false,
      'ac_azienda_fattura'		=> $this->session->order['O_AZIENDA']	,
      'ac_indirizzo_fattura'	=> $this->session->order['O_INDIRIZZO'],
      'ac_citta_fattura'			=> $this->session->order['O_CITTA'],
      'ac_prov_fattura'				=> $this->session->order['O_PV'],
      'ac_cap_fattura'				=> $this->session->order['O_CAP'],
      'ac_country_fattura'		=> $this->session->order['O_NAZIONE']	,
      'ac_cf_fattura'				  => '',
      'ac_piva_fattura'				=> '',
      'bl_delivery_delivery'	=> $shipto,
      'ac_firstname_delivery'	=> $_POST['os_nome']	,
      'ac_lastname_delivery'	=> $_POST['os_cognome']	,
      'ac_company_delivery'		=> $_POST['os_azienda']	,
      'ac_address_delivery'		=> $_POST['os_indirizzo'],
      'ac_city_delivery'			=> $_POST['os_citta'],
      'ac_state_delivery'			=> $_POST['os_pv'],
      'ac_country_delivery'		=> $_POST['os_nazione']	,
      'ac_zip_delivery'				=> $_POST['os_cap']	,
      'ac_phone_delivery'			=> $_POST['os_telefono']	,
      'ac_mobile_delivery'		=> $_POST['os_mobile']	,
      'bl_stato'					    => 1,
      'bl_regalo'					    => 0,
      'ac_biglietto'          => ''
    );
    $this->db->insert('tbl_ordini_summary_temp', $myordine );

		$updateOrder = $this->session->order;

		$updateOrder['SHIPTO']			= $shipto;
		$updateOrder['OS_NOME'] 		= $_POST['os_nome'];
		$udpateOrder['OS_COGNOME']	= $_POST['os_cognome'];
		$updateOrder['OS_AZIENDA']	= $_POST['os_azienda'];
		$updateOrder['OS_INDIRIZZO']= $_POST['os_indirizzo'];
		$updateOrder['OS_CITTA']    = $_POST['os_citta'];
		$updateOrder['OS_PV']				= $_POST['os_pv'];
		$updateOrder['OS_NAZIONE']	= $_POST['os_nazione'];
		$updateOrder['OS_CAP']			= $_POST['os_cap'];
		$updateOrder['OS_TELEFONO'] = $_POST['os_telefono'];
		$updateOrder['OS_MOBILE']		= $_POST['os_mobile'];
		$updateOrder['OS_LASTNAME'] = $_POST['os_cognome'];
		$this->session->set_userdata('order',$updateOrder);
    return true;
  }

  function save_order($str){
    $sql = "INSERT INTO tbl_ordini
            SELECT d.*
            FROM tbl_ordini_temp d
            WHERE ac_nrordine = ?";

    $filter = array ($this->session->order['id'].'/'.mdate('%Y',time()));
    $result = $this->db->query($sql,$filter);
    application_log('Ordine '.$this->session->order['id'].' created in ORDINI => '.$result,'INFO');
    $pagamento = array (
        'ac_pagamento' => $str
    );
    $this->db->where('ac_nrordine',$this->session->order['id'].'/'.mdate('%Y',time()));
    $this->db->update('tbl_ordini',$pagamento);

    $sql = "INSERT INTO tbl_ordini_summary
            SELECT d.*
            FROM tbl_ordini_summary_temp d
            WHERE ac_nrordine = ?";

    $filter = array ($this->session->order['id'].'/'.mdate('%Y',time()));
    $result = $this->db->query($sql,$filter);
    application_log('Ordine '.$this->session->order['id'].' created in ORDINI SUMMARY => '.$result,'INFO');
		$status = 1;
		if ( $str == 'BONIFICO' ) {
			$status = -1;
		}
    $pagamento = array (
        'ac_pagamento' 	=> $str,
				'bl_stato'			=> $status
    );
    $this->db->where('ac_nrordine',$this->session->order['id'].'/'.mdate('%Y',time()));
    $this->db->update('tbl_ordini_summary',$pagamento);
    return true;

  }

	function order_summary_temp($id){
		$nr = $id.'/'.this_year();
		$this->db->select('*');
		$this->db->where('ac_nrordine',$nr);
		$query = $this->db->get('tbl_ordini_summary_temp');
		return $query->result_array();
	}

}
