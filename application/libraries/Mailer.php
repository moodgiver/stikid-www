<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * UUID Class
 *
 * This implements the abilities to create UUID's for CodeIgniter.
 * Code has been borrowed from the followinf comments on php.net
 * and has been optimized for CodeIgniter use.
 * http://www.php.net/manual/en/function.uniqid.php#94959
 *
 * @category Libraries
 * @author Antonio Nardone
 * @link
 * @license GNU LPGL
 * @version 2.1
 */
class Mailer
{

  protected $CI;

  // We'll use a constructor, as you can't directly call a function
  // from a property definition.
  public function __construct()
  {
    // Assign the CodeIgniter super-object
    $this->CI =& get_instance();
    $this->CI->load->model('Site_model');
    $this->CI->load->model('Order_model');
    $this->CI->load->library('email'); // load email library
  }

  public function conferma_ordine($pagamento,$summary){
    $ordine_summary = $summary['ordine_summary'];
    $this->CI->email->from('servizioclienti@sticasa.com', 'STICASA / STIKID');
    $this->CI->email->bcc('sticasa@sticasa.com');
    $this->CI->email->to($this->CI->session->user['email']);
    $this->CI->email->subject("ORDINE STICASA/STIKID ".$this->CI->session->order['id'].'/'.this_year());
    $cart = $this->CI->session->cart_items;
    $order = $this->CI->session->order;
    $content = '';
    foreach ( $cart AS $itm ){
      $imgURL = 'http://static.stikid.com/images/ambienti/small/';
      $telaio = '';
      if ( $itm['telaio'] != '' ){
        $imgURL = 'http://static.stikid.com/images/canvas/preview/';
        $telaio = '<br>Telaio '.$itm['telaio'].' cm';
      }
      $totale_riga = number_format((float)$itm['qty']*(float)$itm['prezzo'],2);

      $content .= '
          <tr>
            <td width="110">
              <img src="'.$imgURL.''.$itm['thumb'].'" width="100">
            </td>
            <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top">'.$itm['prodotto'].'<br><small>Collezione: '.$itm['collezione'].'</small>'.str_replace('|','<br>',$itm['testo']).'</td>
            <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top">'.$itm['colore'].'</td>
            <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top"><cfif itm.telaio EQ "">'.$itm['w'].' x '.$itm['h'].''.$telaio.'</td>
            <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="center">'.$itm['qty'].'</td>
            <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">'.$itm['prezzo'].'</td>
            <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">'.$totale_riga.'</td>
          </tr>
          <tr>
            <td colspan="7"><hr></td>
          </tr>';

    }


    $message = '
    <!DOCUMENT html>
    <html>
    <head>
      <link href="http://www.stikid.com/css/style.css" rel="stylesheet">
      <style>
        td , th , tbody , thead { font-family:Verdana,Helvetica; font-size:1em;}
        .col-center { text-align:center; }
        .col-right { text-align:right; }
      </style>
    </head>
    <body style="background:#fff">
    <img src="http://static.stikid.com/images/email-logo.jpg" alt="Home" title="Home"/>
    <h2>STICASA/STIKID Ordine nr. '.$order['id'].'/'.this_year().'</h2>
    <table width="100%" cellpadding="4" cellspacing="0" border="0" style="border:1px solid ##cecece;background:#fff">
      <thead style="background:##cecece">
        <th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"><strong>Prodotto</strong></th>
        <th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"></th>
        <th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"><strong>Colore</strong></th>
        <th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"><strong>LxA cm</strong></th>
        <th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"><strong>Q.ta\'</strong></th>
        <th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"><strong>P.U.</strong></th>
        <th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"><strong>Totale</strong></th>
      </thead>
      <tbody class="print">';
          $message .= $content;

          $message .= '
          <tr>
            <td colspan="6" align="right" style="font-size:1em;font-family:Verdana,Helvetica" valign="top">Totale</td>
            <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">'.number_format($order['CART_TOTAL'],2).'</td>
          </tr>
          <tr>
            <td colspan="6" style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">Coupon</td>
            <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">'.number_format($order['COUPON_VALUE'],2).'</td>
          </tr>
          <tr>
            <td colspan="6" style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">Totale</td>
            <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">'.number_format($order['SHIPPING'],2).'</td>
          </tr>
          <tr>
            <td colspan="6" style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">Totale</td>
            <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">'.number_format($order['ORDER_TOTAL'],2).'</td>
          </tr>
          <tr>
            <td colspan="7"><hr></td>
          </tr>

      </tbody>
      <tfoot class="print-footer">
        <tr>
          <td colspan="7" style="font-size:1em;font-family:Verdana,Helvetica" valign="top"><h3>Cliente</h3>
          '.$order['O_NOME'].'&nbsp;'.$order['O_COGNOME'].'<br>
          '.$order['O_INDIRIZZO'].'<br>
          '.$order['O_CAP'].' '.$order['O_CITTA'].' '.$order['O_PV'].' '.$order['O_NAZIONE'].'<br>
          Telefono: '.$order['O_TELEFONO'].'<br>
          Cellulare: '.$order['O_MOBILE'].'
          </td>
        </tr>';
        if ( $order['SHIPTO'] ){
          $message .= '
          <tr>
          <td colspan="7" style="font-size:1em;font-family:Verdana,Helvetica" valign="top"><h3>Spedisci a</h3>
          '.$ordine_summary[0]['ac_firstname_delivery'].' '.$ordine_summary[0]['ac_lastname_delivery'].'<br>
          '.$ordine_summary[0]['ac_address_delivery'].'<br>
          '.$ordine_summary[0]['ac_zip_delivery'].' '.$ordine_summary[0]['ac_city_delivery'].' '.$ordine_summary[0]['ac_state_delivery'].' '.$ordine_summary[0]['ac_country_delivery'].'<br>
          Telefono: '.$ordine_summary[0]['ac_phone_delivery'].'<br>
          Cellulare: '.$ordine_summary[0]['ac_mobile_delivery'].'
          </td>
          </tr>';
        }
        $message .= '<tr>
          <td colspan="7" style="font-size:1em;font-family:Verdana,Helvetica" valign="top">
          <br><br>
          Per qualsiasi richiesta o chiarimento si prega di contattare l\'assistenza clienti scrivendo un\'e-mail a  servizioclienti@sticasa.com e indicando il numero d\'ordine nr. '.$order['id'].'/'.this_year().'
          </td>
        </tr>';
        if ( $pagamento == 'BONIFICO' ){
          $message .= '
            <tr>
              <td colspan="7"  style="font-size:1em;font-family:Verdana,Helvetica" valign="top">
              <small>Dati Bonifico (per pagamenti tramite bonifico)<br>
              <strong>DUAL SRL - IBAN IT64Q0200801634000102842164 - UNICREDIT</strong><br>
              Indicare nel bonifico</br>
              ORDINE STICASA/STIKID NR. '.$order['id'].'/'.this_year().'</small>
              </td>
            </tr>';
        } else {
          $message .= '
          <tr>
          <td colspan="7"  style="font-size:1em;font-family:Verdana,Helvetica" valign="top">
          PAGAMENTO: CC/PAYPAL
          </td>
          </tr>';
        }
    $message .= '</tfoot>
    </table>
    </body>
    </html>';
    $this->CI->email->message($message);
    $noerr = $this->CI->email->send();
    if ( $noerr ){
      $params = array(
        'tipo'    => 'CONFERMA ORDINE '.$pagamento,
        'email'   => $this->CI->session->user['email'],
        'ordine'  => $this->CI->session->order['id'].'/'.this_year(),
        'subject' => "ORDINE STICASA/STIKID ".$this->CI->session->order['id'].'/'.this_year(),
        'message' => $content,
        'status'  => $noerr
      );
      $this->CI->db->insert('tbl_emails_logs',$params);
      return true;
    } else {
      $params = array(
        'tipo'    => 'CONFERMA ORDINE '.$pagamento,
        'email'   => $this->CI->session->user['email'],
        'ordine'  => $this->CI->session->ordine['id'],
        'subject' => "ORDINE STICASA/STIKID ".$this->CI->session->order['id'].'/'.this_year(),
        'message' => $content,
        'status'  => $noerr
      );
      $this->CI->db->insert('tbl_emails_logs',$params);
      return $noerr;
    }
  }

  function send_reset_password($email,$uuid){
    $this->CI->email->from('servizioclienti@sticasa.com', 'STICASA / STIKID');
    $this->CI->email->bcc('sticasa@sticasa.com');
    $this->CI->email->to($email);//$email;
    $this->CI->email->subject("STICASA/STIKID REIMPOSTAZIONE PASSWORD");
    $message = 'E\' stata richiesta la reimpostazione della password del tuo account STICASA/STIKID';
    $message .= '<br>Per procedere con la reimpostazione <a href="'.base_url().'utente/account/reset/'.$uuid.'">clicca qui</a>';
    $this->CI->email->message($message);
    $result = $this->CI->email->send();
      $params = array(
        'tipo'    => 'RESET PASSWORD',
        'email'   => $email,
        'ordine'  => '-',
        'subject' => "STICASA/STIKID REIMPOSTAZIONE PASSWORD",
        'message' => $message,
        'status'  => $result
      );
      $this->CI->db->insert('tbl_emails_logs',$params);
    return $result;
  }
}
