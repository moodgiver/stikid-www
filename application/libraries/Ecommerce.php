<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Ecommerce Class
 *
 * This implements the functionalities for ecommerce.
 *
 * @category Libraries
 * @author Antonio Nardone
 * @link
 * @license GNU LPGL
 * @version 2.1
 */
class Ecommerce
{

  protected $CI;

  // We'll use a constructor, as you can't directly call a function
  // from a property definition.
  public function __construct()
  {
    // Assign the CodeIgniter super-object
    $this->CI =& get_instance();
    $this->CI->load->model('Site_model');
    $this->CI->load->model('Order_model');
  }

  //init session variables
  public function init_session(){
    //user session
    if ( !$this->CI->session->userdata('user') ){
			$session_user = array (
				'islogged' => false,
				'email' => '',
				'customer' => '',
				'customer_id' => 0
			);
			$this->CI->session->set_userdata('user', $session_user);
		}

    //order session
		if ( !$this->CI->session->userdata('order') ){
	    $order = array(
			'OS_AZIENDA'		=> '',
			'OS_CAP'				=> '',
			'OS_CITTA' 			=> '',
			'OS_COGNOME'		=> '',
			'OS_EMAIL' 			=> '',
			'OS_INDIRIZZO' 	=> '',
			'OS_MOBILE' 		=> '',
			'OS_NAZIONE'		=> '',
			'OS_NOME' 			=> '',
			'OS_PV' 				=> '',
			'OS_TELEFONO' 	=> '',
			'O_AZIENDA' 		=> '',
			'O_CAP' 				=>'',
			'O_CITTA' 			=> '',
			'O_COGNOME' 		=> '',
			'O_EMAIL' 			=> '',
			'O_INDIRIZZO' 	=> '',
			'O_MOBILE' 			=> '',
			'O_NAZIONE' 		=> '',
			'O_NOME' 				=> '',
			'O_PV' 					=> '',
			'O_TELEFONO' 		=> '',
			'SHIPPING' 			=> 0,
			'id' 						=> '',
			'SHIPTO' 				=> false,
			'CART_TOTAL' 		=> 0,
			'COUPON' 				=> '',
			'COUPON_VALUE' 	=> 0,
			'MESSAGE' 			=> '',
			'ORDER_TOTAL' 	=> 0,
			'FATTURA' 			=> false,
			'CF' 						=> '',
			'PIVA' 					=> '',
			'REGALO'				=> false,
			'BIGLIETTO_TESTO'	=> '',
			'MESSAGE' 			=> '',
			'ISCART'				=> 1
		  );
      $this->CI->session->set_userdata('order',$order);
    }

    if ( !$this->CI->session->userdata('cart_items') ){
			$items = [];
			$this->CI->session->set_userdata('cart_items', $items );
		}
    return true;
  }
  //.init_session


  //funzioni ecommerce generali

  //price() : calcola prezzo sticker (ajax/calcolo_prezzo)
  //@w : width
  //@h : height
  //@c : coefficiente sconto (se < 1 sconto in % es. 0.80 = 20% sconto )
  //return => prezzo ( 99999,99 );
  function price($w,$h,$s)
  {

    // You may need to load the model if it hasn't been pre-loaded
    $this->CI->load->model('Site_model');
    $pricing = $this->CI->Site_model->pricing();
    $prezzoFinale = calcola_prezzo_finale($pricing,$w,$h,$s);
    $areaDiff = 0;
    $prezzoDiff = 0;

    $area_minima = (float)((float)$w*(float)$h)/100;
    $prezzoFinale = $pricing[0]['ac_prezzo_reale'];
    $a = 0;
    $cs = 1;

    foreach ( $pricing AS $i ){
      //$nextArea = (int)$pricing[$a+1]['ac_area'];
      //$nextPrezzoMQ = $pricing[$a+1]['ac_prezzo'];
      $nextArea = (int)$i['ac_area'];
      $nextPrezzoMQ = $i['ac_prezzo'];

      if ( (int)$area_minima > (int)$i['ac_area']
            && $a > 0
            && (int)$area_minima < (int)$pricing[$a+1]['ac_area'] ) {
        $prevArea = (int)$pricing[$a-1]['ac_area'];
        $startingPrice = $pricing[$a-1]['ac_prezzo_reale'];
        $areaDiff = (int)$area_minima - (int)$pricing[$a-1]['ac_area'];
        if ( $areaDiff > 0 ){
          $prezzoDiff = ((int)$areaDiff * (int)$nextPrezzoMQ)/100;
          $prezzoFinale = (int)((int)$startingPrice + $prezzoDiff)*$s;
        } else {
          $prezzoFinale = $pricing[$a]['ac_prezzo_reale'];
        }
        break;
      } else {
        if ( (int)$area_minima == (int)$i['ac_area'] ){
          $prezzoFinale = $pricing[$a]['ac_prezzo_reale'];
        }
      }
      $a++;
    }
    $prezzoFinale = abs($prezzoFinale);
    if ( $prezzoFinale < 15 ){
      $prezzoFinale = 15;
    }
    return number_format($prezzoFinale,2);
  }
  //.price()

  //prezzo_prodotto() : calcolo prezzo sticker (replace calcolo_prezzo)
  //@w : width
  //@h : height
  //return => prezzo ( 99999,99 );
  function prezzo_prodotto($w,$h){
    $this->CI->load->model('Site_model');
    $prezzi = $this->CI->Site_model->pricing();
    $n = 0;
    foreach ( $prezzi AS $row ){
      $area_minima = (int)($w*$h)/100;
      if ( (float)$area_minima <= (float)$row['ac_area'] ){
        //echo $area_minima .'=>'.$row['ac_area'].'=>'.$prezzi[$n-1]['ac_prezzo_reale'];
        if ( $n > 0 ){
          $areaBase = $prezzi[$n-1]['ac_area'];
        } else {
          $areaBase = $prezzi[$n]['ac_area'];
        }
        $areaDiff = $area_minima-$areaBase;
        //echo $areaBase;
        $nextArea = $row['ac_area'];
        if ( $n > 0 ){
          $prezzoBase = $prezzi[$n-1]['ac_prezzo_reale'];
        } else {
          $prezzoBase = $prezzi[$n]['ac_prezzo_reale'];
        }
        $nextPrezzo = $row['ac_prezzo'];
        $prezzoDiff = $row['ac_prezzo']*$areaDiff/100;
        $prezzoFinale = (int)($prezzoBase + $prezzoDiff);
        return $prezzoFinale;
        break;
      }
      $n++;
    }
  }
  //.prezzo_prodotto()


  //add_item_to_cart : aggiungi prodotto al carrello (ajax/session_addtocart)
  //@form : form array
  //id_prodotto
  //prodotto
  //width
  //height
  //prezzo
  //colore
  //collezione
  //qty = 1
  //font
  //testo
  //thumb
  //sconto
  //telaio
  //uuid
  //return => none
  function add_item_to_cart($form){
    $items = $this->CI->session->cart_items;
    $cart_item = array (
      'id'          => $form['id_prodotto'],
      'prodotto'    => $form['prodotto'],
      'w'           => $form['width'],
      'h'           => $form['height'],
      'prezzo'      => $form['prezzo'],
      'colore'      => $form['colore'],
      'collezione'  => $form['collezione'],
      'qty'         => 1,
      'font'        => $form['font'],
      'testo'       => $form['testo'],
      'thumb'       => $form['thumb'],
      'sconto'      => $form['sconto'],
      'telaio'      => $form['telaio'],
      'uuid'        => $this->CI->uuid->v4()
    );
    array_push($items,$cart_item);
    $this->CI->session->set_userdata('cart_items',$items);
    $this->calcola_totali_carrello();
  }
  //.add_item_to_cart()

  //cart_item_qty : aggiungi prodotto al carrello (ajax/cart-item-qty)
  //@form : form array
  //ricrea la sessione del carrello aggiornando la quantità dell'item
  //return => none
  public function cart_item_qty($form){
    $items = $this->CI->session->cart_items;
    $renewItems = [];
    $pos = 0;
    foreach ( $items AS $item ){
      if ( $item['uuid'] == $form['uuid'] ){
        if ( (int)$form['qty'] > 0 ){
          echo $form['qty'];
          $cart_item = array (
          'id'          => $item['id'],
          'prodotto'    => $item['prodotto'],
          'w'           => $item['w'],
          'h'           => $item['h'],
          'prezzo'      => $item['prezzo'],
          'colore'      => $item['colore'],
          'collezione'  => $item['collezione'],
          'qty'         => (int)$form['qty'],
          'font'        => $item['font'],
          'testo'       => $item['testo'],
          'thumb'       => $item['thumb'],
          'sconto'      => $item['sconto'],
          'telaio'      => $item['telaio'],
          'uuid'        => $item['uuid']
          );
          array_push ( $renewItems , $cart_item );
        }
      } else {
        array_push ( $renewItems , $item );
      }
    }
    $this->CI->session->set_userdata('cart_items',$renewItems);
    $this->calcola_totali_carrello();
  }
  //.cart_item_qty()

  //calcola_totali_carrello(): ricalcola totali ordine (funzione interna)
  //riaggiorna le variabili di sessione dell'ordine
  //questa funzione viene chiamata quando il carrello viene aggiornato da:
  //- inserimento nuovo prodotto
  //- modifica quantita prodotto
  //- rimozione prodotto da carrello
  //- inserimento coupon valido
  //la funzione richiama al suo interno due funzioni:
  // calcola_spedizioni(): per il ricalcolo delle spese di spedizione e quindi dei totali
  // set_totale_ordine(): reimposta le variabili di sessione dei totali dell'ordine
  //return => none
  public function calcola_totali_carrello(){
    $totale = 0;
    $carrello = 0;
    foreach ( $this->CI->session->cart_items AS $item ){
      $carrello += (float)$item['prezzo']*(float)$item['qty'];
    }
    $ordine = $this->CI->session->order;
    $ordine['CART_TOTAL'] = (float)$carrello;
    $spedizione = $this->calcola_spedizioni($carrello);
    //$coupon_perc = $this->CI->session->order['COUPON_VALUE'];
    //$coupon_value = ((float)$carrello*(float)$coupon_perc)/100;

    $sconto = $this->CI->session->order['SCONTO'];
    $coupon_value = $sconto;
    $totale = $carrello + $spedizione - $coupon_value;
    $this->set_totale_ordine($carrello,$spedizione,$coupon_value,$totale);
  }
  //.calcola_totali_carrello

  //set_totale_ordine(): riassegna le variabili di sessione per i totali dell'ordine
  //@carrello
  //@spedizione
  //@coupon_value
  //@$totale
  //return => none
  function set_totale_ordine($carrello,$spedizione,$coupon_value,$totale){
    $ordine = $this->CI->session->order;
    $ordine['CART_TOTAL']   = (float)$carrello;
    $ordine['SHIPPING']     = (float)$spedizione;
    $ordine['COUPON_VALUE'] = (float)$coupon_value;
    $ordine['ORDER_TOTAL']  = (float)$totale;
    $this->CI->session->set_userdata('order',$ordine);
  }
  //.set_totale_ordine()

  //calcola_coupon() : calcola il valore in percentuale dello sconto da applicare in funzione del coupon inserito
  //=>(ajax/coupon)
  //@form (@form['coupon'])
  //return => percentuale di sconto
  function calcola_coupon($form){
    $ordine = $this->CI->session->order;
    $perc = $this->CI->Site_model->coupon($form['coupon']);
    $valore = $perc;
    $ordine['COUPON'] = $form['coupon'];
    $ordine['COUPON_VALUE'] = $valore['valore'];
    $ordine['COUPON_TIPO'] = $valore['tipo'];
    $ordine['SCONTO'] = 0;

    $sconto = 0;
    if ( $valore['tipo'] == 0 ){
      $sconto = ((float)$form['totale']*(float)$valore['valore'])/100;
      $ordine['SCONTO'] = $sconto;
    } else {
      $sconto = $valore['valore'];
      $ordine['SCONTO'] = $sconto;
    }
    //print_r($valore);
    $this->CI->session->set_userdata('order',$ordine);
    $this->calcola_totali_carrello();
    echo $sconto;
  }
  //.calcola_coupon()

  //calcola_spedizioni() : calcolo spedizioni
  //=>ajax/spedizioni
  //@totale_carrello: totale dei prodotti nel carrello
  //return => spese di spedizione
  function calcola_spedizioni($totale_carrello){
    $pricing = $this->CI->Site_model->spedizioni();
    $spese = 0;
    foreach ( $pricing AS $price ){
      if ( (float)$price['ac_prezzo_max'] > (float)$totale_carrello  ){
        $ordine = $this->CI->session->order;
        $spedizione = (float)$price['ac_spedizione'];
        $ordine['SHIPPING'] = $spedizione;
        $this->CI->session->set_userdata('order',$ordine);
        return $spedizione;
        break;
      }
    }
    return $spese;
  }
  //.calcola_spedizioni()

  //registra_ordine() : registra l'ordine copiando le righe dalle tabelle temporanee
  //=> base_url/ordine/bonifico
  //=> base_url/ordine/paypal
  //@pagamento (modalità di pagamento)
  //- salva i dati nelle tabelle effettive
  //- mailer->conferma_ordine() : invia mail di conferma d'ordine
  //- resetta le sessioni carrello e ordine
  //return => true / false
  function registra_ordine($pagamento){
    $data['ordine_summary'] =  $this->CI->Order_model->order_summary_temp($this->CI->session->order['id']);
    $this->CI->Order_model->save_order($pagamento);

    if ( $this->CI->mailer->conferma_ordine($pagamento,$data) ){

      return true;
    } else {
      return false;
    }
  }
  //.registra_ordine()

  //reset_order_session() : reimposta i totali (0) delle variabili di sessione dell'ordine
  //return => none
  function reset_order_session(){
    $ordine = $this->CI->session->order;
		$ordine['id'] = '';
		$ordine['ORDER_TOTAL'] 	= 0;
		$ordine['SHIPPING'] 		= 0;
		$ordine['SHIPTO'] 			= false;
		$ordine['CART_TOTAL'] 	= 0;
		$ordine['COUPON']				= '';
		$ordine['COUPON_VALUE'] = 0;
		$ordine['MESSAGE'] 			=	0;
		$ordine['FATTURA'] 			= false;
		$this->CI->session->set_userdata('order', $ordine);
  }
  //.reset_order_session()

  //set_session_order_nr() : salva nella variabile di sessione dell'ordine in nr di ordine progressivo generato
  //@nr (nr ordine)
  //return => none
  function set_session_order_nr ( $nr ){
    $ordine_data = $this->CI->session->order;
    $ordine_data['id'] = $nr;
    $this->CI->session->set_userdata('order',$ordine_data);
    redirect(base_url().'cart/spedizione');
    return true;
  }
  //.set_session_order_nr()

}
