<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    function application_log($msg,$tipo){
      // Get a reference to the controller object
      $CI = get_instance();
      $path = '/home/admin/codeigniter/stikid_dev/stikid/application/logs/';
      $filename = 'applog_'.date('Y-m-d').'.log';
      $row = $tipo.' - '.date('Y-m-d').' '.date('H:i:s').' - '.$msg;
      file_put_contents( $path.''.$filename,"\n".$row,FILE_APPEND);
    }

    function application_log_read(){
      $CI = get_instance();
      $path = '/home/admin/codeigniter/stikid_dev/stikid/application/logs/';
      $filename = 'applog_'.date('Y-m-d').'.log';
      echo '<h3>LOG '.$filename.'</h3>';
      if ($file = fopen($path.''.$filename, "r")) {
        while(!feof($file)) {
          $line = fgets($file);
          echo $line.'<br>';
        }
        fclose($file);
      }
    }
