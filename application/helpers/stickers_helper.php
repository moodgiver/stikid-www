<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('calcolo_prezzo'))
{

    function common_data(){
      // Get a reference to the controller object
      $CI = get_instance();

      // You may need to load the model if it hasn't been pre-loaded
      $CI->load->model('Site_model');
      $CI->load->model('User_model');
      $data['header_meta'] = $CI->Site_model->header_meta();
      $data['header_hooks'] = $CI->Site_model->header_hooks();
      $data['header_top_right'] = $CI->Site_model->header_top_right();

      //load banner hooks
      $data['banner_hooks'] = $CI->Site_model->banners();

      $data['sidebar_menu'] = $CI->Site_model->sidebar_menu();
      $data['collezioni'] = $CI->Site_model->collezioni();
      $data['collezioni_qpb'] = $CI->Site_model->collezioni_qpb();
      //$data['offerte'] = $CI->Site_model->offerte();
      $data['footer_1'] = $CI->Site_model->pagine_footer(1);
      $data['footer_2'] = $CI->Site_model->pagine_footer(2);
      $data['footer_3'] = $CI->Site_model->pagine_footer(3);
      $data['footer_4'] = $CI->Site_model->pagine_footer(4);
      return $data;

    }

    function this_year(){
      return mdate('%Y',time());
    }

    function calcolo_prezzo($w,$h,$s)
    {
      // Get a reference to the controller object
      $CI = get_instance();

      // You may need to load the model if it hasn't been pre-loaded
      $CI->load->model('Site_model');
      $pricing = $CI->Site_model->pricing();
      $prezzoFinale = calcola_prezzo_finale($pricing,$w,$h,$s);

      return number_format($prezzoFinale,2);

    }

    function calcola_prezzo_finale($pricing,$w,$h,$s){
      $areaDiff = 0;
      $prezzoDiff = 0;
      $startingPrice = $pricing[0]['ac_prezzo_reale'];
      $area_minima = (float)((float)$w*(float)$h)/100;
      $prezzoFinale = $pricing[0]['ac_prezzo_reale'];
      $a = 0;
      $cs = 1;
      foreach ( $pricing AS $i ){
        $nextArea = (int)$i['ac_area'];
        $nextPrezzoMQ = $i['ac_prezzo'];

        if ( (int)$area_minima > (int)$i['ac_area']
              && $a > 0
              && (int)$area_minima < (int)$pricing[$a+1]['ac_area'] ) {
          $prevArea = (int)$pricing[$a-1]['ac_area'];
          $startingPrice = $pricing[$a-1]['ac_prezzo_reale'];
          $areaDiff = (int)$area_minima - (int)$pricing[$a-1]['ac_area'];
          if ( $areaDiff > 0 ){
            $prezzoDiff = ((int)$areaDiff * (int)$nextPrezzoMQ)/100;
            $prezzoFinale = (int)((int)$startingPrice + $prezzoDiff)*$s;
          } else {
            $prezzoFinale = $pricing[$a]['ac_prezzo_reale'];
          }
					break;
        } else {
          if ( (int)$area_minima == (int)$i['ac_area'] ){
            $prezzoFinale = $pricing[$a]['ac_prezzo_reale'];
          }
        }
        $a++;
      }
      $prezzoFinale = abs($prezzoFinale);
      if ( $prezzoFinale < 15 ){
        $prezzoFinale = 15;
      }
      return $prezzoFinale;
    }

    function uri_cat($c,$tipo){
      if ( $tipo == 0 ){
        return str_replace(' ','-',$c).'-wall-stickers';
      }
      if ( $tipo == 1 ){
        return str_replace(' ','-',$c).'-adesivi-murali';
      }
      if ( $tipo == 2 ){
        return str_replace(' ','-',$c).'-quadri-per-bambini';
      }
    }

    function uri_prodotto($categoria,$prodotto,$id,$tipo){
      if ( $tipo == 0 ){
        return str_replace(' ','-',$categoria).'/'.str_replace(' ','-',$prodotto).'-wall-sticker/'.$id;
      }
      if ( $tipo == 1 ){
        return str_replace(' ','-',$categoria).'/'.str_replace(' ','-',$prodotto).'-adesivo-murale/'.$id;
      }
      if ( $tipo == 2 ){
        return str_replace(' ','-',$categoria).'/'.str_replace(' ','-',$prodotto).'-quadro-per-bambino/'.$id;
      }
    }

    function prezzo_base_qpb($lista){
      // Get a reference to the controller object
      $CI = get_instance();

      // You may need to load the model if it hasn't been pre-loaded
      $CI->load->model('Site_model');
      $pricing = $CI->Site_model->prezzi_qpb($lista);
      return $pricing[0]['ac_prezzo'];
    }

    function calcolo_spedizioni($prezzo)
    {
      // Get a reference to the controller object
      $CI = get_instance();

      // You may need to load the model if it hasn't been pre-loaded
      $CI->load->model('Site_model');
      $pricing = $CI->Site_model->spedizioni();
      $spese = 0;

      foreach ( $pricing AS $price ){
        if ( (float)$price['ac_prezzo_max'] > (float)$prezzo  ){
          return (float)$price['ac_spedizione'];
          break;
        }
      }
      return $spese;
  	}

    function coupon($coupon)
    {
      // Get a reference to the controller object
      $CI = get_instance();
      // You may need to load the model if it hasn't been pre-loaded
      $CI->load->model('Site_model');
      $c = $CI->Site_model->coupon($coupon);
      return $c;
    }

    function ordine_nr(){
      // Get a reference to the controller object
      $CI = get_instance();

      // You may need to load the model if it hasn't been pre-loaded
      $CI->load->model('Order_model');
      $c = $CI->Order_model->numero_ordine();
      return $c;
    }

    function adesso(){
      $CI = get_instance();
      return date('Y-m-d H:i:s');
    }


}
