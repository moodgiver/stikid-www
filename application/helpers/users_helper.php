<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function check_register($email){
  $CI = get_instance();
  $CI->load->model('User_model');
  $user = $CI->User_model->check_email($email);
  return $user;
}

function check_login(){
  $CI = get_instance();
  $CI->load->model('User_model');
  $user = $CI->User_model->login_user($_POST);
  return $user;
}

function send_email_registrazione(){
  $CI = get_instance();

  // You may need to load the model if it hasn't been pre-loaded
  $CI->load->model('Site_model');
  $CI->load->model('User_model');

  $CI->load->library('email'); // load email library

  $message = 'Benvenuto,<br>
  Le confermiamo la registrazione al sito STICASA/STIKID registrata in data odierna.
  <br><br>
  Grazie per la sua registrazione.
  <br><br>
  Per qualsiasi quesito puo\' contattarci direttamente dal sito.</br><br>
  Nel caso avesse ricevuto questa mail senza aver effettuato la registrazione a www.sticasa.com o www.stikid.com la preghiamo di contattare immediatamente la nostra assistenza clienti
  servizioclienti@sticasa.com
  <br><br>
  Registrazione del: '.date('d-m-Y H:i:s').'<br>
  Indirizzo IP: '.$_SERVER['REMOTE_ADDR'].'<br><br>

  www.sticasa.com - www.stikid.com - www.quadriperbambini.com - www.4tribe.com';
  // Get a reference to the controller object

  $order = $CI->session->order;
  $cart = $CI->session->cart_items;
  $CI->email->from('servizioclienti@sticasa.com', 'STICASA / STIKID');
  $CI->email->bcc('sticasa@sticasa.com');
  $CI->email->to($CI->session->user['email']);
  $CI->email->subject("REGISTRAZIONE STICASA/STIKID");
  $CI->email->message($message);
  $noerr = $CI->email->send();
  if ( $noerr ){
    return true;
  } else {
    return $noerr;
  }
}

function province(){
  $CI = get_instance();
  $CI->load->model('User_model');
  $province = $CI->User_model->province();
  return $province;
}
